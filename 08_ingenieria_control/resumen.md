# Controlador mixto con feedback y red neuronal para servidor web Apache

## Grupo

- Chávez Astucuri, Steve Manuel\hfill 09200078
- Nuñez Supo, David Percy\hfill 10200192
- Rojas Escudero Alex James\hfill 09200111

## Introducción

En años recientes, los usuarios de la internet han crecido exponencialmente. De acuerdo a Webstat1, los usuarios de la internet
fueron alrededor de un billón en Marzo del 2007. Algunos usuarios reportan quejas debido a tiempos altos de respuesta durante
picos de tráfico.

Este alto tiempo de respuesta no necesariamente es causa del poco ancho de banda o de un cliente muy lento.
En vez, el cuello de botella usualmente es el servidor web en sí. Cuando se tienen picos de tráfico, excesivas peticiones,
el servidor puede sobrecargarse.

Usando un mecanismo de control de admisión, se pueden rechazar algunas peticiones
cuando el tráfico entrante es muy alto manteniendo así la calidad de servicio(QoS) y por tanto se puede garantizar un tiempo de respuesta adecuado a pesar de picos de tráfico.

\newpage

## Descripción de la arquitectura

![Arquitectura](arquitectura.png)

La Figura 1 muestra la arquitectura del controlador mixto con feedback y red neuronal.

El modelo neuronal es usado para calcular el valor del parámetro **MaxClients**(número máximo de clientes) de Apache para alcanzar
un tiempo de respuesta promedio dado el promedio observado actual de la llegada de peticiones.

El controlador con feedback compara el tiempo de respuesta promedio real alcanzado con el promedio deseado y ajusta **MaxClients**
para asegurar que el promedio del tiempo de respuesta deseado se mantenga.

El modelo neuronal y el controlador con feedback operan concurrentemente y de una forma complementario como se muestra en la Figura 1.

Apache está estructurado como un pool de procesos esclavo monitoreados por un maestro. Cada proceso esclavo es responsable
de mantener la comunicación con los clientes web y puede mantener solo una conexión a la vez.

El parámetro **MaxClients** limita el tamaño del pool de procesos esclavo y por tanto impone una limitación de capacidad de procesamiento
en el servidor. Un valor **MaxClients** más alto permite a Apache procesar más peticiones de clientes, pero si es muy grande,
hay un excesivo uso de recursos que aumenta considerablemente el tiempo de respuesta promedio.

Periódicamente, el modelo neuronal estima la medida de llegada de peticiones $\lambda$ y recalcula **MaxClients** para que el tiempo de
de respuesta promedio se mantenga en el valor deseado. El controlador con feedback compara entonces el tiempo de respuesta promedio
obtenido previamente con el promedio de referencia(mostrado como _Ref_ en la Figura 1). La diferencia(mostrada como $\Delta$ _Response time_ en la Figura)
es el error que el controlador tiene que corregir haciendo un ajuste de **MaxClients**.

## Configuración del experimento

Se integró la red neuronal codificada en C con el servidor web Apache.

El servidor usado presenta las siguientes características: _Pentium III, procesador de 800MHz, 512Mbs de memoria, FreeBSD 5.1_
con conexión Ethernet. El servidor web es Apache 1.3 de la Apache Software Foundation.

Los clientes presentan las siguientes características: _Pentium IV, procesador de 2.4GHz, 256Mbs de memoria, Linux Mandrake 9.2_
con conexión _Ethernet_. En estas se instalo un software generador de carga HTTP, llamado **HTTPERF**.

![Tráfico generado por HTTPERF](httperf.png)

\newpage

## Resultados obtenidos

Para poder evaluar la performance del controlador mixto con feedback y red neuronal, comparamos su performance
con el controlador con feedback solo y la red neuronal sola. Se llevaron a cabo 3 experimentos. El primero
muestra el efecto del controlador con red neuronal solo integrado en Apache. El segundo muestra el efecto de integrar
el controlador con feedback. El último muesetra el efecto mixto. En todos los experimentos, el tiempo de respuesta
promedio deseada fue de 400ms y la entrada de carga se muestra en la Figura 2.

![Tiempo de respuesta promedio con y sin red neuronal](sin_con_red_neuronal.png)

![Tiempo de respuesta promedio con control PI](con_pi_control.png)

![Tiempo de respuesta promedio con red neuronal y control PI](con_red_neuronal_y_pi_control.png)

\newpage

Para comparar los resultados de los 3 experimentos se uso ajustó el error usando mínimos cuádrados entre el tiempo de
respuesta promedio deseado y el tiempo de respuesta promedia obtenido en toda la duración del experimento.
Mientras más pequeño el error agregado, mejor es la convergencia.

  Controlador              Red neuronal    PI   PI y Red neuronal
------------------------   ------------  -----  -----------------
ajuste minimos cuadrados        23        6,45       3,17

## Conclusión

Usando la red neuronal sola, un error grande y constante se desarrolla y la fluctuación también es grande(Figura 3)
El controlador PI reduce la fluctuación y el error(Figura 4) pero esto mejora mucho en la presencia de la red neuronal(Figura 5)
Esto es por que la red neuronal es capaz de proporcionar una salida aproximada que permite alcanzar una respuesta promedio
mas cercana a la deseada. El controlador PI entonces solo tiene que manejar el error residual.

La evaluación experimental muestra las ventajas de integrar un modelo de red neuronal con un controlador con feedback
para obtener garantías de calidad de servicio(QoS) en el servidor web Apache.

## Referencia

- Autores : R. FONTAINE, P. LAURENCOT and A. AUSSEM
- Título original: Mixed Neural and Feedback Controller for Apache Web Server
- Año: 2009
