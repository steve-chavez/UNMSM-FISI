# Tarea 1 de Ingeniería Económica

- Profesor : Rodolfo Rojas Gallo
- Alumno   : Manuel Chávez Astucuri
- Código   : 09200078

## Problema 1

1. Planteamiento del problema

La Gerencia de Seguridad Ciudadana de la Municipalidad de La Victoria(MLV), desea mejorar su capacidad de toma de decisiones en vistas de la creciente 
actividad delincuencial del distrito. Para esto requiere de un sistema integrado de información geográfica, donde pueda visualizar las incidencias y 
los recursos disponibles en un instante de tiempo. De esta forma el personal estratégico del área puede hacer una asignación de recursos más eficiente.			

2. Análisis del problema

Para satisfacer la necesidad de la MLV, se desea desarrollar un sistema hecho a medida que use solo componentes open source(sin costo de licencia).
Para esto se debe reservar a un número de empleados del área de TI de la municipalidad por un tiempo dado.

3. Búsqueda de Alternativas

Se puede comprar un sistema ya hecho del provedor X, con componentes propietarios(costos de licencia).

4. Evaluación de Alternativas

El sistema del proveedor X, provee varias de las funcionalidades requeridas por el sistema, pero no todas.
La customización de este sistema es costosa(mas tiempo) si es que el personal de TI de la MLV es la encargada, por tanto se depende
del proveedor para añadir las funcionalidades requeridas(costo adicional).

Características del sistema del proveedor X:

- Costo: 10000 dólares
- Tiempo: Instalación inmediata
- Soporte adicional: 1000 dólares por funcionalidad requerida

Características del sistema hecho a medida por la MLV:

- Costo: 15000 soles
- Tiempo: 5 meses para puesta en producción
- 2 empleados a tiempo completo para su implementación: 1500 mensual cada uno
- El soporte vendrá del mismo personal, solo tomando tiempo de su horario de oficina(no costo adicional).

5. Selección de la solución

Se elige la implementación por parte de la MLV. No se requiere inmediatez en la puesta en producción del sistema. Y el costo es menor.
			
## Problema 2

![](diagrama.png)

## Problema 3

## Ejercicios de interés simple

1. Calcular a cuánto asciende el interés simple producido por un capital de 25 000 dólares invertido durante 4 años a una tasa del 6 % anual.

$$
I = 25000\times0.06\times4 = \$6000
$$

2. Calcular el interés simple producido por 30 000 dólares durante 90 días a una tasa de interés anual del 5 %.

$$
I = 30000\times0.05\times\frac{90}{360} = \$375
$$

\pagebreak

## Ejercicios de interés compuesto

3. Se depositan $8.000 en un banco que reconoce una tasa de interés del 36% anual, capitalizable mensualmente.  ¿Cuál será el monto acumulado en cuatro años?

$$
n = 4 \text{ años} = 48 \text{ meses}
$$
$$
i = 0.36 \text{ anual} = \frac{0.36}{12} \text{ mensual}  
$$
$$
i = 0.03 \text{ mensual}  
$$
$$
I = 8000\times(1 + 0.03)^{48} = 8000\times4.1322 = 33058.01
$$

4. Calcular el valor final de un capital de $ 20.000 a interés compuesto durante 15 meses y 15 días a la tasa de interés del 24% capitalizable mensualmente.

$$
n = 15\text{ meses, }15\text{ días} = 15.5\text{ meses}
$$
$$
i = 0.24\text{ anual} = \frac{0.24}{12}\text{ mensual}
$$
$$
I = 20000\times(1 + \frac{0.24}{12})^{15.5} = 20000\times1.3592 = 25185.20
$$
