;; Funcion
(defun primos(numero)
  (let
    ((num numero) (resultado))
    (loop while (= (rem num 2) 0) do
      (setq resultado (append resultado (list 2)))
      (setq num (floor num 2)))
    (do
      ((i 3 (+ 2 i)))
      ((> i num))
      (loop while (= (rem num i) 0) do
        (setq resultado (append resultado (list i)))
        (setq num (floor num i))))
    resultado))

;; Pruebas
(print
  (if
    (and
      (equal (primos 5) '(5))
      (equal (primos 12) '(2 2 3))
      (equal (primos 16) '(2 2 2 2))
      (equal (primos 135) '(3 3 3 5))
      (equal (primos 2145) '(3 5 11 13))
      (equal (primos 12345) '(3 5 823)))
    "primos: pruebas exitosas"
    "primos: pruebas fallidas"))
