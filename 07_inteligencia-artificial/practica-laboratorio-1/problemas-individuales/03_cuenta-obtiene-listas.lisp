;; Funciones
(defun cuenta-listas(lista)
  (cond
    ((or (null lista) (atom lista)) 0)
    (t (+ (if (atom (car lista)) 0 1) (cuenta-listas (cdr lista))))))

(defun obtiene-listas(lista)
  (cond
    ((or (null lista) (atom lista)) nil)
    (t (append (if (atom (car lista)) nil (list (car lista))) (obtiene-listas (cdr lista))))))

(defun cuenta-obtiene-listas(lista) "devuelve el conteo como primer elemento y el resto son las listas"
  (append (list (cuenta-listas lista)) (obtiene-listas lista)))

;; Pruebas
(print
  (if
    (and
      (equal (cuenta-obtiene-listas '(nil (1 2) (3 4) (5) 7 8 9)) '(3 (1 2) (3 4) (5)))
      (equal (cuenta-obtiene-listas '((1) (2) (3) (4) (5) (6))) '(6 (1) (2) (3) (4) (5) (6)))
      (equal (cuenta-obtiene-listas '((666))) '(1 (666)))
      (equal (cuenta-obtiene-listas '(1 (2 3) nil 4 (5 6) nil)) '(2 (2 3) (5 6)))
      (equal (cuenta-obtiene-listas 100) '(0))
      (equal (cuenta-obtiene-listas 'algo) '(0)))
    "cuenta-obtiene-listas: pruebas exitosas"
    "cuenta-obtiene-listas: pruebas fallidas"))
