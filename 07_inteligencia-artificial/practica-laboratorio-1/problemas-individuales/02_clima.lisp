;; Funcion
(defun clima(temperatura)
  (cond
    ((< temperatura 0) 'helado)
    ((and (>= temperatura 0) (< temperatura 10)) 'frio)
    ((and (>= temperatura 10) (< temperatura 20)) 'calido)
    ((<= 20 temperatura 30) 'sofocante)
    ((> temperatura 30) 'abrasivo)))


;; Pruebas
(print
  (if
    (and
      (eq (clima -1) 'helado)
      (eq (clima 5)  'frio)
      (eq (clima 11) 'calido)
      (eq (clima 22) 'sofocante)
      (eq (clima 31) 'abrasivo))
    "clima: pruebas exitosas"
    "clima: pruebas fallidas"))
