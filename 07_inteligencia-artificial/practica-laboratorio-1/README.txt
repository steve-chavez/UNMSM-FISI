Todos los problemas estan incluidos en labo_1_chavez_astucuri_manuel.txt.

Los problemas individuales en la carpeta problemas-individuales/ incluyen pruebas, pueden ser corridos con sbcl en linea de comandos:

sbcl --script problemas-individuales/01_mcd.lisp
sbcl --script problemas-individuales/02_clima.lisp
sbcl --script problemas-individuales/03_cuenta-obtiene-listas.lisp
sbcl --script problemas-individuales/04_primos.lisp
sbcl --script problemas-individuales/05_torres-de-hanoi.lisp
