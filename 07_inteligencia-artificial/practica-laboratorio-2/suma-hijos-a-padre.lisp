;; Función
(defun suma-hijos-a-padre(l) "Para un arbol binario, suma los nodos hijos a los nodos padre"
  (cond
    ((null l) nil)
    (t
      (let
        ((izq (suma-hijos-a-padre (if (atom l) nil (second l))))
         (der (suma-hijos-a-padre (if (atom l) nil (third l))))
         (obtener-raiz (lambda (x)
           (cond
             ((or (null x) (atom x)) 0)
             (t (first x))))))
        (remove nil
          (list (+ (if (atom l) l (first l))
                   (funcall obtener-raiz izq)
                   (funcall obtener-raiz der))
                izq
                der))))))

;; Pruebas:
(print
  (if
    (and
;; Ejemplo dejado en clase:
;;
;;     18                87
;;    /  \              /  \
;;  13    20    ->    20    39
;; /  \    |         /  \    |
;;7    10 19        7    10 19
      (equal (suma-hijos-a-padre
               '(18 (13 7 10) (20 19)))
               '(87 (30 (7) (10)) (39 (19))))
      (equal (suma-hijos-a-padre
               '(1 (2) (3)))
               '(6 (2) (3)))
      (equal (suma-hijos-a-padre
               '(1 (2 (9) (10 (11))) (4 (5) (6 (7)))))
               '(55 (32 (9) (21 (11))) (22 (5) (13 (7)))))
      (equal (suma-hijos-a-padre
               nil)
               nil))
    "suma-hijos-a-padre: pruebas exitosas"
    "suma-hijos-a-padre: pruebas fallidas"))
