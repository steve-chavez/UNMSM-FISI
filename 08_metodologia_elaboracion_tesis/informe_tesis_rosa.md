---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{0pt}
- \usepackage{titling}
- \usepackage{array}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{float}
- \usepackage{listings}
- \usepackage{tikz}
---


\title{Devops aplicado a PostgreSQL para evitar el riesgo de vendor lock-in del cloud computing en la empresa Simply Connected Systems\vspace{2.0cm}}
\author{\textbf{Manuel Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Orientador: Prof. Menéndez Mueras Rosa}

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Junio 2020}
\end{center}

\thispagestyle{empty}

\newpage

# 1. Introducción

## 1.1. Realidad problemática

En la ya madura era de la información, es conocido que para una empresa
u organización, los datos son el activo más valioso. Sin embargo, mantener
una infraestructura, ya sea local o en la nube como IaaS, capaz de manejar
grandes volúmenes de datos es un trabajo de mucho costo para cualquier empresa,
en especial si la empresa no se especializa en TI. Esto es independiente de la
tecnología de base de datos que se use(SQL o NoSQL).

Es por esto que diversos proveedores de la nube(AWS, GCP, Azure, etc), ofrecen
servicios de cloud computing conocidos como DBaaS, que autogestionan bases de datos
en diversas escalas. En esencia un modelo de **leasing** para bases de datos.

Estos DBaaS ofrecen una gran conveniencia para gestionar las bases de datos;
sin embargo se cuenta con muchos riesgos al hacerse dependiente
de un servicio en la nube, más aún si solo se depende de un solo proveedor.

En el caso particular de Simply Connected Systems(ver ANEXO 01) es de suma importancia para
las empresas a las cuales brinda servicios de TI la capacidad de gestionar su propia infraestructura y
controlar su propia información.

Simply Connected Systems brinda soluciones de TI a empresas que han operado por 4 generaciones(casi 100 años).
Cuál sería la postura de negociación que podría tomar una empresa si luego de
10 años de operación toda su información se encuentra asilada en el servicio de
determinado proveedor? Podrá migrar fácilmente si es que su actual proveedor
aumenta los precios drásticamente?

No. Es seguro que la empresa estará ante un caso de aprisionamiento tecnológico(vendor lock-in).

\newpage

## 1.2 Riesgos del cloud computing

Para ejemplificar mejor la realidad problemática, a continuación se mostrará una ontología de los riesgos identificados en
_Risks in enterprise cloud computing_[@dutta2013risks].

![Ontología de riesgos](images/risks-ontology.png){ width=80% }

\newpage

De estos riesgos, los principales obtenidos por la encuesta realizada por [@dutta2013risks] son:

\begin{table}[H]
  \begin{tabular}{ | m{0.1\textwidth} | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  \textbf{Ranking} &
  \textbf{ID Riesgo} &
  \textbf{Descripcion} &
  \textbf{Score del Riesgo} \\
  \hline
  1 &
  LR1.1 &
  La privacidad de la empresa o data del cliente es puesta en riesgo en la nube &
  153.50 \\
  \hline
  2 &
  LR1.3 &
  Leyes inconsistentes de protección de datos en los distintos países donde la data se genera y se guarda. &
  151.75 \\
  \hline
  3 &
  OGR4.2 &
  Dificultad para cambiar de proveedor de la nube incluso en caso de insatisfacción con el servicio(vendor lock-in) &
  148.50 \\
  \hline
  4 &
  OGR5.2 &
  Las compañías usuarias carecen de recuperación de desastres y planes de contingencia para lidiar con fallas
  técnicas inesperadas en la nube &
  147.75 \\
  \hline
  5 &
  LR3.2 &
  Dificultad de migración de la empresa al término del contrato con el proveedor de la nube &
  140.25 \\
  \hline
  6 &
  OPR4.2 &
  Inadecuado entrenamiento y/o conocimiento de los servicios de la nube. &
  139.75 \\
  \hline
  7 &
  OPR5.1 &
  Las aplicaciones en la nube temporalmente caen temporalmente en fuera de servicio. &
  137.25 \\
  \hline
  8 &
  OPR2.1 &
  Costos ocultos que se incrementan debido a modos de operacion no transparentes en la nube. &
  136.00 \\
  \hline
  9 &
  TR4.3 &
  Ataques de denegación de servicio a un proveedor de la nube. &
  135.50 \\
  \hline
  10 &
  TR4.1 &
  Acceso no authorizado a data empresarial en la nube. &
  135.00 \\
  \hline
  \end{tabular}
  \caption{Riesgos principales del Cloud Computing}
\end{table}

## 1.3 Trabajos previos

Se muestran a continuación, trabajos previos relacionados a riesgos en la nube, 3 internacionales y 3 nacionales, respectivamente.

### A decision framework to mitigate vendor lock-in risks in cloud (saas) mgration

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Bournemouth University
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Doctor and Philosophy
|-----------------------|----------------------------------------------------------------------
|Autor                  | Justice Nsirimovu Opara-Martins

En esta tesis se provee un framework de decisiones para mitigar los riesgos de vendor lock-in
en una migración a la nube. Esto es útil para esta investigación debido a que pone a la luz el gran trabajo
que se debe dar para mitigar los riesgos del vendor lock-in. En nuestro caso buscamos evadir totalmente
este riesgo.

### Enhancing Information Security in Cloud Computing Services using SLA Based Metrics

|Año                    | 2011
|-----------------------|----------------------------------------------------------------------
|Institución            | Blekinge Institute of Technology - Sweden
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Master thesis
|-----------------------|----------------------------------------------------------------------
|Autores                | Nia Ramadianti Putri Medard Charles Mganga

En esta tesis se mencionan unas métricas de acuerdos de nivel de servicio(SLAs) para asegurar la seguridad en la nube.
Esto es particularmente útil para reutilizar métricas y asegurar las mismas garantías de estos SLAs.

### Cloud Computing: COBIT-mapped benefits, risks and controls for consumer enterprises

|Año                    | 2012
|-----------------------|----------------------------------------------------------------------
|Institución            | Stellenbosch University
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Masters of Commerce (Computer Auditing)
|-----------------------|----------------------------------------------------------------------
|Autor                  | Zacharias Enslin

En esta tesis se mapean procesos de COBIT a riesgos en la nube.

- Riesgos mapeados al proceso Plan and Organize(PO)
- Riesgos mapeados al proceso Acquire and Implement(AI)
- Riesgos mapeados al proceso Deliver and Support(DS)
- Riesgos mapeados al proceso Monitor and Evaluate(ME)

Útil para obtener una estructura más formal de riesgos y tener el gran panorama de estos.

\newpage

### Modelo de continuidad de servicios de las tecnologías de la información y comunicación utilizando cloud computing en la empresa americas potash perú s.a

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Universidad Católica Los ángeles de Chimbote - Facultad de Ingeniería
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Magíster en tecnología de información y comunicación
|-----------------------|----------------------------------------------------------------------
|Autora                 | Ing. Dominguez Oliva Wendy

En esta tesis se mencionan los "Riesgos encontrados por Gartner" en 2011, se citan los siguientes:

- Accesos de usuarios con privilegios.
- Cumplimiento normativo
- Localización de los datos
- Aislamiento de datos
- Recuperación
- Soporte investigativo
- Viabilidad a largo plazo

### Propuesta de implementación de cloud computing para asegurar continuidad operativa de infraestructura informática en empresa de internet

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Universidad San Ignacio de Loyola
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Título Profesional de Ingeniero Empresarial y de Sistemas
|-----------------------|----------------------------------------------------------------------
|Autor                  | Chirinos Muñoz, Percy Humberto

En esta tesis se citan las "Desventajas del Cloud Computing" encontradas por García Juárez.

- Continuidad del servicio
- Lock-in de servicios/aplicaciones
- Probabilidad de ataques de hackers
- Desconocimiento de políticas de administración de datos/aplicaciones
- Retorno de la inversión

### Propuesta de Arquitectura Cloud Computing para la migración del sistema integrado de control académico de la universidad nacional de tumbes

|Año                    | 2015
|-----------------------|----------------------------------------------------------------------
|Institución            | Universidad San Ignacio de Loyola
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Título Profesional de Ingeniero de sistemas
|-----------------------|----------------------------------------------------------------------
|Autor                  | Bruno Alexander Sánchez Oviedo

En esta tesis se citan los "Riesgos de trabajar con Cloud Computing" encontrador por el group ACE.

- Contratos
- Pérdida de Control
- Agregación de Riesgos
- Costo
- Seguridad de los datos

### Conclusiones

En general, a nivel internacional se puede notar la preocupación por identificar y mitigar adecuadamente los riesgos del cloud computing.
Nacionalmente se puede apreciar que los riesgos no son un mayor obstáculo para la adopción de la nube.

## 1.4 Teorías relacionadas al tema

Empezaremos definiendo qué es cloud computing, de acuerdo a @mell2011nist, listando sus categorías ya tradicionales para luego llegar
a la más reciente subcategoría de DbaaS.

## 1.4.1 Cloud computing

Cloud computing es un modelo que habilita un acceso on-demand, ubicuo y conveniente a un pool compartido de recursos computacionales
que puede ser rápidamente provisto con un esfuerzo mínimo de configuración e interacción con el proveedor del servicio.

Ahora veremos las categorías o modelos de servicio tradicionales:

### 1.4.1.1 Infrastructure as a Service(IaaS)

La capacidad provista al consumidor es la de poder aprovisionar procesamiento, almacenamiento, redes y otros recursos computacionales
fundamentales donde el consumidor es capaz de desplegar y correr software arbitrario, que puede incluir sistemas operativos
y aplicaciones. El consumidor no controla la infraestructura(hardware) pero tiene control sobre el sistema operativo, almacenamiento,
aplicaciones desplegadas y posiblemente la configuración de red.

### 1.4.1.2 Platform as a Service(PaaS)

La capacidad provista al consumidor es el despliegue a la infraestructura del proveedor. Pero solo de aplicaciones creadas usando
lenguajes de programación, librerías, servicios y herramientas soportadas por el proveedor. El consumidor no maneja
o controla la infraestructura incluyendo la configuración de red, servidor, sistema operativo y almacenamiento, pero tiene control
sobre las aplicaciones desplegadas y posiblemente sobre las configuraciones de la aplicación desplegada.

### 1.4.1.3 Software as a Service(SaaS)

La capacidad provista al consumidor es el uso de aplicaciones alojadas en la infraestructura del proveedor. Las aplicaciones son
accesibles desde varios dispositivos clientes a través de una interfaz ligera, como un navegador web o la interfaz de un programa.
El consumidor no maneja o controla la infraestructura del proveedor, incluyendo su configuración de red, servidores, sistemas
operativos, almacenamiento o incluso capacidades individuales de las aplicaciones, con la posible excepción de algunas configuraciones
específicas al usuario.

### 1.4.1.4 Database as a Service(DbaaS)

De acuerdo a @Curino2011RelationalCA, el DbaaS mueve la carga operacional del aprovisionamiento, configuración,
escalado, afinación de performance, políticas de backup y control de acceso de la mano de los usuarios de la base de datos
hacia la mano del proveedor del servicio, reduciendo así los costos al usuario. Siendo Amazon RDS y
Microsoft SQL Azure los pioneros en ofrecer DbaaS.

En esencia, DbaaS es una especialización del SaaS que ofrece un modelo de leasing o outsourcing para Sistemas Gestores de Bases de Datos.

Para poder ahondar en los riesgos del cloud computing, veremos ahora una base de la Gestión de Riesgos según @Sommerville:2010:SE:1841764.

## 1.4.2 Gestión de Riesgos

La gestión del riesgo implica anticipar riesgos que pudieran alterar el calendario del proyecto o
la calidad del software a entregar, y posteriormente tomar acciones para evitar dichos riesgos.

### 1.4.2.1 Tipos de riesgos

Tenemos 3 tipos de riesgos:

1. Riesgos del proyecto: Los riesgos que alteran el calendario o los recursos del proyecto.

2. Riesgos del producto: Los riesgos que afectan la calidad o el rendimiento del software a desarrollar.

3. Riesgos empresariales: Los riesgos que afectan la organización que desarrolla o adquiere el software.

En el caso del cloud computing estamos ante un **riesgo del proyecto** y **riesgo del producto**.

### 1.4.2.2 Proceso de Gestión de Riesgos

Para evaluar los riesgos necesitamos seguir las siguientes etapas:

1. Identificación del riesgo: La enumeración de los posibles riesgos para el proyecto, producto y empresa.

2. Análisis de riesgos: Se debe valorar la probabilidad y las consecuencias de dichos riesgos.

3. Planeación del riesgo: Elaborar planes para enfrentar el riesgo, evitarlo o minimizar sus efectos en el proyecto.

4. Monitorización del riesgo: Valorar regularmente el riesgo y los planes para atenuarlo, y revisarlos cuando se aprenda más
sobre el riesgo.

Afortunadamente para nuestro caso sobre cloud computing, ya existe un desarrollo de la **identificación** y **análisis** de riesgos el cual
desarrollaremos en la sección del estado del arte.

Lo cual nos deja con la mayoría de trabajo para la **Planeación del riesgo**, la cual tiene 3 tipos de estrategias:

- Estrategias de evitación: reducir la probabilidad de que surja el riesgo.

- Estrategias de minimización: reducir el efecto del riesgo.

- Planes de contingencia: prepararse pra el mayor impacto del riesgo y tener una estrategia para hacer frente a ello.

## 1.4.3 DevOps e Infraestructura como Código

Para entender cómo pensamos obtener la **administración autónoma** veremos qué es **DevOps** y **Infraestructura como Código**, según @Artac_2017.

**DevOps** comprende una serie de tácticas de ingeniería de software que apuntan a
reducir el tiempo accionable de cambio de diseño de software.

Una de estas tácticas es la **Infraestructura como código** que implica definir un modelo
que contiene specificaciones listas para la orquestación en la nube.

### 1.4.3.1 Gestión de sistemas

A continuación veremos unos modelos de gestión de sistemas que nos permitirán evaluar las diferentes herramientas
de **Infraestructura como código** que nos ofrece el mercado.

De acuerdo a @Traugott:2002:WOM:1050517.1050529 existen 3 modelos de gestión de sistemas:

\begin{figure}%
  \centering
  \subfloat[Divergente]{{\includegraphics{images/divergence.png} }}%
  \qquad
  \subfloat[Convergente]{{\includegraphics{images/convergence.png} }}%
  \qquad
  \subfloat[Congruente]{{\includegraphics{images/congruence.png} }}%
  \caption{Modelos de gestión de sistemas}%
\end{figure}

\newpage

### 1.4.3.2 Modelo divergente

La divergencia es caracterizada por la configuración del sistema alejándose del estado ideal del sistema.

Esto es ilustrado en el flujo de trabajo típico de un sysadmin, el cual sigue un checklist y manualmente ejecuta paso por paso los comandos
necesarios para alcanzar el estado del sistema deseado. Si 3 distintos sysadmins siguen el mismo checklist,
es probable que se obtengan 3 resultados distintos.

Este viene a ser el modelo tradicional, donde se usan scripts bash para automatizar algún despliegue. Esto no es considerado
**Infraestructura como código**, puesto que sigue un enfoque imperativo y no declarativo.

### 1.4.3.3 Modelo convergente

La convergencia es caracterizada por la configuración del sistema acercándose al estado ideal del sistema.

Esta es una mejora sobre el modelo divergente, en este caso se usan herramientas más maduras
que permiten declarar una descripción del sistema. Algunas herramientas actuales del mercado son: **Puppet**, **Ansible**.
El problema es que estás herramientas aún pueden diverger del estado ideal, es necesario
monitorear las diferencias entre la descripción del sistema y estado ideal deseado.

### 1.4.3.4 Modelo congruente

La congruencia es la práctica de mantener la configuración del sistema en completa igualdad al estado ideal del sistema.

Este modelo es caracterizado por herramientas actuales como **contenedores**(Docker, LXC) o **NixOS**(@dolstra2008nixos).
En estos casos el estado real del sistema es un espejo de la descripción del sistema.

## 1.4.4 Indicadores

### 1.4.4.1 Recuperación de desastres(Disaster Recovery)

La principal oferta de un DbaaS es la recuperación de desastres, es decir, la restauración de la base de datos al
estado más reciente y consistente. Para esto tenemos dos indicadores.

### 1.4.4.2 Tiempo objetivo de recuperación(RTO)

Es el tiempo máximo permitido para la recuperación de un servicio después de una interrupción.

\begin{equation*}
\boldsymbol{RTO = FechaHoraDesastre - FechaHoraRecuperacion}
\end{equation*}

Un RTO ideal para un servicio de cloud computing viene a ser menor a 1 minuto.

### 1.4.4.3 Punto objetivo de recuperación(RPO)

Es la cantidad máxima de datos que se pueden perder cuando se restablece el servicio después de una interrupción.
El punto de recuperación objetivo se expresa en un período de tiempo antes de la falla.
Por ejemplo, un punto de recuperación objetivo de un día puede ser apoyado por los backups diarios, y donde se pueden perder datos
de hasta 24 horas.

\begin{equation*}
\boldsymbol{RPO = DatosPerdidos(FechaHoraDesastre - FechaUltimoBackup)}
\end{equation*}

Cero es un valor válido y es equivalente a un requisito de "pérdida de datos cero".

### 1.4.3.4 Estado inicial

Al desplegar una nueva instancia de PostgreSQL, sin configuración alguna para recuperación de desastres(lo cual es bastante
común, incluso en sistemas que llevan años en producción), se tienen las siguientes medidas:

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  $\infty$ &
  $\infty$ \\ \hline
  \end{tabular}
  \caption{RPO/RTO inicial}
\end{table}

Basícamente la pérdida de datos es infinita y el tiempo de recuperación también es infinito.
Nuestro objetivo es mejorar estas medidas a resultados más aceptables para la recuperación de desastres.

### 1.4.3.5 Objetivo

Se desea llegar a los siguientes resultados:

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  ~ 0 &
  < 1 min \\ \hline
  \end{tabular}
  \caption{RPO/RTO deseado}
\end{table}

Lo que básicamente significa que existe pérdida de datos 0 y un tiempo de recuperación menor a 1 minuto. Lo que es ideal
según las características ofrecidas por un DbaaS.

## 1.5 Metodología de la variable independiente

Para este proyecto se analizaron herramientas de Devops en base al modelo de gestión de sistemas expuesto prevamente.

### 1.5.1 Herramientas de gestión de sistemas

Ahora veremos las herramientas más populares actualmente que permiten gestionar la configuración del sistema a través de la Infraestructura como
Código.

\begin{figure}[H]
  \centering
  \includegraphics{images/iac-tools.png}
  \caption{Herramientas de gestión de sistemas}%
\end{figure}

### 1.5.1.1 Docker

Docker es una herramienta que permite crear entornos llamados contenedores,
compartir estos y correr aplicaciones aisladas en ellos.

El concepto de contenedores es similar a las máquinas virtuales.
Mientras que una máquina virtual ejecuta recursos virtualizados, un contenedor
comparte los recursos con su host.

Docker usa distintas técnicas ofrecidas por el kernel de Linux, para aislar
y abstrar el contenedor del host.

Otro aspecto importante de Docker es que permite compartir y distribuir imágenes.
Una imagen es derivada de una imagen base, por ejemplo ubuntu o fedora.
Y encima de este entorno el usuario puede realizar diferentes acciones que producen otra capa.

Un ejemplo de configuración en Docker es el siguiente:

```
FROM ubuntu:latest
ADD . /data
WORKDIR /data
RUN apt-get update
RUN apt-get install -y python
EXPOSE 8080
CMD python -m SimpleHTTPServer 8080
```

Este copia el directoria actual a `/data` y crea un servidor HTTP simple que corre en el puerto 8080.

### 1.5.1.2 Puppet

Puppet es una herramienta que consiste de un servidor y multiples clientes, sobre los cuales
el servidor define una configuración.

Las distintas configuraciones son agrupadas en un llamado **manifest** y estas son guardadas
en el servidor. Estos **manifests** son los que describen el estado deseado para cada uno de los clientes.
Los clientes se conectan al servidor, obtienen los **manifests** y aplican su contenido.

El siguiente es un ejemplo de configuración en Puppet:

```
package { "apache2"
  ensure => installed
}
user { "admin"
  ensure => present
}
file {'foo' :
  path    => '/tmp/foo',
  ensure  => present,
  mode    => 0640,
  content => "Soy el archivo"
```

Si el **manifest** se aplica se asegurara de que el paquete "apache2" exista, que
el usuario "admin" esté presente y que un archivo "/tmp/foo" con el contenido "Soy un archivo"
esté presente.

### 1.5.1.3 NixOS

**NixOS**[@dolstra2008nixos], es una distribución de Linux que mejora el estado del arte en la gestión de configuración del sistema.
Nos brinda la capacidad de tener una configuración declarativa y puramente funcional, lo que permite hacer upgrades del sistema de forma confiable.

Un ejemplo de **configuración declarativa** se puede ver a continuación:

```nix
services.postgresql = {
  enable = true;
  package = pkgs.postgresql100;
  extraPlugins = [
    postgis
    pgrouting
  ];
  authentication = pkgs.lib.mkOverride 10 ''
    # Acepta Todas las conexiones IPv4 locales:
    host    all all 127.0.0.1/32 trust
  '';
  extraConfig = ''
    # Logging de todas las sentencias
    log_statement = 'all'
  '';
};
```

Con esta especificación, NixOS puede crear una instancia de PostgreSQL con las siguientes características:

- Instala la versión especificada de PostgreSQL(versión 10 en este caso).
- Está automáticamente "daemonizado"(se ejecuta indefinidamente). No hay necesidad de agregar configuraciones adicionales.
- Define una lista de extensiones requeridas. Estas ya vienen incluidas y no hay necesidad de ejecutar comandos adicionales.
- Define opciones de "logging" y autenticación. Sin necesidad de ubicar archivos adicionales.

Estas tareas involucraría varios pasos en cualquier otra distribución de Linux u otro sistema operativo.

### 1.5.2 Comparación de herramientas

\begin{table}[H]
  \center
  \begin{tabular}{| m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} |} \hline
  &
  \textbf{Puppet} &
  \textbf{Docker} &
  \textbf{NixOS} \\ \hline
  \textbf{Modelo} &
  Convergente &
  Congruente &
  Congruente \\ \hline
  \textbf{Sin divergencia} &
  Difícil &
  Garantizado &
  Garantizado \\ \hline
  \textbf{Resultados reproducibles} &
  Difícil &
  Posible &
  Garantizado \\ \hline
  \textbf{Componentes} &
  SO + Puppet en servidor y clientes &
  SO + Docker + Gestor de Contenedores &
  NixOS \\ \hline
  \textbf{Facilidad de uso} &
  Difícil &
  Fácil &
  Difícil \\ \hline
  \end{tabular}
  \caption{Comparación de herramientas de gestión de sistemas}
\end{table}

Desde el punto de vista de congruencia, NixOS y Docker(**modelo congruente**) son superiores a Puppet(**modelo convergente**) puesto
que garantizan que el resultado de la configuración sea el estado deseado.

Desde el punto de vista de componentes, NixOS es superior a Docker, puesto que Docker añade una capa de virtualización,
ciertamente es una capa mucho más ligera que una máquina virtual, sin embargo nuestra propuesta involucra **bases de datos**, las
cuales requieren uso a discreción del hardware.

## 1.6 Formulación del problema

### 1.6.1 Problema principal

La aplicación de Devops en PostgreSQL puede evitar la dependencia en un proveedor en la nube y el riesgo de vendor lock-in?

### 1.6.2 Problema secundario

Podemos ofrecer las mismas garantías en recuperación de desastres de un DbaaS con NixOS?

## 1.7 Justificación del estudio

### 1.7.3 Justificación institucional

Simply Connected Systems brinda soluciones de TI a empresas que han operado por 4 generaciones(casi 100 años).
Es de suma importancia para estas empresas garantizar la continuidad del servicio sin mayor costo operativo por el mayor
tiempo posible. Es por ello que son reacias a tercerizar su infraestructura.

### 1.7.1 Justificación tecnológica

El personal de Simply Connected Systems tiene experiencias con diversas bases de datos comerciales, como Sybase y Oracle y
conoce de procedimientos de recuperación de desastres. Sin embargo no son conocedores de las intrincancias de gestión de PostgreSQL,
es por ello que desean que esta parte les sea automatizada.

### 1.7.4 Justificación operativa

La justificación operativa es el costo de mantenimiento reducido y la reducción de la complejidad de la gestión imperativa, ad-hoc y
propensa al error.

## 1.8 Hipótesis

La aplicación de Devops en PostgreSQL puede evitar la dependencia en un proveedor en la nube y el riesgo de vendor lock-in?
Podemos ofrecer las mismas garantías en recuperación de desastres de un DbaaS con NixOS?

### 1.8.1 Hipótesis General

Si se ofrece una especificación usando la infraestructura como código se puede prescindir de los proveedores de la nube para gestionar PostgreSQL.

### 1.8.2 Hipótesis Específica

Usando la infraestructura como código, se puede ofrecer características de recuperación de desastres similares a las de un DbaaS.

## 1.9 Objetivo

### 1.9.1 Objetivo General

Aplicar la infraestructura como código usando NixOS para ofrecer las mismas garantías que ofrece un proveedor de la nube.

### 1.9.2 Objetivo Específica

Aplicar mejores prácticas de recuperación de desastres usando NixOS para asegurar la misma calidad de un DbaaS.

\newpage

# ANEXO 01: Entrevista para determinar la problemática operativa actual en la empresa Simply Connected Systems

\hfill \break

\begin{center}
  \begin{tabular}{ | l | l |} \hline
  \textbf{Nro Entrevista} & 1 \\ \hline
  \textbf{Entrevistado} & Steven Kramer \\ \hline
  \textbf{Cargo} & Chief Connection Officer \\ \hline
  \textbf{Fecha} & 2019/10/12 \\ \hline
  \end{tabular}
\end{center}

\hfill \break

## 1. A qué se dedica Simply Connected Systems?

Simply Connected Systems[^1] está basada en Coral Springs en el estado de Florida, Estados Unidos.

Nos dedicamos a integrar sistemas infórmaticos corporativos de la forma más simple. Ofrecemos soluciones informáticas móviles
no disruptivas, customizables, asequibles, sin papel, sin retipeo, que permiten a las empresas mantener sus sistemas informáticos
heredados, flujo de trabajo e información.

Nos enorgullecemos en tener como caso de éxito la integración informática de una compañía eléctrica que ha operado por
4 generaciones(casi 100 años). Nuestras soluciones están pensadas para ser de bajo mantenimiento y lo más duraderas posible.

Actualmente nos estamos enfocando en un nuevo mercado: empresas de servicios de restauración. Estas empresas reparan daños
ocasionados por desastres naturales en distintas instalaciones. Hacen esto con maquinaria que trabaja "on site", la cual
se encarga de la dehumidificación del aire, absorción de agua, etc. Esta maquinaria en ocasiones se extravia,
por lo cual estamos proveyendo un sistema de monitoreo, usando dispositivos IoT, que le permita a la empresa
identificar la posición y el estado de la maquinaria.

El desarrollo de estos sistemas es algo nuevo para nosotros y hacemos uso extenso de la nube con sus distintos proveedores.

## 2. El desarrollo de estos sistemas presenta nuevas dificultades?

Sí, existen diversas tareas que escapan a la expertiz del equipo de Simply Connected Systems. Por lo cual hacemos uso
de diversos contratistas de distintas nacionalidades. Usamos la plataforma Upwork[^2] para encontrarlos y contratarlos, esto nos
ha funcionado muy bien.

Así también, la nube ha reducido mucho nuestro costo operativo. En particular, para manejar nuestras bases de datos PostgreSQL[^3]
usamos AWS RDS[^4] y este nos ofrece muchas ventajas pero aquí viene nuestro principal problema.

Aunque nuestras soluciones basadas en la nube son suficientes para empresas pequeñas, empresas más grandes, con mayor madurez,
con politicas de gobernanza de datos y aversión al riesgo, desean alojar nuestras soluciones en infraestructura privada.

Sin embargo, AWS RDS no dispone de una versión "on premise". Para cumplir con los requisitos de estas empresas,
nuestros sistemas tendrían que replicar todas las ventajas que AWS RDS nos ofrece e instalarlos de una forma reproducible
para cada uno de nuestros clientes.

[^1]: https://www.simplyconnectedsystems.com
[^2]: https://upwork.com
[^3]: http://postgresql.org/
[^4]: https://aws.amazon.com/rds/

## 3. Una versión "on-premise" de AWS RDS solucionaría el problema?

Sí. Sabemos que AWS RDS al manejar PostgreSQL ofrece funcionalidades como: backups incrementales, recuperación en un punto de tiempo,
alta disponibilidad, etc, en las cuales muchas empresas confían para garantizar la robustez de sus bases de datos.

Sin embargo, muchas empresas han manejado a PostgreSQL exitosamente en tiempos anteriores al boom del cloud computing.
Esto es debido al gran ecosistema open source que mantiene PostgreSQL. AWS RDS admite combinar distintos componentes open source
gratuitos que el ecosistema ofrece.

Creemos que en estos tiempos de DevOps, esta tarea, antes herculeana, es ahora factible.

Esto nos permitiría replicar un sistema robusto en cualquier infraestructura, tanto pública como privada.

\newpage

# ANEXO 02: Diagrama de Ishikawa

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip

\usetikzlibrary{arrows,shapes.geometric,positioning,matrix}
\tikzset{
  ishikawa/.style={align=center, inner sep=0pt},
  matter/.style  ={rectangle, minimum size=6mm, very thick, draw=red!70!black!40,
    top color=white, bottom color=red!50!black!20, font=\itshape},
  level_1/.style ={ellipse, node distance=60pt, minimum size=6mm, very thick,
    draw=red!50!black!50, top color=white, bottom color=red!50!black!20, font=\itshape},
  level_2/.style={rectangle, minimum size=6mm, font=\itshape, font=\scriptsize}}
\tikzset{
  rows/.style 2 args={@/.style={row ##1/.style={#2}},@/.list={#1}},
  cols/.style 2 args={@/.style={column ##1/.style={#2}},@/.list={#1}},
}
\begin{tikzpicture}
\matrix[
  matrix of nodes,
  row sep=3cm,
  column sep=1cm,
  rows={1,3}{nodes=level_1},
  rows={2}{nodes=matter,anchor=center}
] (m) {
Atributos de calidad & Recursos humanos & \\
         &          & Portabilidad del sistema \\
Requerimientos  & Riesgos de la nube & \\
};
\path[very thick,
  toarr/.style={->, shorten <=+0pt, shorten >=+.1cm},
  fromarr/.style={<-, shorten >=+0pt, shorten <=+.1cm}]
  [toarr]
  (m-1-1|-m-2-3) edge (m-2-3) % mid(left to right) arrow

  (m-1-1) edge[xslant=-.5]
    coordinate[pos=.3]   (@-1-1-1)
    coordinate[near end] (@-1-1-2) (m-1-1|-m-2-3)
  [fromarr]
  (@-1-1-1) edge node[above,level_2]{Alta disponibilidad} ++ (left:2.9cm)
  (@-1-1-2) edge node[above,level_2]{Alta confiabilidad} ++ (left:2.7cm)

  [toarr]
  (m-1-2) edge[xslant=-.5]
    coordinate[pos=.3]   (@-1-2-1)
    coordinate[near end] (@-1-2-2) (m-1-2|-m-2-3)
  [fromarr]
  (@-1-2-1) edge node[above,level_2]{Carencia de DBA} ++ (left:2.5cm)
  (@-1-2-2) edge node[above,level_2]{Dependencia en contratistas} ++ (left:3.8cm)

  [toarr]
  (m-3-1) edge[xslant=.5]
    coordinate[pos=.3]   (@-3-1-1)
    coordinate[near end] (@-3-1-2) (m-3-1|-m-2-3)
  [fromarr]
  (@-3-1-1) edge node[above,level_2]{Infraestructura privada} ++ (left:2.8cm)

  [toarr]
  (m-3-2) edge[xslant=.5]
    coordinate[pos=.3]   (@-3-2-1)
    coordinate[near end] (@-3-2-2) (m-3-2|-m-2-3)
  [fromarr]
  (@-3-2-1) edge node[above,level_2]{Vendor lock-in} ++ (left:2cm)
  (@-3-2-2) edge node[above,level_2]{Dependencia en RDS} ++ (left:2.5cm)
;
\end{tikzpicture}

## ANEXO 03: Matriz de consistencia

\begin{table}[H]
  \begin{tabular}{ | m{0.25\textwidth} | m{0.25\textwidth} | m{0.25\textwidth} | m{0.25\textwidth} |}
  \hline
  \textbf{PROBLEMA} &
  \textbf{OBJETIVOS} &
  \textbf{HIPÓTESIS} &
  \textbf{VARIABLES E INDICADORES} \\
  \hline
  \textbf{General} &
  \textbf{General} &
  \textbf{General} &
  \textbf{Variable Independiente} \\
  \hline
  La aplicación de Devops en PostgreSQL puede evitar la dependencia en un proveedor en la nube y el riesgo de vendor lock-in?  &
  Aplicar la infraestructura como código usando NixOS para ofrecer las mismas garantías que ofrece un proveedor de la nube &
  Si se ofrece una especificación usando la infraestructura como código se puede prescindir de los proveedores de la nube para gestionar PostgreSQL. &
  Devops aplicado a PostgreSQL \\
  \hline
  \textbf{Específico} &
  \textbf{Específico} &
  \textbf{Específico} &
  \textbf{Variable Dependiente} \\
  \hline
  Podemos ofrecer las mismas garantías en recuperación de desastres de un DbaaS con NixOS? &
  Aplicar mejores prácticas de recuperación de desastres usando NixOS para asegurar la misma calidad de un DbaaS.  &
  H1.  Usando la infraestructura como código, se puede ofrecer características de recuperación de desastres similares a las de un DbaaS.  &
  Mitigación de riesgos en la nube \newline\newline
  \textbf{Indicadores:} \newline
  \begin{itemize}
    \item Punto de recuperación objetivo(RPO)
    \item Tiempo de recuperación objetivo(RTO)
  \end{itemize} \\
  \hline
  \end{tabular}
  \caption{Matriz de consistencia}
\end{table}

\newpage

# Bibliografía
