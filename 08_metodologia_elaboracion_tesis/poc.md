---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{0pt}
- \usepackage{titling}
- \usepackage{array}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{float}
- \usepackage{listings}
---


\title{Administración autónoma de bases de datos para mitigación de riesgos de aprisionamiento tecnológico en la nube\vspace{2.0cm}}
\author{\textbf{Steve Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Orientador: Prof. Nehil Muñoz Casildo}

Informe de Tesis, presentada al curso de Proyecto de Tesis, \linebreak como requisito para la sustentación de ciclo.

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Mayo 2019}
\end{center}

\thispagestyle{empty}

\newpage

## Abreviaturas

\bigskip

- **IaaS**: Infrastructure as a Service(Infraestructura como servicio)
- **DBaaS**: Database as a Service(Base de datos como servicio)
- **AWS**: Amazon Web Services
- **RDS**: Relational Database as a Service(Base de datos relacional como servicio)
- **AZ**:  Availability Zone(zona de disponibilidad)
- **VPN**: Virtual Private Network
- **VPC**: Virtual Private Cloud(término que usa AWS para referirse a un VPN)
- **DR**: Disaster Recovery(recuperación de desastres)
- **RPO**: Recovery Point Objective(punto de recuperación objetivo)
- **RTO**: Recovery Time Objective(tiempo de recuperación objetivo)
- **SGBD**: Sistema gestor de base de datos
- **DBA**: Database Administrator(administrador de base de datos)
- **SO**: Sistema Operativo

\setcounter{page}{1}

\newpage

## Taxonomia

\bigskip

CCS / Information systems / Data management systems / Database administration / Autonomous database administration

\newpage

## Agradecimientos

\bigskip

Agradezco a la Universidad Nacional Mayor de San Marcos, mi *alma mater*, y a sus profesores,
por haberme formado y dado la oportunidad de culminar la carrera de Ingeniería de Sistemas.

\newpage

## Resumen

Actualmente es tendencia la migración de infraestructuras de TI a la nube.
Esto es debido a la gran conveniencia y reducción de costos que ofrecen los distintos proveedores de cloud computing.
En particular los modernos DbaaS, proveen automatización de tareas administrativas de mantenimiento de base de datos.

Sin embargo, los servicios en la nube en general cuentan con muchos riegos:
Desde pérdida de los datos hasta subidas drásticas de precio por parte del proveedor.
Una forma conocida de mitigar estos riesgos es no hacerse dependiente de un solo proveedor y
usar solo los servicios de IaaS. El problema al hacer esto, es que se pierde el valor agregado del DbaaS.

La propuesta de esta tesis, es proveer la conveniencia de un DbaaS, la administración autonóma, sobre una infraestuctura multi nube,
usando una herramienta open source que puede ser reutilizada, customizada de acuerdo a la necesidad y desplegada sobre cualquier IaaS.
Evitando así la pérdida de gobernabilidad sobre los datos, el riesgo de aprisionamiento tecnológico y a su vez aumentando
la robustez de la infraestructura.

## Abstract

Currently, there's a tendency of migrating IT infrastructures to the cloud.
This is due the convenience and costs reduction that cloud computing vendors offer.
DbaaS in particular, provides the automation of database administrative tasks.

However, cloud computing services in general come with many risks: from data loss to vendors pricing models.
One way to mitigate these risks is to not become dependent on a single vendor and only use IaaS.
The problem with this is that the added value of DbaaS is lost.

This thesis proposal, is to provide the DbaaS convenience, that is the autonomous adminstration, over a multi-cloud infrastructure,
by using an open source tool that can be reutilized and customed according to need.
This way, there's no loss in data governance and vendor lock-in risk is avoided and at the same time infrastructure resillience
is increased.

\newpage

\renewcommand*\contentsname{Índice general}
\tableofcontents

\newpage

\renewcommand{\listtablename}{Índice de tablas}
\listoftables

\newpage

\renewcommand{\listfigurename}{Índice de figuras}
\listoffigures

\newpage

# 1. Introducción

En la ya madura era de la información, es conocido que para una empresa
u organización, los datos son el activo más valioso. Sin embargo, mantener
una infraestructura, ya sea local o en la nube como IaaS, capaz de manejar
grandes volúmenes de datos es un trabajo de mucho costo para cualquier empresa,
en especial si la empresa no se especializa en TI. Esto es independiente de la
tecnología de base de datos que se use(SQL o NoSQL).

Es por esto que diversos proveedores de la nube(AWS, GCP, Azure, etc), ofrecen
servicios de cloud computing conocidos como DBaaS, que autogestionan bases de datos
en diversas escalas. En esencia un modelo de **leasing** para bases de datos.

Estos DBaaS ofrecen una gran conveniencia para gestionar las bases de datos;
sin embargo se cuenta con muchos riesgos al hacerse dependiente
de un servicio en la nube, más aún si solo se depende de un solo proveedor.
Se ahondará más sobre estos riesgos en el **Estado del arte**.

## 1.1. Planteamiento del Problema

Para aprovechar el costo de reducido que ofrece algun proveedor de la nube y a
la vez mitigar los riegos mencionados, la mejor opción es usar al proveedor solo
como servicio de IaaS(se ahondará más sobre el término en el **Marco Teórico**).
De esta forma es posible mover la infraestructura TI de proveedor a proveedor si se requiere. El
problema que viene al hacer esto, es que se pierde la conveniencia del servicio
del proveedor que gestiona la base de datos.

## 1.2. Motivación y Contexto

Es de suma importancia a nivel empresarial y más aún a nivel gubernamental,
proveer a los equipos de TI de herramientas que les permita gestionar su propia
infraestructura y controlar su propia información.

Cuál sería la postura de negociación que podría tomar una empresa si luego de
10 años de operación toda su información se encuentra asilada en el servicio de
determinado proveedor? Podrá migrar fácilmente si es que su actual proveedor
aumenta los precios drásticamente?

No. Es seguro que la empresa estará ante un caso de aprisionamiento tecnológico(vendor lock-in).

## 1.3. Objetivos

En esencia lo que buscamos **evitar** el riesgo de aprisionamiento tecnológico y **mitigar**
riesgos de pérdida de datos.

Nos enfocaremos en el gestor de base de datos PostgreSQL, por ser el gestor open source más maduro actualmente.

Se implementará una herramienta que autogestione un clúster de PostgreSQL. Garantizando integridad de la data(backups),
y la alta disponibilidad ofrecida por los actuales DbaaS del mercado. Se ahondará más en la propuesta en el **Estado del arte**.

## 1.4. Organización de la tesis

En el capítulo 2 se verá una identificación de **riesgos del cloud computing** en general.

En el capítulo 3 se verán los **estado del arte** sobre las características que ofrecen los DbaaS actuales.

En el capítulo 4 presentaremos nuestra propuesta sobre **administración autónoma de bases de datos**,
qué tareas administrativas comprenderá y cuál será el alcance de esta propuesta.

En el capítulo 5 se verá cómo se hizo el **planeamiento del estado del arte**.
Se mostrarán las actividades principales, vistas a gran nivel como una caja negra y a bajo nivel como caja blanca,
usando la notación IDEF0.

## 1.5. Cronograma

![Cronograma](images/cronograma.png)

\newpage

# 2. Marco teórico

Empezaremos definiendo qué es cloud computing, de acuerdo a @mell2011nist, listando sus categorías ya tradicionales para luego llegar
a la más reciente subcategoría de DbaaS.

## 2.1. Cloud computing

Cloud computing es un modelo que habilita un acceso on-demand, ubicuo y conveniente a un pool compartido de recursos computacionales
que puede ser rápidamente provisto con un esfuerzo mínimo de configuración e interacción con el proveedor del servicio.

Ahora veremos las categorías o modelos de servicio tradicionales:

### 2.1.1 Infrastructure as a Service(IaaS)

La capacidad provista al consumidor es la de poder aprovisionar procesamiento, almacenamiento, redes y otros recursos computacionales
fundamentales donde el consumidor es capaz de desplegar y correr software arbitrario, que puede incluir sistemas operativos
y aplicaciones. El consumidor no controla la infraestructura(hardware) pero tiene control sobre el sistema operativo, almacenamiento,
aplicaciones desplegadas y posiblemente la configuración de red.

### 2.1.2 Platform as a Service(PaaS)

La capacidad provista al consumidor es el despliegue a la infraestructura del proveedor. Pero solo de aplicaciones creadas usando
lenguajes de programación, librerías, servicios y herramientas soportadas por el proveedor. El consumidor no maneja
o controla la infraestructura incluyendo la configuración de red, servidor, sistema operativo y almacenamiento, pero tiene control
sobre las aplicaciones desplegadas y posiblemente sobre las configuraciones de la aplicación desplegada.

### 2.1.3 Software as a Service(SaaS)

La capacidad provista al consumidor es el uso de aplicaciones alojadas en la infraestructura del proveedor. Las aplicaciones son
accesibles desde varios dispositivos clientes a través de una interfaz ligera, como un navegador web o la interfaz de un programa.
El consumidor no maneja o controla la infraestructura del proveedor, incluyendo su configuración de red, servidores, sistemas
operativos, almacenamiento o incluso capacidades individuales de las aplicaciones, con la posible excepción de algunas configuraciones
específicas al usuario.

### 2.1.4 Database as a Service(DbaaS)

De acuerdo a @Curino2011RelationalCA, el DbaaS mueve la carga operacional del aprovisionamiento, configuración,
escalado, afinación de performance, políticas de backup y control de acceso de la mano de los usuarios de la base de datos
hacia la mano del proveedor del servicio, reduciendo así los costos al usuario. Siendo Amazon RDS y
Microsoft SQL Azure los pioneros en ofrecer DbaaS.

En esencia, DbaaS es una especialización del SaaS que ofrece un modelo de leasing o outsourcing para Sistemas Gestores de Bases de Datos.

Para poder ahondar en los riesgos del cloud computing, veremos ahora una base de la Gestión de Riesgos según @Sommerville:2010:SE:1841764.

## 2.2 Gestión de Riesgos

La gestión del riesgo implica anticipar riesgos que pudieran alterar el calendario del proyecto o
la calidad del software a entregar, y posteriormente tomar acciones para evitar dichos riesgos.

### 2.2.1 Tipos de riesgos

Tenemos 3 tipos de riesgos:

1. Riesgos del proyecto: Los riesgos que alteran el calendario o los recursos del proyecto.

2. Riesgos del producto: Los riesgos que afectan la calidad o el rendimiento del software a desarrollar.

3. Riesgos empresariales: Los riesgos que afectan la organización que desarrolla o adquiere el software.

En el caso del cloud computing estamos ante un **riesgo del proyecto** y **riesgo del producto**.

### 2.2.2 Proceso de Gestión de Riesgos

Para evaluar los riesgos necesitamos seguir las siguientes etapas:

1. Identificación del riesgo: La enumeración de los posibles riesgos para el proyecto, producto y empresa.

2. Análisis de riesgos: Se debe valorar la probabilidad y las consecuencias de dichos riesgos.

3. Planeación del riesgo: Elaborar planes para enfrentar el riesgo, evitarlo o minimizar sus efectos en el proyecto.

4. Monitorización del riesgo: Valorar regularmente el riesgo y los planes para atenuarlo, y revisarlos cuando se aprenda más
sobre el riesgo.

Afortunadamente para nuestro caso sobre cloud computing, ya existe un desarrollo de la **identificación** y **análisis** de riesgos el cual
desarrollaremos en la sección del estado del arte.

Lo cual nos deja con la mayoría de trabajo para la **Planeación del riesgo**, la cual tiene 3 tipos de estrategias:

- Estrategias de evitación: reducir la probabilidad de que surja el riesgo.

- Estrategias de minimización: reducir el efecto del riesgo.

- Planes de contingencia: prepararse pra el mayor impacto del riesgo y tener una estrategia para hacer frente a ello.

## 2.3 Conceptos generales de Bases de datos

Para entender la propuesta primero vamos a esclarecer conceptos generales de bases de datos.

### 2.3.2 Transacciones y propiedades ACID

Una transaccion es una serie de acciones, estas pueden ser lecturas o escrituras
a objetos de la base de datos.

Las transacciones deben soportar 4 propiedades. Conocidas con el acrónimo ACID.

1. Atomicidad: las operaciones de todas las transacciones ocurren como una unidad indivisible.
2. Consistencia: las transacciones deben mantenter a la base de datos en un estado consistente, de acuerdo a las condiciones definidas.
3. Aislamiento: La ejecución de una transacción está aislada una de otra.
4. Durabilidad: Si una transacción finaliza, sus efectos persiten. Incluso en el caso que falle el sistema.

### 2.3.1 Replicación

La replicación en bases de datos es usada para dos propósitos: mejorar la performance del sistema y
mejorar la tolerancia a fallas.

Según @chouk2003master estas estrategias pueden dividirse en 4 dependiendo de cuándo ocurre la replicación y en qué nodos ocurre.

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.3\textwidth} | m{0.3\textwidth} |}
  \hline
  &
  \textbf{Síncrona} &
  \textbf{Asíncrona} \\
  \hline
  \textbf{Copia primaria (Maestro)} &
  Las escrituras solo están permitidas en \textbf{un solo nodo}. \newline \newline
  Las escrituras se propagan \textbf{antes} que la transacción finalize. &
  Las escrituras solo están permitidas en \textbf{un solo nodo}. \newline \newline
  Las escrituras se propagan \textbf{después} que la transacción finalize. \\
  \hline
  \textbf{Todos los nodos (Multi-Maestro)} &
  Las escrituras están permitidas en \textbf{todos los nodos}. \newline \newline
  Las escrituras se propagan \textbf{antes} que la transacción finalize. &
  Las escrituras están permitidas en \textbf{todos los nodos}. \newline \newline
  Las escrituras se propagan \textbf{después} que la transacción finalize. \\
  \hline
  \end{tabular}
  \caption{Estrategias de replicación}
\end{table}

### 2.3.2 Particionamiento horizontal o Sharding

Cuando se trata de escalar el almacenamiento y el throughput que puede soportar la base de datos, una opción inmediata es añadirle más
recursos al servidor, sin embargo no se puede escalar de esta manera de forma indefinida.

Otra opción es usar **sharding**, este es un proceso en el cual se particiona las filas de una tabla en múltiples bases de datos
independientes.

Según @Sitaram_2012 existen 3 técnicas de particionamiento:

### 2.3.3 Particionamiento por Round Robin

Este distribuye las filas de forma que las filas de la primera transacción van en la primera base de datos,
la segunda transacción en la segunda base de datos y así sucesivamente.

La ventaja de esta técnica es la simplicidad, sin embargo se pierden las asociaciones.
Si se requieren hacer consultas usando JOINs, se requiere hacerlo en multiples bases de datos, lo cual resulta complejo e ineficiente.

Los siguientes dos técnicas no sufren de la desventaja de perder las asociaciones.

### 2.3.4 Particionamiento por Hash

En esta técnica, el valor de un atributo selecionado es "hasheado" para encontrar la base de datos
en la cual la fila será guardada. Si las conslutas son hechas frecuentemente en un atributo,
por ejemplo `id_cliente` entonces las asociaciones son preservadas usando este atributo,
puesto que las filas con el mismo valor del atributo pueden ser encontrados en la misma base de datos.

### 2.3.5 Particionamiento por Rango

En esta técnica se guardan filas con atributos similar en la misma base de datos.
Por ejemplo el rango de `id_cliente` puede ser dividido en diferentes de bases de datos.

Una técnica generalizada es el `Particionamiento por Lista` la cual busca
una combinación de atributos de la fila en un directorio para encontrar la partición a la cual pertenece.

![](images/sharding.png)

### 2.3.6 Recuperación de desastres(Disaster Recovery)

El objetivo de la recuperación de desastres es restaurar la base de datos fallida al
estado más reciente y consistente. Para esto tenemos dos métricas:

### 2.3.7 Punto objetivo de recuperación(RPO)

Es la cantidad máxima de datos que se pueden perder cuando se restablece el servicio después de una interrupción.
El punto de recuperación objetivo se expresa en un período de tiempo antes de la falla.
Por ejemplo, un punto de recuperación objetivo de un día puede ser apoyado por los backups diarios, y donde se pueden perder datos
de hasta 24 horas.

Cero es un valor válido y es equivalente a un requisito de "pérdida de datos cero".

### 2.3.8 Tiempo objetivo de recuperación(RTO)

Es el tiempo máximo permitido para la recuperación de un servicio después de una interrupción.
El nivel de servicio que se presta es inferior al objetivo de nivel de servicio normal.

Un RTO ideal para cloud computing viene a ser menor a 1 minuto.

## 2.4 DevOps e Infraestructura como Código

Para entender cómo pensamos obtener la **administración autónoma** veremos qué es **DevOps** y **Infraestructura como Código**, según @Artac_2017.

**DevOps** comprende una serie de tácticas de ingeniería de software que apuntan a
reducir el tiempo accionable de cambio de diseño de software.

Una de estas tácticas es la **Infraestructura como código** que implica definir un modelo
que contiene specificaciones listas para la orquestación en la nube.

## 2.4 Gestión de sistemas

A continuación veremos unos modelos de gestión de sistemas que nos permitirán evaluar las diferentes herramientas
de **Infraestructura como código** que nos ofrece el mercado.

De acuerdo a @Traugott:2002:WOM:1050517.1050529 existen 3 modelos de gestión de sistemas:

\begin{figure}%
  \centering
  \subfloat[Divergente]{{\includegraphics{images/divergence.png} }}%
  \qquad
  \subfloat[Convergente]{{\includegraphics{images/convergence.png} }}%
  \qquad
  \subfloat[Congruente]{{\includegraphics{images/congruence.png} }}%
  \caption{Modelos de gestión de sistemas}%
\end{figure}

\newpage

### 2.4.1 Modelo divergente

La divergencia es caracterizada por la configuración del sistema alejándose del estado ideal del sistema.

Esto es ilustrado en el flujo de trabajo típico de un sysadmin, el cual sigue un checklist y manualmente ejecuta paso por paso los comandos
necesarios para alcanzar el estado del sistema deseado. Si 3 distintos sysadmins siguen el mismo checklist,
es probable que se obtengan 3 resultados distintos.

Este viene a ser el modelo tradicional, donde se usan scripts bash para automatizar algún despliegue. Esto no es considerado
**Infraestructura como código**, puesto que sigue un enfoque imperativo y no declarativo.

### 2.4.2 Modelo convergente

La convergencia es caracterizada por la configuración del sistema acercándose al estado ideal del sistema.

Esta es una mejora sobre el modelo divergente, en este caso se usan herramientas más maduras
que permiten declarar una descripción del sistema. Algunas herramientas actuales del mercado son: **Puppet**, **Ansible**.
El problema es que estás herramientas aún pueden diverger del estado ideal, es necesario
monitorear las diferencias entre la descripción del sistema y estado ideal deseado.

### 2.4.3 Modelo congruente

La congruencia es la práctica de mantener la configuración del sistema en completa igualdad al estado ideal del sistema.

Este modelo es caracterizado por herramientas actuales como **contenedores**(Docker, LXC) o **NixOS**(@dolstra2008nixos).
En estos casos el estado real del sistema es un espejo de la descripción del sistema.

# 2. Estado del arte

En esta sección veremos:

- Los **riesgos del cloud computing** que queremos evitar al no usar un DbaaS.

- El **estado ideal** que debería tener nuestro sistema y para esto nos basaremos en las funcionalidades
  ofrecidas por el DbaaS líder del mercado actual: AWS RDS.

- **Cómo llegaremos a este estado ideal** con **Infraesctructura de código** usando un **modelo congruente** de gestión de sistemas.

## 2.1 Riesgos del cloud computing

A continuación se mostrarán los riesgos identificados en _Risks in enterprise cloud computing_[@dutta2013risks].

### 2.1.1 Identificación de riesgos

![Ontología de riesgos](images/risks-ontology.png){ width=80% }

\newpage

### 2.1.2 Análisis de riesgos

De estos riesgos, los principales obtenidos por la encuesta realizada por [@dutta2013risks] son:

\begin{table}[H]
  \begin{tabular}{ | m{0.1\textwidth} | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  \textbf{Ranking} &
  \textbf{ID Riesgo} &
  \textbf{Descripcion} &
  \textbf{Score del Riesgo} \\
  \hline
  1 &
  LR1.1 &
  La privacidad de la empresa o data del cliente es puesta en riesgo en la nube &
  153.50 \\
  \hline
  2 &
  LR1.3 &
  Leyes inconsistentes de protección de datos en los distintos países donde la data se genera y se guarda. &
  151.75 \\
  \hline
  3 &
  OGR4.2 &
  Dificultad para cambiar de proveedor de la nube incluso en caso de insatisfacción con el servicio(vendor lock-in) &
  148.50 \\
  \hline
  4 &
  OGR5.2 &
  Las compañías usuarias carecen de recuperación de desastres y planes de contingencia para lidiar con fallas
  técnicas inesperadas en la nube &
  147.75 \\
  \hline
  5 &
  LR3.2 &
  Dificultad de migración de la empresa al término del contrato con el proveedor de la nube &
  140.25 \\
  \hline
  6 &
  OPR4.2 &
  Inadecuado entrenamiento y/o conocimiento de los servicios de la nube. &
  139.75 \\
  \hline
  7 &
  OPR5.1 &
  Las aplicaciones en la nube temporalmente caen temporalmente en fuera de servicio. &
  137.25 \\
  \hline
  8 &
  OPR2.1 &
  Costos ocultos que se incrementan debido a modos de operacion no transparentes en la nube. &
  136.00 \\
  \hline
  9 &
  TR4.3 &
  Ataques de denegación de servicio a un proveedor de la nube. &
  135.50 \\
  \hline
  10 &
  TR4.1 &
  Acceso no authorizado a data empresarial en la nube. &
  135.00 \\
  \hline
  \end{tabular}
  \caption{Riesgos principales del Cloud Computing}
\end{table}

### 2.1.3 Planeación de riesgos

Con nuestra propuesta:

- **Evitaremos** los riesgos relacionados al vendor lock-in(2, 3, 5 y 8), puesto que el usuario/s podrá elegir y cambiar de proveedor.

- Con el enfoque multi nube, se podrán **minimizar** los riesgos 1, 7, 9 y 10

- La herramienta generada, al ser de código libre, tiene también un fin didáctico y **minimizará** los riesgos 4 y 6.

- **Plan de contingencia**: Si es que algún proveedor de la nube sufriese una falla total, con el enfoque multi-nube se puede cambiar a otro proveedor.

### 2.1.4 Monitorización de riesgos

La herramienta proveerá de dashboards de monitoreo de los atributos de calidad de la base de datos, los cuales permitirán reevaluar los
riesgos constantemente.

### 2.1.3 Consideraciones Finales

Los riesgos identificados son la principal motivación de esta propuesta.
Lo que nos lleva a estudiar los reales beneficios que los DbaaS proveen.

## 2.2 Cuadrante mágico para Cloud Computing de Gartner

De acuerdo a Gartner @leong2018magic, el actual líder del mercado en cloud computing viene a ser AWS.

\begin{figure}%
  \centering
  \includegraphics{images/magic-q.png}
  \caption{Cuadrante Mágico de Gartner al 2018}%
\end{figure}

Elegiremos a este actual líder para obtener las funcionalidades deseadas de un DbaaS.

## 2.2 Funcionalidades de AWS RDS

Las siguientes son las funcionalidades más importantes de AWS RDS(Relational Database as a Service).

Nosotros nos enfocaremos en PostgreSQL, pero RDS en realidad ofrece la misma capacidad de gestión para distintos gestores de bases
de datos.

### 2.2.1 Aprovisionamiento de recursos automático

RDS hace una elección de discos duros o discos de estado sólido y dispostivos de red para garantizar el óptimo rendimiento
de la base de datos. Además configura el sistema operativo con optimizaciones hechas a medida para un SGBD en particular.

### 2.2.2 Seguridad

RDS ubica las bases de datos en VPCs, protegiéndolas del acceso público. Además ofrece cifrado SSL para el intercambio de datos.

### 2.2.3 Backups automáticos

RDS ofrece backups automáticos, garantizando un RPO y RTO óptimo.

### 2.2.4 Alta disponibilidad

RDS ofrece replicas en distintas zonas de disponibilidad(AZs), estas están distribuidas geográficamente.

### 2.2.5 Escalabilidad con mínimo tiempo inactivo por mantenimiento

RDS permite escalar los recursos que usa la base de datos mientras está en uso, con mínimo tiempo inactivo(downtime).
Esto lo hace replicando una instancia de la base de datos mientras otra es promovida.

### 2.2.6 Tareas administrativas no automatizadas

Ningún DbaaS actualmente automatiza la creación de índices(auto indexación) o migraciones(cambio de esquemas).
Por tanto es aún necesaria la presencia de un DBA para un manejo efectivo de la base de datos.

### 2.2.7 Consideraciones Finales

Históricamente, todas estas tareas autogestionadas for RDS han sido realizadas manualmente y de _forma imperativa_ por un DBA.
Cuando decimos _forma imperativa_ nos referimos a secuencias de comandos UNIX ad hoc, ejecutados para gestionar la base de datos.
La _forma imperativa_ es una de las razones por la cual no se ha desarrollado el enfoque de administración autónoma.

## 2.3 Atributos de Calidad de un DbaaS

Según @lehner2010database, los DbaaS deben poseer los siguientes atributos de calidad:

### 2.3.1 Consistencia y Fiabilidad

La consistencia de los datos es una propiedad sumamente importante para los requisitos una aplicación de misión crítica.
Esto es ya es garantizado por las propiedades ACID de un SGBD. Sin embargo en entornos altamente distribuidos es un reto
garantizar la consistencia.

La fiabilidad puede ser lograda con redundacia, para esto se pueden usar técnicas de replicación.

### 2.3.2 Seguridad y Confidencialidad

Ya que los DbaaS representan un outsourcing de datos, es necesario que la confidencialidad y seguridad de ellos sea garantizada.

### 2.3.3 Previsibilidad y QoS(Calidad de servicio)

Puesto que al usar un DbaaS se hace también un outsourcing de los recursos de hardware, es necesario que se brinden garantías
sobre las propiedades no funcionales. Por esto los DbaaS proveen un SLA(Acuerdo de nivel de servicio).

### 2.3.4 Escalabilidad elástica

Adicionalmente, de acuerdo a @curino2011relational, un buen DbaaS debe soportar cargas de trabajo de distinta magnitud. El reto surge
cuando la base de datos excede la capacidad de una sola máquina. Por tanto un DbaaS debe soportar "escalar hacia afuera", lo
cual permite distribuir la carga en más de una máquina.

## 2.3 Comparación de SGBDs open source

### 2.3.1 Ranking db-engines 2019

Primero veamos los gestores de bases de datos más populares mundialmente, según @solid2019db.

\begin{table}[H]
  \center
  \begin{tabular}{ | l | l | r |}
  \hline
  \textbf{Ranking} &
  \textbf{SGBD} &
  \textbf{Score} \\
  \hline
  1 &
  Oracle &
  1321.26 \\
  \hline
  2 &
  MySQL &
  1229.52 \\
  \hline
  3 &
  Microsoft SQL Server &
  1090.83 \\
  \hline
  4 &
  PostgreSQL &
  483.28 \\
  \hline
  \end{tabular}
  \caption{Ranking de db-engines al 2019}
\end{table}

### 2.3.2 PostgreSQL vs MySQL

Tanto Oracle como SQL Server solo ofrecen licencias comerciales y no son open source, lo cual las descalifica para nuestra propuesta.
Requerimos que la herramienta sea open source para poder extenderla si es necesario. Además que estos SGBDs comerciales
van a favor del riesgo de vendor lock-in, que justamente buscamos combatir.

Eso nos deja a PostgreSQL y MySQL, sobre los cuales haremos un cuadro comparativo.

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.4\textwidth} | m{0.4\textwidth} |}
  \hline
  &
  \textbf{PostgreSQL} &
  \textbf{MySQL} \\
  \hline
  \textbf{Gobernanza} &
  Es libre y open source y tiene la licencia PostgreSQL similar a la licencia MIT &
  Aunque es open source. El desarrollo es manejado por Oracle, el cual ofrece versiones pagadas para uso comercial \\
  \hline
  \textbf{Conformidad con SQL} &
  Ofrece total conformidad con el standard SQL &
  Parcialmente conforme con SQL, no implementa todo el standard(por ejemplo CHECK constraints) \\
  \hline
  \textbf{Comunidad} &
  Comunidad madura que sigue contribuyendo y aumentando funcionalidades de forma estable &
  Comunidad grande que se enfoca en mantener funcionalidades pasadas y raramente añade nuevas\\
  \hline
  \textbf{Replicación} &
  Incluye replicación maestro-esclavo y otros tipos de replicación son soportadas a través de extensiones. &
  Incluye replicación maestro-esclavo y maestro-maestro. \\
  \hline
  \textbf{Soporte NoSQL/JSON} &
  Múltiples soportados(JSON, JSONB, hstore, grafos a través de extensiones) &
  Solo JSON \\
  \hline
  \textbf{Sistema de Tipos extensible} &
  Su arquitectura soporta la extensibilidad. Es posible añadir nuevos tipos de datos, tipos de índice, etc. &
  No soporta extensibilidad. \\
  \hline
  \textbf{Extensible con lenguajes de programación} &
  Soporta extensibilidad en variedad de lenguajes, incluyendo python, java, javascript, .net, R, ruby y otros. &
  Solo soporta C, y su arquitectura no está diseñada para la extensibilidad. \\
  \hline
  \end{tabular}
  \caption{Comparación de PostgreSQL y MySQL}
\end{table}

Concluimos eligiendo PostgreSQL, puesto que presenta una arquitectura madura y sigue evoluacionando gracias a su extensibilidad.
Mientras MySQL se enfoca más en estabilizar sus funcionalidades presentes y su desarrollo se nota estancado.

## 2.3 Herramientas de gestión de sistemas

Ahora veremos las herramientas actuales que permiten gestionar la configuración del sistema a través de la Infraestructura como
Código.

\begin{figure}[H]
  \centering
  \includegraphics{images/iac-tools.png}
  \caption{Herramientas de gestión de sistemas}%
\end{figure}

### 2.3.1 NixOS

**NixOS**[@dolstra2008nixos], es una distribución de Linux que mejora el estado del arte en la gestión de configuración del sistema.
Nos brinda la capacidad de tener una configuración declarativa y puramente funcional, lo que permite hacer upgrades del sistema de forma confiable.

Un ejemplo de **configuración declarativa** se puede ver a continuación:

```nix
services.postgresql = {
  enable = true;
  package = pkgs.postgresql100;
  extraPlugins = [
    postgis
    pgrouting
  ];
  authentication = pkgs.lib.mkOverride 10 ''
    # Acepta Todas las conexiones IPv4 locales:
    host    all all 127.0.0.1/32 trust
  '';
  extraConfig = ''
    # Logging de todas las sentencias
    log_statement = 'all'
  '';
};
```

Con esta especificación, NixOS puede crear una instancia de PostgreSQL con las siguientes características:

- Instala la versión especificada de PostgreSQL(versión 10 en este caso).
- Está automáticamente "daemonizado"(se ejecuta indefinidamente). No hay necesidad de agregar configuraciones adicionales.
- Define una lista de extensiones requeridas. Estas ya vienen incluidas y no hay necesidad de ejecutar comandos adicionales.
- Define opciones de "logging" y autenticación. Sin necesidad de ubicar archivos adicionales.

Estas tareas involucraría varios pasos en cualquier otra distribución de Linux u otro sistema operativo.

### 2.3.1 Puppet

Puppet es una herramienta que consiste de un servidor y multiples clientes, sobre los cuales
el servidor define una configuración.

Las distintas configuraciones son agrupadas en un llamado **manifest** y estas son guardadas
en el servidor. Estos **manifests** son los que describen el estado deseado para cada uno de los clientes.
Los clientes se conectan al servidor, obtienen los **manifests** y aplican su contenido.

El siguiente es un ejemplo de configuración en Puppet:

```
package { "apache2"
  ensure => installed
}
user { "admin"
  ensure => present
}
file {'foo' :
  path    => '/tmp/foo',
  ensure  => present,
  mode    => 0640,
  content => "Soy el archivo"
```

Si el **manifest** se aplica se asegurara de que el paquete "apache2" exista, que
el usuario "admin" esté presente y que un archivo "/tmp/foo" con el contenido "Soy un archivo"
esté presente.

### 2.3.1 Docker

Docker es una herramienta que permite crear entornos llamados contenedores,
compartir estos y correr aplicaciones aisladas en ellos.

El concepto de contenedores es similar a las máquinas virtuales.
Mientras que una máquina virtual ejecuta recursos virtualizados, un contenedor
comparte los recursos con su host.

Docker usa distintas técnicas ofrecidas por el kernel de Linux, para aislar
y abstrar el contenedor del host.

Otro aspecto importante de Docker es que permite compartir y distribuir imágenes.
Una imagen es derivada de una imagen base, por ejemplo ubuntu o fedora.
Y encima de este entorno el usuario puede realizar diferentes acciones que producen otra capa.

Un ejemplo de configuración en Docker es el siguiente:

```
FROM ubuntu:latest
ADD . /data
WORKDIR /data
RUN apt-get update
RUN apt-get install -y python
EXPOSE 8080
CMD python -m SimpleHTTPServer 8080
```

Este copia el directoria actual a `/data` y crea un servidor HTTP simple que corre en el puerto 8080.

### 2.3.4 Comparación de herramientas

\begin{table}[H]
  \center
  \begin{tabular}{| m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} |} \hline
  &
  \textbf{Puppet} &
  \textbf{Docker} &
  \textbf{NixOS} \\ \hline
  \textbf{Modelo} &
  Convergente &
  Congruente &
  Congruente \\ \hline
  \textbf{Sin divergencia} &
  Difícil &
  Garantizado &
  Garantizado \\ \hline
  \textbf{Resultados reproducibles} &
  Difícil &
  Posible &
  Garantizado \\ \hline
  \textbf{Componentes} &
  SO + Puppet en servidor y clientes &
  SO + Docker + Gestor de Contenedores &
  NixOS \\ \hline
  \textbf{Facilidad de uso} &
  Difícil &
  Fácil &
  Difícil \\ \hline
  \end{tabular}
  \caption{Comparación de herramientas de gestión de sistemas}
\end{table}

Desde el punto de vista de congruencia, NixOS y Docker(**modelo congruente**) son superiores a Puppet(**modelo convergente**) puesto
que garantizan que el resultado de la configuración sea el estado deseado.

Desde el punto de vista de componentes, NixOS es superior a Docker, puesto que Docker añade una capa de virtualización,
ciertamente es una capa mucho más ligera que una máquina virtual, sin embargo nuestra propuesta involucra **bases de datos**, las
cuales requieren uso a discreción del hardware.

# 3. Prueba de Concepto

## 3.1 Objetivo de la Prueba de Concepto

Al desplegar una nueva instancia de PostgreSQL, sin configuración alguna para recuperación de desastres(lo cual es bastante
común, incluso en sistemas que llevan años en producción), se tienen las siguientes medidas:

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  $\infty$ &
  $\infty$ \\ \hline
  \end{tabular}
  \caption{RPO/RTO inicial}
\end{table}

Basícamente la pérdida de datos es infinita y el tiempo de recuperación también es infinito.
Nuestro objetivo es mejorar estas medidas a resultados más aceptables para la recuperación de desastres.

## 3.2 Entorno de la Prueba de Concepto

### 3.2.1 NixOS y NixOps

Por los motivos presentados en el estado del arte, usaremos NixOS para la prueba de concepto.

Gracias a NixOS podemos usar a _NixOps_[@dolstra2013charon], que extiende las capacidades de NixOS para aprovisionamiento y
despliegue de múltiples instancias en una red. Esto nos brinda la capacidad de hacer despliegues multi nube lo cual aumenta más la
resiliencia de la base de datos.

### 3.2.1 Base de datos

Vamos a usar una base de datos pública conocida como World. En ella se listan todos los países, ciudades e idioma hablado en el mundo.
Esta base de datos tiene la siguiente estructura:

\begin{figure}%
  \centering
  \includegraphics{images/world-er.png}
  \caption{Diagrama ER de World}%
\end{figure}

### 3.2.1 AWS EC2

Usaremos el servicio de IaaS de AWS(conocido como EC2) para desplegar nuestras instancias de PostgreSQL.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{images/ec2-logo.jpeg}
  \caption{AWS EC2}%
\end{figure}

### 3.2.1 AWS S3

Usaremos el servicio de almacenamiento de AWS(conocido como S3) para guardar los backups de PostgreSQL.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.3\textwidth]{images/s3.png}
  \caption{AWS S3}%
\end{figure}

### 3.2.1 Herramientas UNIX

Para interactuar con la base de datos usaremos **psql** que es provista por PostgreSQL por defecto y funciona en terminales tradicionales.
Para conectarnos a la instancia de EC2 usaremos **ssh** y para interactuar con NixOS usaremos un shel **bash**.

Todas estas son herramientas UNIX típicas en cualquier entorno.

## 3.3 Proceso de la Prueba de Concepto

Ahora veremos como se logró la prueba de concepto, paso a paso.

### 3.3.1 Configuración de servidor en EC2 y almacenamiento en S3

Primero definiremos los recursos en la nube de AWS, esto se hace usando una especificación en archivo nombrado **ec2-s3.nix**.
Esto luego nos servirá para desplegarlo con NixOps.

\begin{lstlisting}[language=sh, caption=Configuración de EC2 y S3, numbers=left, captionpos=b]]
let
  region = "us-east-2";
  accessKeyId = "dev";

  ec2 = { resources, ... }:
    {
      deployment.targetEnv = "ec2";
      deployment.ec2 = {
        accessKeyId = accessKeyId;
        region = region;
        instanceType = "t2.micro";
        associatePublicIpAddress = true;
        keyPair = resources.ec2KeyPairs.pg-key-pair;
      };
    };
in
{
  resources = {
    ec2KeyPairs.pg-key-pair = {
      inherit region accessKeyId;
    };
    s3Buckets.poc-362 = {
      inherit region accessKeyId;
      versioning = "Suspended";
    };
  };
  pg = ec2;
  ## pg-recovery = ec2;
}
\end{lstlisting}

En 2 definimos la zona de disponibilidad en la cual sera desplegado nuestro servidor.

En 3 indicamos el nombre del archivo de nuestras credenciales de AWS(necesitamos estas para autenticarnos).

De 5 a 15 definimos las características de nuestro servidor en EC2. Notoriamente en 11, se define el tipo de instancia ec2(t2.micro),
que viene a ser una de uso gratuito, con limitados recursos, para fines de demostración.

De 19 a 21 definimos una llave(key pair) que le servirá a NixOps para acceder a nuestro servidor.

De 22 a 25 definimos el almacenamiento S3, estas unidades de almacenamiento son conocidas como "buckets" las cuales deben
tener un nombre único, en este caso `poc-362`.

En 27 definimos que todas estas características de ec2 serán destinadas a nuestro servidor dedicado para PostgreSQL(abreviado
usualmente como pg).

Finalmente, en la **línea 28**, hemos comentado la instancia de PostgreSQL que nos servirá de recuperación, la cual no se desplegará
al inicio.

### 3.3.1 Configuración de instacia primaria de PostgreSQL

Aquí haremos la configuración de PostgreSQL, definida en un archivo **pg.nix**.

\begin{lstlisting}[language=sh, caption=Configuración de instacia primaria de PostgreSQL, numbers=left, captionpos=b, showstringspaces=false, breaklines=true]]
{
  network.description = "Postgres servers";

  pg = { config, pkgs, resources, ... }: {
    environment.systemPackages = [
      pkgs.awscli
    ];
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      authentication = pkgs.lib.mkForce ''
        local all all trust
        local replication postgres trust
      '';
      extraConfig = ''
        wal_level = archive
        archive_mode = on
        max_wal_senders = 10
        archive_command = '${pkgs.awscli}/bin/aws s3 cp %p s3://${resources.s3Buckets.poc-362.name} 1>&2'
      '';
    };

    systemd.services.postgresql = {
      environment = {
        AWS_ACCESS_KEY_ID = builtins.readFile ./key_id.secret;
        AWS_SECRET_ACCESS_KEY = builtins.readFile ./access_key.secret;
      };
    };
  };

  pg-recovery = { config, pkgs, resources, ... }: {
    environment.systemPackages = [
      pkgs.awscli
    ];
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      recoveryConfig = ''
        restore_command = '${pkgs.awscli}/bin/aws s3 cp s3://${resources.s3Buckets.poc-362.name}/%f %p'
        # Especificar el tiempo de restauracion deseado
        # recovery_target_time='2018-12-22 00:04:00'
      '';
    };

    systemd.services.postgresql = {
      environment = {
        AWS_ACCESS_KEY_ID = builtins.readFile ./key_id.secret;
        AWS_SECRET_ACCESS_KEY = builtins.readFile ./access_key.secret;
      };
    };
  };
}
\end{lstlisting}

En 31 definimos el nombre de nuestra red de instancias de PostgreSQL.

De 33 a 59 definimos las caracteristicas de nuestro servidor que contendrá a PostgreSQL.

Primero, de 34 a 36 definimos un paquete adicional(aws-cli) que nos servirá para enviar los backups incrementales a S3.

De 37 a 50 definimos las características de PostgreSQL.

Esencialmente en 44 a 49, definimos que queremos backups incrementales, en PostgreSQL estas piezas de información son conocidas
como WAL logs. De 45 a 47 habilitamos el envío de estos WAL, y en 48 enviamos estos WALs a nuestro anteriormente definido S3 bucket
`poc-362`.

De 60 a 80, definimos nuestra instancia de PostgreSQL de recuperación, que inicialmente no será desplegada.

De 67 a 71, se define cómo se hará la recuperación. Primero en 68, se define que se consumirán los WAL logs enviados por la instancia
primaria, opcionalmente en 70, se puede especificar el punto de tiempo de recuperación deseado.

Finalmente tanto como de 52 a 58 y de 74 a 79 se definen las credenciales usadas por `aws-cli` en ambas instancias.

### 3.3.2 Provisión de los recursos a EC2 y S3

Ahora podemos desplegar los recursos ya definidos a la nube de AWS. Para esto nos ayudará NixOps. Primero crearemos un registro
local de este despliegue, usando los dos archivos previamente definidos y el comando `nixops`:

```
nixops create ec-s3.nix pg.nix -d poc
```

A este registro local lo llamamos **poc**.

Ahora estamos listo para desplegar a la nube los recursos, nuevamente usando `nixops` desplegaremos **poc**:

```
nixops deploy -d poc
```

Y ahora podemos ver en la interfaz gráfica de AWS que la instancia EC2 fue creada:

\begin{figure}[H]
  \centering
  \includegraphics{images/ec2-dashboard-1.png}
  \caption{Instancia primaria de PostgreSQL en EC2}%
\end{figure}

Se puede notar el nombre que le dimos a nuestra red "Postgres Servers" y el nombre de la instancia que desplegamos("pg").

Así también podemos ver el bucket S3 creado:

\begin{figure}[H]
  \centering
  \includegraphics{images/s3-dashboard-1.png}
  \caption{Bucket S3 para almacenamiento de backups incrementales}%
\end{figure}

Este finaliza con el nombre que le asignamos **poc-362**, puesto que NixOps le asigna un identificador que asegura su unicidad.

### 3.3.3 Exploración de la base de datos

Manualmente, descargaremos y crearemos la base de datos **World** antes mencionada, en nuestra instancia de PostgreSQL en AWS.
Esta base de datos fue obtenida de https://wiki.postgresql.org/wiki/Sample_Databases. Para esto nos conectamos primero a nuestra
instancia con `nixops`:

```
nixops ssh -d poc pg
```

Una vez descargada el script de la base de datos **World** la crearemos en nuestra instancia de PostgreSQL:

```
create database world;
\i world.sql
```

Se puede notar que los WALs están siendo enviados a S3.

\begin{figure}[H]
  \centering
  \includegraphics{images/s3-dashboard-2.png}
  \caption{Enviado de backups incrementales a S3}%
\end{figure}

Ahora le daremos un vistazo a la data de la tabla **countrylanguage** usando **psql**:

```
psql -U postgres
\c world
SELECT * FROM countrylanguage;
```

Y estos son los resultados:

\begin{figure}[H]
  \centering
  \includegraphics{images/world-1.png}
  \caption{Data inicial de la bd World}%
\end{figure}

Se puede ver la lista de códigos de país con sus lenguajes más usados según porcentaje.

Teniendo un total de 984 registros según la sentencia SQL con el **count**.

### 3.3.4 Simulación del error en la base de datos

Ahora simularemos el error en la base de datos, con el ya clásico DELETE sin WHERE:

\begin{figure}[H]
  \centering
  \includegraphics{images/world-2.png}
  \caption{DELETE sin WHERE}%
\end{figure}

### 3.3.5 Recuperación de desastres

Ahora que ocurrió el error, lo que haremos será simplemente descomentar la **línea 28** del archivo **ec2-s3.nix** y desplegar
nuevamente con **nixops**, a su vez mediremos el tiempo de este despliegue con el comando **time**:

```
time nixops deploy -d poc
```

El tiempo total que reporta **time** para la finalización del despliegue es:

```
nixops deploy -d poc 19.46s
```

Podemos ver en el dashboard de AWS a la nueva instancia llamada **pg-recovery**:

\begin{figure}[H]
  \centering
  \includegraphics{images/ec2-dashboard-2.png}
  \caption{AWS EC2 con instancia de recuperacion}%
\end{figure}

Y ahora nos conectaremos a esta instancia:

```
nixops ssh -d poc pg-recovery
```

Y veremos los contenidos de la tabla **countrylanguage**:

\begin{figure}[H]
  \centering
  \includegraphics{images/world-3.png}
  \caption{Base de datos recuperada}%
\end{figure}

Lo cual indica que la recuperación fue exitosa.

## 3.4 Resultado de la Prueba de Concepto

Con los pasos anteriores ejecutados, se logró la siguiente medida:

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  0 &
  19.46s < 1 min \\ \hline
  \end{tabular}
  \caption{RPO/RTO final}
\end{table}

Lo que básicamente significa que existe pérdida de datos 0 y un tiempo de recuperación menor a 1 minuto. Lo que es ideal
según las características ofrecidas por un DbaaS.

\newpage

# 3. Planeamiento del Estado del Arte

## 3.1. Planeamiento

### 3.1.1 Preguntas de investigación

Q1: Cuáles son los riesgos del cloud computing?

Q2: Cuále son las ventajas que ofrecen los DbaaS?

Q3: Cómo conseguir la ventaja del DbaaS en un datacenter propio?

Q4: Qué herramientas usar para conseguir una administración autónoma?

Q5: Cómo validar la herramienta?

### 3.1.2 Bancos de Journal

- Google Scholar
- SciHub
- ScienceDirect

### 3.1.3 Palabras claves

- Cloud computing risks
- Disaster Recovery
- DbaaS
- Multi Cloud
- Schema Evolution
- Nix
- NixOS
- NixOps/Charon

### 3.1.4 Periodo de búsqueda

2000 - 2019

### 3.1.5 Criterios de selección

- Resultados estadísticos
- Madurez de las herramientas
- Número de citaciones

\newpage

## 3.2 Diagrama de Caja Negra

![Diagrama de caja negra](images/caja-negra.png)

## 3.3 Diagrama de Caja Blanca

![Diagrama de caja blanca](images/caja-blanca.png)

\newpage

# Bibliografía
