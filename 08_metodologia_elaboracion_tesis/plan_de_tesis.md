---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{24pt}
- \usepackage{titling}
---


\title{Administración autónoma de bases de datos para mitigación de riesgos de aprisionamiento tecnológico en la nube\vspace{2.0cm}}
\author{\textbf{Steve Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Orientador: Prof. Nehil Muñoz Casildo}

Informe de Tesis, presentada al curso de Proyecto de Tesis, \linebreak como requisito para la sustentación de ciclo.

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Mayo 2019}
\end{center}

\thispagestyle{empty}

\newpage

## Abreviaturas

\bigskip

- **IaaS**: Infrastructure as a Service(Infraestructura como servicio)
- **DBaaS**: Database as a Service(Base de datos como servicio)
- **AWS**: Amazon Web Services
- **RDS**: Relational Database as a Service(Base de datos relacional como servicio)
- **AZ**:  Availability Zone(zona de disponibilidad)
- **VPN**: Virtual Private Network
- **VPC**: Virtual Private Cloud(término que usa AWS para referirse a un VPN)
- **DR**: Disaster Recovery(recuperación de desastres)
- **RPO**: Recovery Point Objective(punto de recuperación objetivo)
- **RTO**: Recovery Time Objective(tiempo de recuperación objetivo)
- **SGBD**: Sistema gestor de base de datos
- **DBA**: Database Administrator(administrador de base de datos)

\setcounter{page}{1}

\newpage

## Taxonomia

\bigskip

CCS / Information systems / Data management systems / Database administration / Autonomous database administration

\newpage

## Agradecimientos

\bigskip

Agradezco a la Universidad Nacional Mayor de San Marcos, mi *alma mater*, y a sus profesores,
por haberme formado y dado la oportunidad de culminar la carrera de Ingeniería de Sistemas.

\newpage

## Resumen

Actualmente es tendencia la migración de infraestructuras de TI a la nube.
Esto es debido a la gran conveniencia y reducción de costos que ofrecen los distintos proveedores de cloud computing.
En particular los modernos DbaaS, proveen automatización de tareas administrativas de mantenimiento de base de datos.

Sin embargo, los servicios en la nube en general cuentan con muchos riegos:
Desde pérdida de los datos hasta subidas drásticas de precio por parte del proveedor.
Una forma conocida de mitigar estos riesgos es no hacerse dependiente de un solo proveedor y
usar solo los servicios de IaaS. El problema al hacer esto, es que
se pierde el valor agregado del DbaaS.

La propuesta de esta tesis, es proveer la conveniencia de un DbaaS, la administración autonóma, sobre una infraestuctura multi nube.
Evitando así el riesgo de aprisionamiento tecnológico y a su vez aumentando la robustez de la infraestructura.

## Abstract

Currently, there's a tendency of migrating IT infrastructures to the cloud.
This is due the convenience and costs reduction that cloud computing vendors offer.
DbaaS in particular, provides the automation of database administrative tasks.

However, cloud computing services in general come with many risks: from data loss to vendors pricing models.
One way to mitigate these risks is to not become dependent on a single vendor and only use IaaS.
The problem with this is that the added value of DbaaS is lost.

This thesis proposal, is to provide the DbaaS convenience, that is the autonomous adminstration, over a multi-cloud infrastructure.
This way, the vendor lock-in risk is avoided and at the same time infrastructure resillience is increased.

\newpage

\renewcommand*\contentsname{Índice general}
\tableofcontents

\newpage

\renewcommand{\listtablename}{Índice de tablas}
\listoftables

\newpage

\renewcommand{\listfigurename}{Índice de figuras}
\listoffigures

\newpage

1. Introducción
===============

En la ya madura era de la información, es conocido que para una empresa
u organización, los datos son el activo más valioso. Sin embargo, mantener
una infraestructura, ya sea local o en la nube como IaaS, capaz de manejar
grandes volúmenes de datos es un trabajo de mucho costo para cualquier empresa,
en especial si la empresa no se especializa en TI. Esto es independiente de la
tecnología de base de datos que se use(SQL o NoSQL).

Es por esto que diversos proveedores de la nube(AWS, GCP, Azure, etc), ofrecen
servicios de cloud computing conocidos como DBaaS, que autogestionan bases de datos
en diversas escalas. En esencia un modelo de **leasing** para bases de datos.

Estos DBaaS ofrecen una gran conveniencia para gestionar las bases de datos;
sin embargo se cuenta con muchos riesgos al hacerse dependiente
de un servicio en la nube, más aún si solo se depende de un solo proveedor.
Se ahondará más sobre estos riesgos en el **Capítulo 2**.

1.1. Planteamiento del Problema
-------------------------------

Para aprovechar el costo de reducido que ofrece algun proveedor de la nube y a
la vez mitigar los riegos mencionados, la mejor opción es usar al proveedor solo
como servicio de IaaS(báicamente hardware como servicio). De esta forma es
posible mover la infraestructura TI de proveedor a proveedor si se requiere. El
problema que viene al hacer esto, es que se pierde la conveniencia del servicio
del proveedor que gestiona la base de datos.

1.2. Motivación y Contexto
-------------------------------

Es de suma importancia a nivel empresarial y más aún a nivel gubernamental,
proveer a los equipos de TI de herramientas que les permita gestionar su propia
infraestructura y controlar su propia información.

Cuál sería la postura de negociación que podría tomar una empresa si luego de
10 años de operación toda su información se encuentra asilada en el servicio de
determinado proveedor? Podrá migrar fácilmente si es que su actual proveedor
aumenta los precios drásticamente?

No. Es seguro que la empresa estará ante un caso de aprisionamiento tecnológico(vendor lock-in).

1.3. Objetivos
--------------

En esencia lo que buscamos **evitar** el riesgo de aprisionamiento tecnológico y **mitigar**
riesgos de pérdida de datos.

Nos enfocaremos en el gestor de base de datos PostgreSQL, por ser el gestor open source más maduro actualmente.

Se implementará una herramienta que autogestione un clúster de PostgreSQL. Garantizando integridad de la data(backups),
y la alta disponibilidad ofrecida por los actuales DbaaS del mercado. Se ahondará más en la propuesta en el **Capítulo 4**.

1.4. Organización de la tesis
-----------------------------

En el capítulo 2 se verá una identificación de **riesgos del cloud computing** en general.

En el capítulo 3 se verán los **estado del arte** sobre las características que ofrecen los DbaaS actuales.

En el capítulo 4 presentaremos nuestra propuesta sobre **administración autónoma de bases de datos**,
qué tareas administrativas comprenderá y cuál será el alcance de esta propuesta.

En el capítulo 5 se verá cómo se hizo el **planeamiento del estado del arte**.
Se mostrarán las actividades principales, vistas a gran nivel como una caja negra y a bajo nivel como caja blanca,
usando la notación IDEF0.

1.5. Cronograma
---------------

![Cronograma](images/cronograma.png)

\newpage

2. Riesgos del cloud computing
==============================

A continuación se mostrarán los riesgos identificados en _Risks in enterprise cloud computing_[@dutta2013risks].

2.1. Ontología de riesgos
-------------------------

![Ontología de riesgos](images/risks-ontology.png){ width=80% }

\newpage

2.2. Riesgos principales identificados
--------------------------------------

De estos riesgos, los principales obtenidos por la encuesta realizada por [@dutta2013risks] son:

![10 principales riesgos](images/top-risks.png)

Con nuestra propuesta:

- Evitaremos los riesgos relacionados al vendor lock-in(2, 3, 5 y 8), puesto que el usuario/s podrá elegir y cambiar de proveedor.

- Con el enfoque multi nube, se podrán mitigar los riesgos 1, 7, 9 y 10

- La herramienta generada tiene también un fin didáctico y mitigará los riesgos 4 y 6.

2.3. Consideraciones Finales
----------------------------

Estos riesgos son la principal motivación de esta propuesta.
Lo que nos lleva a estudiar los reales beneficios que estos DbaaS proveen.

\newpage

3. Estado del arte
==================

A continuación se mostrarán las funcionalidades ofrecidas por el DbaaS líder del mercado actual: AWS RDS.

3.1. Aprovisionamiento de recursos automático
--------------------------------------------

RDS hace una elección de discos duros o discos de estado sólido y dispostivos de red para garantizar el óptimo rendimiento
de la base de datos. Además configura el sistema operativo con optimizaciones hechas a medida para un SGBD en particular.

3.2. Seguridad
--------------

RDS ubica las bases de datos en VPCs, protegiéndolas del acceso público. Además ofrece cifrado SSL para el intercambio de datos.

3.3. Backups automáticos
------------------------

RDS ofrece backups automáticos, garantizando un RPO y RTO óptimo.

3.4. Alta disponibilidad
------------------------

RDS ofrece replicas en distintas zonas de disponibilidad(AZs), estas están distribuidas geográficamente.

3.5. Escalabilidad con mínimo tiempo inactivo por mantenimiento
---------------------------------------------------------------

RDS permite escalar los recursos que usa la base de datos mientras está en uso, con mínimo tiempo inactivo(downtime).
Esto lo hace replicando una instancia de la base de datos mientras otra es promovida.

3.6. Tareas administrativas no automatizadas
--------------------------------------------

Ningún DbaaS actualmente automatiza la creación de índices(auto indexación) o migraciones(cambio de esquemas).
Por tanto es aún necesaria la presencia de un DBA para un manejo efectivo de la base de datos.

3.7. Consideraciones Finales
----------------------------

Históricamente, todas estas tareas autogestionadas for RDS han sido realizadas manualmente y de _forma imperativa_ por un DBA.
Cuando decimos _forma imperativa_ nos referimos a secuencias de comandos UNIX ad hoc, ejecutados para gestionar la base de datos.
La _forma imperativa_ es una de las razones por la cual no se ha desarrollado el enfoque de administración autónoma.
En el siguiente capítulo mostramos una alternativa a la _forma imperativa_, la **configuración declarativa**
que es la base de la propuesta de la tesis.

\newpage

4. Administración autónoma de bases de datos
============================================

Primero mostraremos las herramientas usadas para alcanzar la **administración autónoma**.

4.1. Herramientas
-----------------

- _NixOS_[@dolstra2008nixos], esta es una distribución de Linux que mejora el estado del arte en la gestión de configuración del sistema.
Nos brinda la capacidad de tener una configuración declarativa y puramente funcional, lo que permite hacer upgrades del sistema de forma confiable.

- _NixOps_[@dolstra2013charon], extiende las capacidades de NixOS para aprovisionamiento y despliegue de múltiples instancias en una red.
Esto nos brinda la capacidad de hacer despliegues multi nube lo cual aumenta más la resiliencia de la base de datos.

4.2. Configuración declarativa
------------------------------

Un ejemplo de **configuración declarativa** se puede ver a continuación:

```nix
services.postgresql = {
  enable = true;
  package = pkgs.postgresql100;
  extraPlugins = [
    postgis
    pgrouting
    tds_fdw
  ];
  authentication = pkgs.lib.mkOverride 10 ''
    # All unix socket
    local   all all trust
    # IPv4 local connections:
    host    all all 127.0.0.1/32 trust
    # IPv6 local connections:
    host    all all ::1/128 trust
    # Replication
    local   replication postgres trust
  '';
  extraConfig = ''
    logging_collector = 'on'
    log_directory = 'pg_log'
    log_filename = 'postgresql-%Y-%m-%d.log'
    log_statement = 'all'
  '';
};
```

Con esta especificación, NixOS puede crear un cluster de PostgreSQL con las siguientes características:

- Instala la versión especificada de PostgreSQL(versión 10 en este caso).
- Está automáticamente "daemonizado"(se ejecuta indefinidamente). No hay necesidad de agregar configuraciones adicionales.
- Define una lista de extensiones requeridas. Estas ya vienen incluidas y no hay necesidad de ejecutar comandos adicionales.
- Define opciones de "logging" y autenticación. Sin necesidad de ubicar archivos adicionales.

Estas tareas involucraría varios pasos en cualquier otra distribución de Linux u otro sistema operativo.

4.3. Administración autónoma
----------------------------

La **configuración declarativa** es lo que permitirá alcanzar la **administración autónoma**. Esto sumado a NixOps
nos permitirá desarrollar iterativamente las características de RDS. Y esto sobre múltiples proveedores en la nube.

A grandes rasgos, el software tendrá el siguiente diseño.

Entradas:

- Credenciales del proveedor de la nube(AWS, Hetzner, Alibaba, etc).
- Requerimientos de escala(requiere replicación internacional? o basta con una base de datos local?)

Salida:

- Base de datos con las características autónomas de RDS.

4.3. Validación
---------------

Una vez finalizada la implementación se piensa hacer "open source" esta herramienta, de forma que se pueda ganar
validación internacional. Además de probarlo en una empresa que ya gestiona recursos en la nube.

\newpage

5. Planeamiento del Estado del Arte
===================================

5.1. Planeamiento
-----------------

5.1.1 Preguntas de investigación
--------------------------------

Q1: Cuáles son los riesgos del cloud computing?

Q2: Cuále son las ventajas que ofrecen los DbaaS?

Q3: Cómo conseguir la ventaja del DbaaS en un datacenter propio?

Q4: Qué herramientas usar para conseguir una administración autónoma?

Q5: Cómo validar la herramienta?

5.1.2 Bancos de Journal
------------------------

- Google Scholar
- SciHub
- ScienceDirect

5.1.3 Palabras claves
----------------------

- Cloud computing risks
- Disaster Recovery
- DbaaS
- Multi Cloud
- Schema Evolution
- Nix
- NixOS
- NixOps/Charon

5.1.4 Periodo de búsqueda
-------------------------

2000 - 2019

5.1.5 Criterios de selección
----------------------------

- Resultados estadísticos
- Madurez de las herramientas
- Número de citaciones

\newpage

5.2 Diagrama de Caja Negra
--------------------------

![Diagrama de caja negra](images/caja-negra.png)

5.3 Diagrama de Caja Blanca
---------------------------

![Diagrama de caja blanca](images/caja-blanca.png)

\newpage

Bibliografía
============
