# My thesis

Usar:

```
pandoc --bibliography=references.bib informe.md -o informe.pdf
pandoc --bibliography=references.bib plan_de_tesis.md -o plan_de_tesis.pdf
pandoc --bibliography=references.bib estado_del_arte.md -o estado_del_arte.pdf
```

Para enviar zipeado:
```
zip -r myfiles.zip mydir
```
