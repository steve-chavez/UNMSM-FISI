
---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{0pt}
- \usepackage{titling}
- \usepackage{array}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{float}
- \usepackage{listings}
- \usepackage{tikz}
---


\title{Devops aplicado a PostgreSQL para obtener los SLAs de un Database as a Service(DbaaS) en la empresa Simply Connected Systems\vspace{2.0cm}}
\author{\textbf{Manuel Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Orientador: Prof. Menéndez Mueras Rosa}

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Julio 2020}
\end{center}

\thispagestyle{empty}

\newpage

## Antecedentes

Se muestran a continuación, trabajos previos relacionados a cómo alcanzar los acuerdos de nivel de servicio(SLA) de los vendors de la nube, y como evadir los riesgos de la adopción de la nube.
Los SLAs que nos interesan los de Alta Disponibilidad y Alta Durabilidad(recuperación de desastres).

### MACHS: Mitigating the Achilles Heel of the Cloud through High Availability and Performance-aware Solutions

|Traducción             | Mitigando el talón de aquiles de la nube a través de soluciones de alta disponibilidad y performance
|-----------------------|------------------------------------------------------------
|Año                    | 2017
|-----------------------|------------------------------------------------------------
|Institución            | The University of Western Ontario
|-----------------------|------------------------------------------------------------
|Grado académico        | Doctor of Philosophy in Electrical and Computer Engineering
|-----------------------|------------------------------------------------------------
|Autor                  | Manar Jammal

En esta tesis se mencionan casos de violaciones de acuerdos de nivel de servicio(SLAs) que suelen ocurrir con los proveedores de la nube.
Se muestra una metodología para minimizar estas violaciones y aumentar la calidad de servicio.

Se usa un simulador de errores(CloudSim) que genera fallas en los servicios y se hallan indicadores de tiempo de falla promedio(MTTF).

### A decision framework to mitigate vendor lock-in risks in cloud (saas) mgration

|Traducción             | Un framework de toma de decisiones para mitigar el riesgo de vendor lock-in en una migración a la nube
|-----------------------|-------------------------------------------------------------------------------
|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Bournemouth University
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Doctor and Philosophy
|-----------------------|----------------------------------------------------------------------
|Autor                  | Justice Nsirimovu Opara-Martins

En esta tesis se provee un framework de decisiones para mitigar los riesgos de vendor lock-in
en una migración a la nube. Esto es útil para esta investigación debido a que pone a la luz el gran trabajo
que se debe dar para mitigar los riesgos del vendor lock-in. En nuestro caso buscamos evadir totalmente
este riesgo.

### Cloud Computing: COBIT-mapped benefits, risks and controls for consumer enterprises

|Traducción             | Cloud computing: Beneficios, riesgos y controles mapeados al COBIT para empresas consumidoras.
|-----------------------|-------------------------------------------------------------------------------
|Año                    | 2012
|-----------------------|----------------------------------------------------------------------
|Institución            | Stellenbosch University
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Masters of Commerce (Computer Auditing)
|-----------------------|----------------------------------------------------------------------
|Autor                  | Zacharias Enslin

En esta tesis se mapean procesos de COBIT a riesgos en la nube.

- Riesgos mapeados al proceso Plan and Organize(PO)
- Riesgos mapeados al proceso Acquire and Implement(AI)
- Riesgos mapeados al proceso Deliver and Support(DS)
- Riesgos mapeados al proceso Monitor and Evaluate(ME)

Útil para obtener una estructura más formal de riesgos y tener el gran panorama de estos.

### Enhancing Information Security in Cloud Computing Services using SLA Based Metrics

|Traducción             | Mejorando la seguridad de la información en servicios de cloud computing usando métricas de SLA
|-----------------------|-------------------------------------------------------------------------------
|Año                    | 2011
|-----------------------|----------------------------------------------------------------------
|Institución            | Blekinge Institute of Technology - Sweden
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Master thesis
|-----------------------|----------------------------------------------------------------------
|Autores                | Nia Ramadianti Putri Medard Charles Mganga

En esta tesis se mencionan unas métricas de acuerdos de nivel de servicio(SLAs) para asegurar la seguridad en la nube.
Esto es particularmente útil para reutilizar métricas y asegurar las mismas garantías de estos SLAs.

\newpage

### Modelo de continuidad de servicios de las tecnologías de la información y comunicación utilizando cloud computing en la empresa americas potash perú s.a

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Universidad Católica Los ángeles de Chimbote - Facultad de Ingeniería
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Magíster en tecnología de información y comunicación
|-----------------------|----------------------------------------------------------------------
|Autora                 | Ing. Dominguez Oliva Wendy

En esta tesis se mencionan los "Riesgos encontrados por Gartner" en 2011, se citan los siguientes:

- Accesos de usuarios con privilegios.
- Cumplimiento normativo
- Localización de los datos
- Aislamiento de datos
- Recuperación
- Soporte investigativo
- Viabilidad a largo plazo

### Propuesta de implementación de cloud computing para asegurar continuidad operativa de infraestructura informática en empresa de internet

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Universidad San Ignacio de Loyola
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Título Profesional de Ingeniero Empresarial y de Sistemas
|-----------------------|----------------------------------------------------------------------
|Autor                  | Chirinos Muñoz, Percy Humberto

En esta tesis se citan las "Desventajas del Cloud Computing" encontradas por García Juárez.

- Continuidad del servicio
- Lock-in de servicios/aplicaciones
- Probabilidad de ataques de hackers
- Desconocimiento de políticas de administración de datos/aplicaciones
- Retorno de la inversión

### Conclusiones

En general, a nivel internacional se puede notar la preocupación por identificar y mitigar adecuadamente los riesgos del cloud computing.
Nacionalmente se puede apreciar que los riesgos no son un mayor obstáculo para la adopción de la nube.

## Indicadores

### Recuperación de desastres(Disaster Recovery)

Una de las principales ofertas de un DbaaS es la recuperación de desastres, es decir, la restauración de la base de datos al
estado más reciente y consistente. Para esto tenemos dos indicadores.

### Tiempo objetivo de recuperación(RTO)

Es el tiempo máximo permitido para la recuperación de un servicio después de una interrupción.

\begin{equation*}
\boldsymbol{RTO = FechaHoraDesastre - FechaHoraRecuperacion}
\end{equation*}

Un RTO ideal para un DbaaS viene a ser menor a 1 minuto.

### Punto objetivo de recuperación(RPO)

Es la cantidad máxima de datos que se pueden perder cuando se restablece el servicio después de una interrupción.
El punto de recuperación objetivo se expresa en un período de tiempo antes de la falla.
Por ejemplo, un punto de recuperación objetivo de un día puede ser apoyado por los backups diarios, y donde se pueden perder datos
de hasta 24 horas.

\begin{equation*}
\boldsymbol{RPO = DatosPerdidos(FechaHoraDesastre - FechaUltimoBackup)}
\end{equation*}

Cero es un valor válido y es equivalente a un requisito de "pérdida de datos cero".
