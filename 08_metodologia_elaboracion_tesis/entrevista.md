---
header-includes:
- \usepackage{caption}
- \usepackage{tikz}
- \usetikzlibrary{positioning}
- \usepackage[export]{adjustbox}
- \usepackage{array}
---

# Entrevista para determinar la problemática operativa actual en la empresa Simply Connected Systems

\hfill \break

\begin{center}
  \begin{tabular}{ | l | l |} \hline
  \textbf{Nro Entrevista} & 1 \\ \hline
  \textbf{Entrevistado} & Steven Kramer \\ \hline
  \textbf{Cargo} & Chief Connection Officer \\ \hline
  \textbf{Fecha} & 2019/10/12 \\ \hline
  \end{tabular}
\end{center}

\hfill \break

## 1. A qué se dedica Simply Connected Systems?

Simply Connected Systems[^1] está basada en Coral Springs en el estado de Florida, Estados Unidos.

Nos dedicamos a integrar sistemas infórmaticos corporativos de la forma más simple. Ofrecemos soluciones informáticas móviles
no disruptivas, customizables, asequibles, sin papel, sin retipeo, que permiten a las empresas mantener sus sistemas informáticos
heredados, flujo de trabajo e información.

Nos enorgullecemos en tener como caso de éxito la integración informática de una compañía eléctrica que ha operado por
4 generaciones(casi 100 años). Nuestras soluciones están pensadas para ser de bajo mantenimiento y lo más duraderas posible.

Actualmente nos estamos enfocando en un nuevo mercado: empresas de servicios de restauración. Estas empresas reparan daños
ocasionados por desastres naturales en distintas instalaciones. Hacen esto con maquinaria que trabaja "on site", la cual
se encarga de la dehumidificación del aire, absorción de agua, etc. Esta maquinaria en ocasiones se extravia,
por lo cual estamos proveyendo un sistema de monitoreo, usando dispositivos IoT, que le permita a la empresa
identificar la posición y el estado de la maquinaria.

El desarrollo de estos sistemas es algo nuevo para nosotros y hacemos uso extenso de la nube con sus distintos proveedores.

## 2. El desarrollo de estos sistemas presenta nuevas dificultades?

Sí, existen diversas tareas que escapan a la expertiz del equipo de Simply Connected Systems. Por lo cual hacemos uso
de diversos contratistas de distintas nacionalidades. Usamos la plataforma Upwork[^2] para encontrarlos y contratarlos, esto nos
ha funcionado muy bien.

Así también, la nube ha reducido mucho nuestro costo operativo. En particular, para manejar nuestras bases de datos PostgreSQL[^3]
usamos AWS RDS[^4] y este nos ofrece muchas ventajas pero aquí viene nuestro principal problema.

Aunque nuestras soluciones basadas en la nube son suficientes para empresas pequeñas, empresas más grandes, con mayor madurez,
con politicas de gobernanza de datos y aversión al riesgo, desean alojar nuestras soluciones en infraestructura privada.

Sin embargo, AWS RDS no dispone de una versión "on premise". Para cumplir con los requisitos de estas empresas,
nuestros sistemas tendrían que replicar todas las ventajas que AWS RDS nos ofrece e instalarlos de una forma reproducible
para cada uno de nuestros clientes.

[^1]: https://www.simplyconnectedsystems.com
[^2]: https://upwork.com
[^3]: http://postgresql.org/
[^4]: https://aws.amazon.com/rds/

## 3. Una versión "on-premise" de AWS RDS solucionaría el problema?

Sí. Sabemos que AWS RDS al manejar PostgreSQL ofrece funcionalidades como: backups incrementales, recuperación en un punto de tiempo,
alta disponibilidad, etc, en las cuales muchas empresas confían para garantizar la robustez de sus bases de datos.

Sin embargo, muchas empresas han manejado a PostgreSQL exitosamente en tiempos anteriores al boom del cloud computing.
Esto es debido al gran ecosistema open source que mantiene PostgreSQL. AWS RDS admite combinar distintos componentes open source
gratuitos que el ecosistema ofrece.

Creemos que en estos tiempos de DevOps, esta tarea, antes herculeana, es ahora factible.

Esto nos permitiría replicar un sistema robusto en cualquier infraestructura, tanto pública como privada.
