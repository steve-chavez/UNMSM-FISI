# Trabajos previos

Se muestran a continuación, trabajos previos relacionados a riesgos en la nube, 3 nacionales y 3 internaciones, respectivamente.

## Modelo de continuidad de servicios de las tecnologías de la información y comunicación utilizando cloud computing en la empresa americas potash perú s.a

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Universidad Católica Los ángeles de Chimbote - Facultad de Ingeniería
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Magíster en tecnología de información y comunicación
|-----------------------|----------------------------------------------------------------------
|Autora                 | Ing. Dominguez Oliva Wendy

En esta tesis se mencionan los "Riesgos encontrados por Gartner" en 2011, se citan los siguientes:

- Accesos de usuarios con privilegios.
- Cumplimiento normativo
- Localización de los datos
- Aislamiento de datos
- Recuperación
- Soporte investigativo
- Viabilidad a largo plazo

## Propuesta de implementación de cloud computing para asegurar continuidad operativa de infraestructura informática en empresa de internet

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Universidad San Ignacio de Loyola
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Título Profesional de Ingeniero Empresarial y de Sistemas
|-----------------------|----------------------------------------------------------------------
|Autor                  | Chirinos Muñoz, Percy Humberto

En esta tesis se citan las "Desventajas del Cloud Computing" encontradas por García Juárez.

- Continuidad del servicio
- Lock-in de servicios/aplicaciones
- Probabilidad de ataques de hackers
- Desconocimiento de políticas de administración de datos/aplicaciones
- Retorno de la inversión

## Propuesta de Arquitectura Cloud Computing para la migración del sistema integrado de control académico de la universidad nacional de tumbes

|Año                    | 2015
|-----------------------|----------------------------------------------------------------------
|Institución            | Universidad San Ignacio de Loyola
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Título Profesional de Ingeniero de sistemas
|-----------------------|----------------------------------------------------------------------
|Autor                  | Bruno Alexander Sánchez Oviedo

En esta tesis se citan los "Riesgos de trabajar con Cloud Computing" encontrador por el group ACE.

- Contratos
- Pérdida de Control
- Agregación de Riesgos
- Costo
- Seguridad de los datos

## Cloud Computing: COBIT-mapped benefits, risks and controls for consumer enterprises

|Año                    | 2012
|-----------------------|----------------------------------------------------------------------
|Institución            | Stellenbosch University
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Masters of Commerce (Computer Auditing)
|-----------------------|----------------------------------------------------------------------
|Autor                  | Zacharias Enslin

En esta tesis se mapean procesos de COBIT a riesgos en la nube.

- Riesgos mapeados al proceso Plan and Organize(PO)
- Riesgos mapeados al proceso Acquire and Implement(AI)
- Riesgos mapeados al proceso Deliver and Support(DS)
- Riesgos mapeados al proceso Monitor and Evaluate(ME)

## A decision framework to mitigate vendor lock-in risks in cloud (saas) mgration

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Bournemouth University
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Doctor and Philosophy
|-----------------------|----------------------------------------------------------------------
|Autor                  | Justice Nsirimovu Opara-Martins

En esta tesis se provee un framework de decisiones para mitigar los riesgos de vendor lock-in
en una migración a la nube.

## Information Security Guidelines for Organisations Intending to Adopt Cloudsourcing

|Año                    | 2017
|-----------------------|----------------------------------------------------------------------
|Institución            | Stockholm University
|-----------------------|----------------------------------------------------------------------
|Grado académico        | Master thesis
|-----------------------|----------------------------------------------------------------------
|Autor                  | Neelambari Annamalai

En esta tesis se menciona al término "cloudsourcing", outsourcing hacia la nube, y se citan
medidas de seguridad para adoptar la migración a la nube.
