---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{0pt}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{multirow}
- \usepackage{array}
---

# Marco teórico

Empezaremos definiendo qué es cloud computing, de acuerdo a @mell2011nist, listando sus categorías ya tradicionales para luego llegar
a la más reciente subcategoría de DbaaS.

## Cloud computing

Cloud computing es un modelo que habilita un acceso on-demand, ubicuo y conveniente a un pool compartido de recursos computacionales
que puede ser rápidamente provisto con un esfuerzo mínimo de configuración e interacción con el proveedor del servicio.

Ahora veremos las categorías o modelos de servicio tradicionales:

## Infrastructure as a Service(IaaS)

La capacidad provista al consumidor es la de poder aprovisionar procesamiento, almacenamiento, redes y otros recursos computacionales
fundamentales donde el consumidor es capaz de desplegar y correr software arbitrario, que puede incluir sistemas operativos
y aplicaciones. El consumidor no controla la infraestructura(hardware) pero tiene control sobre el sistema operativo, almacenamiento,
aplicaciones desplegadas y posiblemente la configuración de red.

## Platform as a Service(PaaS)

La capacidad provista al consumidor es el despliegue a la infraestructura del proveedor. Pero solo de aplicaciones creadas usando
lenguajes de programación, librerías, servicios y herramientas soportadas por el proveedor. El consumidor no maneja
o controla la infraestructura incluyendo la configuración de red, servidor, sistema operativo y almacenamiento, pero tiene control
sobre las aplicaciones desplegadas y posiblemente sobre las configuraciones de la aplicación desplegada.

## Software as a Service(SaaS)

La capacidad provista al consumidor es el uso de aplicaciones alojadas en la infraestructura del proveedor. Las aplicaciones son
accesibles desde varios dispositivos clientes a través de una interfaz ligera, como un navegador web o la interfaz de un programa.
El consumidor no maneja o controla la infraestructura del proveedor, incluyendo su configuración de red, servidores, sistemas
operativos, almacenamiento o incluso capacidades individuales de las aplicaciones, con la posible excepción de algunas configuraciones
específicas al usuario.

## Database as a Service(DbaaS)

De acuerdo a @Curino2011RelationalCA, el DbaaS mueve la carga operacional del aprovisionamiento, configuración, 
escalado, afinación de performance, políticas de backup y control de acceso de la mano de los usuarios de la base de datos
hacia la mano del proveedor del servicio, reduciendo así los costos al usuario. Siendo Amazon RDS y 
Microsoft SQL Azure los pioneros en ofrecer DbaaS.

En esencia, DbaaS es un modelo de leasing o outsourcing para Sistemas Gestores de Bases de Datos.

Para poder ahondar en los riesgos del cloud computing, veremos ahora una base de la Gestión de Riesgos según @Sommerville:2010:SE:1841764.

## Gestión de Riesgos

La gestión del riesgo implica anticipar riesgos que pudieran alterar el calendario del proyecto o 
la calidad del softawre a entregar, y posteriormente tomar acciones para evitar dichos riesgos. 

## Tipos de riesgos

Tenemos 3 tipos de riesgos:

1. Riesgos del proyecto: Los riesgos que alteran el calendario o los recursos del proyecto.

2. Riesgos del producto: Los riesgos que afectan la calidad o el rendimiento del software a desarrollar.

3. Riesgos empresariales: Los riesgos que afectan la organización que desarrolla o adquiere el software.

En el caso del cloud computing estamos ante un **riesgo del proyecto** y **riesgo del producto**.

## Proceso de Gestión de Riesgos

Para evaluar los riesgos necesitamos seguir las siguientes etapas:

1. Identificación del riesgo: La enumeración de los posibles riesgos para el proyecto, producto y empresa.

2. Análisis de riesgos: Se debe valorar la probabilidad y las consecuencias de dichos riesgos.

3. Planeación del riesgo: Elaborar planes para enfrentar el riesgo, evitarlo o minimizar sus efectos en el proyecto.

4. Monitorización del riesgo: Valorar regularmente el riesgo y los planes para atenuarlo, y revisarlos cuando se aprenda más
sobre el riesgo.

Afortunadamente para nuestro caso sobre cloud computing, ya existe un desarrollo de la **identificación** y **análisis** de riesgos el cual
desarrollaremos en la sección del estado del arte.

## Conceptos generales de Bases de datos

Para entender la propuesta primero vamos a esclarecer conceptos generales de bases de datos.

## Replicación

La replicación en bases de datos es usada para dos propósitos: mejorar la performance del sistema y
mejorar la tolerancia a fallas.

Según @chouk2003master estas estrategias pueden dividirse en 4 dependiendo de cuándo ocurre la replicación y en qué nodos ocurre.

\hfill \break

\begin{tabular}{ | m{0.2\textwidth} | m{0.3\textwidth} | m{0.3\textwidth} |}
\hline
&
\textbf{Síncrona} &
\textbf{Asíncrona} \\
\hline
\textbf{Copia primaria (Maestro)} &
Las escrituras solo están permitidas en \textbf{un solo nodo}. \newline \newline
Las escrituras se propagan \textbf{antes} que la transacción finalize. &
Las escrituras solo están permitidas en \textbf{un solo nodo}. \newline \newline
Las escrituras se propagan \textbf{después} que la transacción finalize. \\
\hline
\textbf{Todos los nodos (Multi-Maestro)} &
Las escrituras están permitidas en \textbf{todos los nodos}. \newline \newline
Las escrituras se propagan \textbf{antes} que la transacción finalize. &
Las escrituras están permitidas en \textbf{todos los nodos}. \newline \newline
Las escrituras se propagan \textbf{después} que la transacción finalize. \\
\hline
\end{tabular}

## Particionamiento horizontal o Sharding

Cuando se trata de escalar el almacenamiento y el throughput que puede soportar la base de datos, una opción inmediata es añadirle más 
recursos al servidor, sin embargo no se puede escalar de esta manera de forma indefinida.

Otra opción es usar **sharding**, este es un proceso en el cual se particiona las filas de una tabla en múltiples bases de datos
independientes.

Según @Sitaram_2012 existen 3 técnicas de particionamiento:

### Particionamiento por Round Robin

Este distribuye las filas de forma que las filas de la primera transacción van en la primera base de datos,
la segunda transacción en la segunda base de datos y así sucesivamente.

La ventaja de esta técnica es la simplicidad, sin embargo se pierden las asociaciones.
Si se requieren hacer consultas usando JOINs, se requiere hacerlo en multiples bases de datos, lo cual resulta complejo e ineficiente.

Los siguientes dos técnicas no sufren de la desventaja de perder las asociaciones.

### Particionamiento por Hash

En esta técnica, el valor de un atributo selecionado es "hasheado" para encontrar la base de datos
en la cual la fila será guardada. Si las conslutas son hechas frecuentemente en un atributo,
por ejemplo `id_cliente` entonces las asociaciones son preservadas usando este atributo,
puesto que las filas con el mismo valor del atributo pueden ser encontrados en la misma base de datos.

### Particionamiento por Rango

En esta técnica se guardan filas con atributos similar en la misma base de datos.
Por ejemplo el rango de `id_cliente` puede ser dividido en diferentes de bases de datos.

Una técnica generalizada es el `Particionamiento por Lista` la cual busca
una combinación de atributos de la fila en un directorio para encontrar la partición a la cual pertenece.

![](images/sharding.png)

## Recuperación de desastres(Disaster Recovery)

El objetivo de la recuperación de desastres es restaurar la base de datos fallida al
estado más reciente y consistente. Para esto tenemos dos métricas:

### Punto objetivo de recuperación(RPO)

Es la cantidad máxima de datos que se pueden perder cuando se restablece el servicio después de una interrupción. 
El punto de recuperación objetivo se expresa en un período de tiempo antes de la falla. 
Por ejemplo, un punto de recuperación objetivo de un día puede ser apoyado por los backups diarios, y donde se pueden perder datos 
de hasta 24 horas.

Cero es un valor válido y es equivalente a un requisito de "pérdida de datos cero".

### Tiempo objetivo de recuperación(RTO)

Es el tiempo máximo permitido para la recuperación de un servicio después de una interrupción. 
El nivel de servicio que se presta es inferior al objetivo de nivel de servicio normal. 

Un RTO ideal para cloud computing viene a ser menor a 1 minuto.

# Estado del arte

En esta sección veremos:

- El **estado ideal** que debería tener nuestro sistema y para esto nos basaremos en las funcionalidades 
  ofrecidas por el DbaaS líder del mercado actual: AWS RDS.

- **Cómo llegaremos a este estado ideal** usando un **modelo congruente** de gestión de sistemas. Para esto veremos
  los 3 distintos modelos de gestión de sistemas.

## Funcionalidades de RDS

### Aprovisionamiento de recursos automático

RDS hace una elección de discos duros o discos de estado sólido y dispostivos de red para garantizar el óptimo rendimiento
de la base de datos. Además configura el sistema operativo con optimizaciones hechas a medida para un SGBD en particular.

### Seguridad

RDS ubica las bases de datos en VPCs, protegiéndolas del acceso público. Además ofrece cifrado SSL para el intercambio de datos.

### Backups automáticos

RDS ofrece backups automáticos, garantizando un RPO y RTO óptimo.

### Alta disponibilidad

RDS ofrece replicas en distintas zonas de disponibilidad(AZs), estas están distribuidas geográficamente.

### Escalabilidad con mínimo tiempo inactivo por mantenimiento

RDS permite escalar los recursos que usa la base de datos mientras está en uso, con mínimo tiempo inactivo(downtime).
Esto lo hace replicando una instancia de la base de datos mientras otra es promovida.

### Tareas administrativas no automatizadas

Ningún DbaaS actualmente automatiza la creación de índices(auto indexación) o migraciones(cambio de esquemas).
Por tanto es aún necesaria la presencia de un DBA para un manejo efectivo de la base de datos.

### Consideraciones

Históricamente, todas estas tareas autogestionadas for RDS han sido realizadas manualmente y de _forma imperativa_ por un DBA.
Cuando decimos _forma imperativa_ nos referimos a secuencias de comandos UNIX ad hoc, ejecutados para gestionar la base de datos.
La _forma imperativa_ es una de las razones por la cual no se ha desarrollado el enfoque de administración autónoma.
En el siguiente capítulo mostramos una alternativa a la _forma imperativa_, la **configuración declarativa**
que es la base de la propuesta de la tesis.

## Gestión de sistemas

De acuerdo a @Traugott:2002:WOM:1050517.1050529 existen 3 modelos de gestión de sistemas:

\begin{figure}%
  \centering
  \subfloat[Divergente]{{\includegraphics{images/divergence.png} }}%
  \qquad
  \subfloat[Convergente]{{\includegraphics{images/convergence.png} }}%
  \qquad
  \subfloat[Congruente]{{\includegraphics{images/congruence.png} }}%
  \caption{Modelos de gestión de sistemas}%
\end{figure}

\newpage

### Modelo divergente

La divergencia es caracterizada por la configuración del sistema alejándose del estado ideal del sistema.

Esto es ilustrado en el flujo de trabajo típico de un sysadmin, el cual sigue un checklist y manualmente ejecuta paso por paso los comandos
necesarios para alcanzar el estado del sistema deseado. Si 3 distintos sysadmins siguen el mismo checklist,
es probable que se obtengan 3 resultados distintos.

### Modelo convergente

La convergencia es caracterizada por la configuración del sistema acercándose al estado ideal del sistema.

Esta es una mejora sobre el modelo divergente, en este caso se usan herramientas más maduras
que permiten declarar una descripción del sistema. Algunas herramientas actuales del mercado son: Puppet, Ansible.
El problema es que estás herramientas aún pueden diverger del estado ideal, es necesario
monitorear las diferencias entre la descripción del sistema y estado ideal deseado.

### Modelo congruente

La congruencia es la práctica de mantener la configuración del sistema en completa igualdad al estado ideal del sistema.

Este modelo es caracterizado por herramientas actuales como **contenedores** o **NixOS**(@dolstra2008nixos). En estos casos
el estado real del sistema es un espejo de la descripción del sistema.

\newpage

## Referencias
