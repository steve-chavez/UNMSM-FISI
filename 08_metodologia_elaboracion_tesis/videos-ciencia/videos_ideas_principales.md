# Ciencia - Ideas Principales

Hecho por:

|Alumno                 |Codigo
|-----------------------|---------
|Chávez Astucuri Manuel | 09200078

## Qué es la ciencia?

Sirve para comprender el mundo que nos rodea. Viene del latin "scentia", que significa conocimiento de varios campos.

La ciencia está dividida en ciencias formales(ejemplos: matematica, logica), ciencias empíricas(ejemplos: ciencias sociales, economía)
y ciencias naturales(ejemplos: física, biología). La ciencia hace uso del método científico.

La ciencia no plantea verdades incuestionables, solo da formas de explicar la realidad en determinado contexto.
La ciencia es susceptible a ser refutada, si no se consigue falsar a la hipótesis se le llama ley.

La experimentación es esencial para la ciencia, esta es dada por observaciones, mediciones y estadísticas.

Si recopilamos todos los experimentos, hechos hasta hoy, obtenemos reglas universales que permiten entender y seguir entendiendo
al mundo que nos rodea.

## Qué es y para qué sirve la investigación?

Sirve para buscar respuestas verdaderas cuando no se tienen respuestas inmediatas. Es el puente entre
pregunta y respuesta verdadera. No se investiga lo que ya se sabe.

La investigacion suele se accidentada, es un proceso creativo en el cual se deben administrinar recursos(tiempo, personas) y
comunicar resultados.

La motivación para la investigación surge de un contexto cultural, temporal y espacial. Su objetivo es
encontrar una solucion práctica.

El lenguaje es el medio principal de la investigación, el lenguaje transmite y transforma nuestros pensamientos.
El conocimiento obtenido por parte de la investigación está sujeto a la realidad social, esta es práctica e histórica.

Un modelo útil para la investigación es el "tablero", este viene a ser el núcleo del proyecto. Se compone de 4 partes:

- Objeto: el asunto del que se trata.
- Pregunta: la cual no puede ser respondida de manera inmediata.
- Objetivo: lo que se desea lograr o probar.
- Respuesta: la respuesta provisional alineada al objetivo.

# Qué es el metodo científico?

Sirve para dar respuesta a algún problema.

El video da ejemplo del problema del móvil que no enciende para ilustrar los siguientes pasos:

1. Observación: Ver el móvil. Tiene algún golpe? Se prende al presionar un botón?
2. Formular hipótesis: Creo que no enciende por que no tiene batería.
3. Experimentación: Conectemos el móvil y veamos si enciende.
4. Elaborar Conclusión: No encendía porque no tenía batería.
