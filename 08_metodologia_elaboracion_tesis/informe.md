# DBaaS sobre IaaS para mitigación de riesgos de aprisionamiento tecnológico en la nube

## Contexto del problema

Actualmente es tendencia la migración de infraestructuras de TI a la nube(`cloud computing`). Sin embargo, muchas migraciones
se hacen sin tomar en cuenta los riesgos que están en juego. Desde pérdida de los datos hasta subidas drásticas de precio por parte del proveedor, los
servicios en la nube cuentan con muchos riegos.

Nos encontramos en el contexto de la propia industria del software. Específicamente en las áreas de DevOps[^1] y
DbaaS[^2].

## Problema

En la ya madura era de la información, es conocido que para una empresa o organización, los datos son el activo más valioso.
Sin embargo, mantener una infraestructura, ya sea local o en la nube como IaaS, capaz de manejar grandes volúmenes de datos es un trabajo
de mucho costo para cualquier empresa, en especial si la empresa no se especializa en TI.
Esto es independiente de la tecnología de base de datos que se use(SQL o NoSQL).

Es por eso que diversos proveedores de la nube(AWS, GCP, Azure, etc), ofrecen servicios de cloud computing conocidos como DBaaS,
que gestiona bases de datos en diversas escalas. En esencia un modelo de `leasing` para bases de datos.

Estos DBaaS ofrecen una gran conveniencia para gestionar las bases de datos; pero como antes se mencionó, se cuenta con muchos riesgos al
hacerse dependiente de un servicio en la nube, más aún si solo se depende de un solo proveedor. Se tiene una ontología de riesgos en la página 5 de _Risks in enterprise cloud computing_[@dutta2013risks].

Para aprovechar el costo de reducido que ofrece algun proveedor de la nube y a la vez mitigar los riegos mencionados, la mejor opción es usar
al proveedor solo como servicio de IaaS(báicamente hardware como servicio).
De esta forma es posible mover la infraestructura TI de proveedor a proveedor si se requiere.
El problema que viene al hacer esto, es que se pierde la conveniencia del servicio del proveedor que gestiona la base de datos.

La solución que se plantea a este problema, es crear una herramienta que ofrezca la conveniencia del DBaaS sobre un IaaS.

[^1]: https://www.gartner.com/smarterwithgartner/the-science-of-devops-decoded/

[^2]: https://www.ibm.com/developerworks/community/blogs/8f058ee2-f3aa-4976-aeb8-4e6102dc86f8/entry/what-is-database-as-a-service-dbaas?lang=en

## Importancia

Es de suma importancia a nivel empresarial y más aún a nivel gubernamental, proveer a los equipos de TI de herramientas que les permita gestionar
su propia infraestructura y controlar su propia información.

Cuál sería la postura de negociación que podría tomar una empresa si luego de 10 años de operación toda su información se encuentra asilada en el servicio de
determinado proveedor? Podrá migrar fácilmente si es que su actual proveedor aumenta los precios drásticamente?

No. Es seguro que la empresa estará ante un caso de aprisionamiento tecnológico(vendor lock-in).

## Técnica y su justificación

Se usarán las siguientes herramientas para implementar la solución:

- _NixOS_[@dolstra2008nixos], esta es una distribución de Linux que mejora el estado del arte en la gestión de configuración del sistema.
Nos brinda la capacidad de tener una configuración declarativa y puramente funcional, lo que permite hacer upgrades del sistema de forma confiable.

- _NixOps_[@dolstra2013charon], extiende las capacidades de NixOs para aprovisionamiento y despliegue de múltiples instancias en una red.
Esto nos brinda la capacidad de hacer despliegues multi-cloud(usar más de un proveedor de la nube) lo cual aumenta más la resiliencia de la base de datos.

Por qué usar estas herramientas en vez de la tendencia actual en contenedores, Kubernetes[^3]?

Los contenedores básicamente vienen a ser versiones más ligeras de las ya conocidas máquinas virtuales, a pesar de esto, los contenedores siguen
siendo una capa adicional sobre el sistema operativo. Tienen limitaciones en cuanto a uso de CPU y disco. Las bases de datos requieren uso a discreción
de los recursos que ofrece el sistema operativo. es por esto que los contenedores no son adecuados para gestionar bases de datos.

Con NixOS solo se gestionará daemons(demonios o servicios) que se ejecutarán nativamente en el sistema operativo.

[^3]: https://kubernetes.io/

\newpage

## Objetivo

Implementar esta herramienta y hacerla pública como proyecto Open Source. De esta forma se puede recibir feedback internacional y posiblemente usuarios
que la usen en producción. Además se probará en una empresa en particular. Así podemos validar la herramienta y sus aspectos funcionales y no-funcionales.

El alcance estará limitado a la gestión de PostgreSQL, puesto que es el gestor de bases de datos relacional Open Source más maduro que ofrece el mercado.

## Referencias
