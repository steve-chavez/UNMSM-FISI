---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{0pt}
- \usepackage{titling}
- \usepackage{array}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{float}
- \usepackage{listings}
- \usepackage{tikz}
- \usepackage{enumitem}
- \usepackage{multirow}
---


\title{Devops aplicado a PostgreSQL para la recuperación de desastres en la empresa Simply Connected Systems\vspace{2.0cm}}
\author{\textbf{Steve Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Asesor: Mgtr. Menéndez Mueras Rosa}

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Setiembre 2020}
\end{center}

\thispagestyle{empty}

\newpage

\renewcommand*\contentsname{Tabla de contenidos}
\tableofcontents

\renewcommand{\listtablename}{Índice de tablas}
\listoftables

\renewcommand{\listfigurename}{Índice de figuras}
\listoffigures

\newpage

# Introducción

## Realidad problemática

En la ya madura era de la información, es conocido que para una empresa u organización, los datos son el activo más valioso.
Para garantizar la perennidad de estos datos, es estándar que la organización posea un plan de **recuperación de desastres**.

Sin embargo, realizar este plan requiere el trabajo de un experto, el cual hace un trabajo ad hoc para cada base de datos; esto incurre en costo operacional y pérdida de agilidad en la organización.
O lo que es peor aún, no se realiza ningún plan, y la empresa queda a merced de riesgos tan grandes como el del **ransomware**.

Con **DevOps** y en particular con la infraestructura como código, se puede hacer del plan de recuperación
de desastres un proceso repetible y ágil. De fácil automatización y sin la necesidad de intervención de un experto para cada caso.

A nivel internacional, organizaciones maduras tienen infraestructura dedicada para la recuperación de desastres. Startups o empresas
pequeñas prefieren confiar en la nube y sus servicios de DbaaS, los cuales se ocupan de la recuperación de desastres de forma automática.

A nivel nacional, se puede observar que existen estándares planes de recuperación de desastres, pero estos están a un nivel documental.
No tienen un enfoque DevOps que permita poner en práctica el plan fácilmente.

A nivel organizacional, Simply Connected Systems(ver Anexo 01) - que brinda soluciones de TI, confía en la recuperación de desastres de los servicios de la nube
para ciertas empresas. Sin embargo, otras empresas que requieren de sus servicios cuentan con infraestructura propia y prefieren alojar
sus bases de datos y controlar su propia información. Estos son los casos que requieren de DevOps y la infraestructura como código para
garantizar y agilizar los planes de recuperación de desastres.

## Problema General

¿La aplicación de Devops en PostgreSQL puede garantizar un adecuado plan de recuperación de desastres?

### Problema Específico 1

¿Ante un desastre, se puede garantizar una pérdida mínima de datos usando Devops?

### Problema Específico 2

¿Ante un desastre, se puede garantizar una recuperación pronta de la base de datos usando Devops?

## Objetivo General

Aplicar Devops en PostgreSQL para garantizar un adecuado plan de recuperación de desastres.

### Objetivo Específico 1

Garantizar una pérdida de datos mínima usando Devops.

### Objetivo Específico 2

Garantizar una recuperación pronta de la base de datos usando Devops.

\newpage

# Trabajos Previos

Se muestran a continuación, trabajos previos relacionados a la **recuperación de desastres**, 3 nacionales y 3 internaciones, respectivamente.

## Implementación de una guía referencial para gestionar los riesgos informáticos en la universidad autónoma del perú

En Perú, en la Facultad de Ingeniería de la Universidad Peruana de Ciencias Aplicadas, en el año 2017,
Michael André Aguirre Abanto y Giomar Anthony Lopez Ynostroza presentan esta tesis para obtener grado académico de Ingeniero de sistemas.

Este trabajo tiene como enfoque la continuidad de negocios de los sistemas informáticos de la universidad autónoma del perú.
Para hacer esto se habla de la recuperación ante desastres. En el cual los backups son de vital importancia, se tienen en cuenta los backups del software base,
software aplicativo, bases de datos. Se mencionan posibles riesgos de desastre para el caso en particular, los cuales son: incendios, terremotos, inundaciones, sabotajes, robos y fallas en el hardware o software.

Así también se mencionan los indicadores para el resultado del procedimiento de recuperación. Citando textualmente:

"El resultado de este procedimiento de recuperación se puede determinar a partir de
indicadores como el RTO (Recovery Time Objective), que informa de en cuánto
tiempo se puede recuperar el sistema informático de la organización, así como el
RPO (Recovery Point Objective), que indica hasta dónde se puede recuperar el
sistema."

## Modelo para la gestión de la continuidad del servicio de tecnologías de la información para empresas de tipo burocracia profesional basada en la norma técnica internacional ISO 22301

En Perú, en la Facultad de Ingeniería de Producción y Servicios de la Universidad Nacional de San Agustín, en el año 2017, Oscar Alberto Ramírez Valdez
presenta esta tesis para alcanzar el grado académico de Magíster en Ingeniería de Sistemas.

En esta tesis se menciona la importancia de la resiliencia(capacidad de un sistema de soportar y recuperarse ante desastres)
en los sistemas de información. Se plantea un modelo de sistema de gestión de continuidad de servicio(SGC).

Se recalca la importancia de la recuperación de desastres y continuidad del negocio:

"Ninguna organización está libre de padecer algún problema que interrumpa la
normalidad de sus operaciones, los desastres suceden y las organizaciones no pueden
correr el riesgo de perder su información o lo que es más importante, la información
crítica de sus clientes. Los desastres pueden tener un impacto financiero importante
en las organizaciones y las interrupciones técnicas hacen que se pierdan clientes y el
tiempo de inactividad puede provocar el cierre de las organizaciones."

Así también se mencionan a los indicadores usuales RPO y RTO y añade dos indicadores más que también son pertinentes.

RTO: Recovery Time Objective, está asociado con la recuperación de recursos, tales
como sistemas de computación, equipos de manufactura e infraestructura física.

RPO:  Recovery Point Objective, este tiempo está relacionado, con la tolerancia que la
empresa puede tener, sobre la pérdida de datos, medidos en términos del tiempo entre el
último respaldo (backup) de datos y el evento del desastre.

MTD: Maximun Tolerable Downtime, consiste en el espacio de tiempo durante el cual
un proceso puede estar inoperante hasta que la empresa empiece a tener pérdidas y
colapse.

WRT: se calcula como el tiempo entre la recuperación del sistema y la normalización del procsamiento

## Diseño de un Plan de Recuperación de Desastres de TI (DRP TI) para el Centro de Cómputo de la sede principal de una entidad educativa superior del sector privado basado en la norma NIST SP 800-34

En Perú, en la Facultad de Ingeniería de la Universidad Peruana de Ciencias Aplicadas, en el año 2019, Yarlequé Gutiérrez Alfredo presenta
esta tesis para obtener el grado académico de Ingeniero de redes y comunicaciones.

En este trabajo se crea un documento de apoyo dentro de una entidad educativa
superior del sector privado para la elaboración de un Plan de Recuperación de Desastres de
TI. Con el fin de salvaguardar el bien más preciado de la organización que es la información.

También se menciona a la Gestión de la Continuidad del Negocio como un proceso holístico que identifica amenazas
potenciales a la organización e impactos a las operaciones del negocio.
Esto proporciona la estructura para construir resiliencia con capacidad para dar respuesta efectiva protegiendo los intereses
de las partes interesadas, el valor de la marca, la reputación y las actividades  creadoras de valor.

Así también se catalogan los tipos de amenazas en:

- Naturales: fenómenos naturales.
- Humanas: intervenciones humanas.
- Humanas no intencionadas: sin premeditación.
- Humanas intencionadas: provocadas por por personal interno(por descontento o varias razones) o externo.

## Effectiveness of Backup and Disaster Recovery in Cloud: A Comparative study on Tape and Cloud based Backup and Disaster Recovery

En Suecia, en Blekinge Institute of Technology, Faculty of Computing, Department of Communication Systems, en el año 2015, Yarrapothu, Sindhura
presenta esta tesis para alcanzar el grado académico de Master of Science in Electrical Engineering with emphasis on Telecommunication Systems.

En este trabajo se discute la efectivdad del backup en un plan de reucperación de desastres. Se analiza la efectividad
de un backup en cinta y de un backup en un storage de la nube.

Se mide las ya conocidas metricas de RPO y RTO respecto a backups en cinta y nube. Se concluye que el backup en nube brinda
mejores resultados que el de cinta.

## Challenges and Opportunities of Having an IT Disaster Recovery Plan

En Suecia, en Umeå University, Faculty of Social Sciences, Department of Informatics, en el año 2017, Ghannam, Mohamed Ziyad
presenta esta tesis para alcanzar el grado académico de Master in IT Management.

En este trabajo, se hace una investigación cualitativa mediante una serie de entrevistas en diversas organizaciones, para determinar los retos y oportunidades de tener un plan de recuperación de desastres.

Se concluye que los principales retos son tener apoyo de la alta gerencia y disponibilidad de personal, y como principales beneficios se tiene la protección de los datos y la reducción de la interrupción de las funciones del negocio.

Así también los entrevistados mencionan los principales desastres ocurridos en sus organizaciones:

- Corte de energía: 70%
- Fuego: 69%
- Gestión de la configuración: 64%
- Cyber ataques: 63%
- Empleados maliciosos: 63%
- Pérdida de datos: 63%

## Database System Architecture for Fault tolerance and Disaster Recovery

En Estados Unidos, en Regis University, School of Computer & Information Science, en el año 2009, Anthony Nguyen presenta esta tesis
para alcanzar el grado académico de Master in Information Technology Management.

En este trabajo se plantea una arquitectura adecuada para la tolerancia a fallos y la recuperación de desastres. Usa la base de datos Oracle.

Se mencionan las siguientes estrategias de recuperación de desastres:

- Cluster con failover(Failover Clustering)
- Espejo de la base de datos(Database mirroring)
- Replicación par a par(Peer-to-peer transactional replication)
- Mantenimiento de una base de datos de reserva en caliente(Maintenance of a warm standby database)
- Backup y restauración(Backup and restore)
- Redundancia de datos usando arreglos de discos(RAID)

\newpage \ \newpage

# Bases teóricas

## Variable Independiente: DevOps

Según @artac2017devops, **DevOps** comprende una serie de tácticas de ingeniería de software que apuntan a
reducir el tiempo accionable de cambio de diseño de software. Una de estas tácticas es aprovechar la **infraestructura como código**,
esto es, escribir un "blueprint" que contiene especificaciones de despiegue, listo para la orquestación en la nube.

Según @sushmaautomation, **DevOps** es un método de desarrollo de software que permite la colaboración de equipos de desarrollo de software y operaciones
para mejorar la calidad del software entregado. Ayuda a los equipos con la integración continua, el planeamiento continuo,
el despliegue continuo y el feedback continuo.

Según @smeds2015devops, **DevOps** aborda el desafío de lo que a menudo
descrito como una brecha entre el personal de desarrollo y de operaciones.
Informes traen expectativas positivas de DevOps y las organizaciones se están volviendo
cada vez más interesados en el fenómeno y en cómo aprovechar los posibles beneficios del mismo.
Aún así, el término en sí está rodeado de ambigüedad. Mientras que el propósito
es claro, "cerrando la brecha entre el desarrollo y las operaciones", hay
todavía hay muchas interpretaciones de lo que realmente significa DevOps.

Para este trabajo, nos enfocaremos en la **infraestructura como código**, procederemos a definirlo a continuación.

### Infraestructura como Código(IaC)

Según @hummer2013testing, uno de los pilares del DevOps es la noción de la infraestructura como código(IaC), que facilita el desarrollo de lógica de automatización
para desplegar, configurar y actualizar componentes interrelacionados. Las automatizaciones que provee la IaC, son repetibles por diseño,
así que pueden llevar al sistema a un estado deseado empezando por cualquier otro estado arbitrario.

Según @artac2018infrastructure, como parte de las tácticas de DevOps, la infraestructura como código(IaC), provee
la habilidad de crear, configurar y manejar infraestructuras complejas mediante código ejecutable.

Así también, @artac2018infrastructure, menciona las 5 típicas actividades de DevOps:

**La gestión de la configuración** es el proceso de desplegar y manejar todos los servicios requeridos(por ejemplo Tomcat, MySQL, Hadoop, Cassandra).
Consume recetas reusables que describen como manejar y configurar el servicio a través de su ciclo de vida.

**El aprovisionamiento** implica adquirir (por ejemplo, de un proveedor público de infraestructura como servicio),
configurar y ejecutar las máquinas virtuales o los contenedores necesarios en los que se pueden ejecutar los servicios.

**El despliegue** es la fase en la que se ejecutan las aplicaciones del usuario en la infraestructura resultante.

**El monitoreo** de todo componente en ejecución (máquinas virtuales, servicios, middleware y aplicaciones) es un habilitador básico para
casi cualquier tipo de actividad de gestión del tiempo de ejecución.

**La autoadaptación** se trata de aplicar un conjunto de métodos (p. Ej.  Autoescalado de VM, recuperación de fallas, etc.) para mantener el estado
conforme con los acuerdos de nivel de servicio y la calidad de las variables de servicio (QoS) en el mismo (por ejemplo, seguridad, red
seguridad, etc.).

Para este trabajo nos enfocaremos en las 3 primeras actividades: **la gestión de la configuración**, el **aprovisionamiento** y el **despliegue**.

## Variable Dependiente: Recuperación de Desastres

Según @yarleque2019drp, es un proceso documentado o conjunto de procedimientos o acciones para recuperar y
proteger la infraestructura de TI de una organización en caso de un desastre. Refiriéndose
con desastre a todo evento súbito, imprevisto catastrófico que interrumpe los procesos de
negocio lo suficiente como para poner en peligro la viabilidad de la organización. Un
desastre podría ser el resultado de un daño importante a una parte de las operaciones, la
pérdida total de una instalación, o la incapacidad de los empleados para acceder a esa
instalación.

Según @ramirez2017bc, es un documento que contiene un conjunto de acciones y procedimientos definidos previamente, con
responsabilidades claramente establecidas, para la recuperación del componente tecnológico, sistemas y servicios de telecomunicaciones.

Según @aguirreLopez2017risks, se encarga de la restauración de las aplicaciones de misión crítica, es decir, las más importantes en una
organización. Se enfoca en el fallo de la instalación o de sus componentes por causas externas, como pueden ser el mego en el centro de datos, o un
fallo del servidor crítico. Algunas de las soluciones posibles son la recuperación, tanto del sistema como de los datos, en una
localización diferente.

### Indicadores

Vamos a desarrollar los indicadores de acorde a lo mencionado por @aguirreLopez2017risks.

El resultado del procedimiento de recuperación de desastres se puede determinar a partir de
indicadores como el **RTO**(Recovery Time Objective), que informa de en cuánto
tiempo se puede recuperar el sistema informático de la organización, así como el
**RPO** (Recovery Point Objective), que indica hasta dónde se puede recuperar el
sistema.

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/rporto.png}
  \caption{RPO y RTO}%
\end{figure}

### Punto objetivo de recuperación(RPO)

Es la cantidad máxima de datos que se pueden perder cuando se restablece el servicio después de una interrupción.
El punto de recuperación objetivo se expresa en un período de tiempo antes de la falla.
Por ejemplo, un punto de recuperación objetivo de un día puede ser apoyado por los backups diarios, y donde se pueden perder datos
de hasta 24 horas.

\begin{equation*}
\boldsymbol{RPO = FechaHoraDesastre - FechaUltimoBackup}
\end{equation*}

Cero es un valor válido y es equivalente a un requisito de "pérdida de datos cero".

### Tiempo objetivo de recuperación(RTO)

Es el tiempo máximo permitido para la recuperación de un servicio después de una interrupción.

\begin{equation*}
\boldsymbol{RTO = FechaHoraRecuperacion - FechaHoraDesastre}
\end{equation*}

\newpage \ \newpage

# Metodología de la investigación

## Tipo de investigación

Investigación aplicada, puesto que buscar dar solución a un problema específico en una organización.

## Hipótesis General

La aplicación de Devops en PostgreSQL garantizará un adecuado plan de recuperación de desastres.

## Hipótesis Específica 1

Devops permite garantizar la pérdida de datos mínima ante un desastre

## Hipótesis Específica 2

Devops permite garantizar la recuperación pronta de la base de datos ante un desastre.

## Diseño de la investigación

Experimental, se manipulará la variable independiente DevOps(en particular la especificación de infraestructura como código)
para influir en la variable dependiente recuperación de desastres.

## Herramientas

Para este proyecto se analizaron herramientas de Devops en base al **modelo de gestión de sistemas**.

### Métodos de gestión de sistemas

De acuerdo a @Traugott:2002:WOM:1050517.1050529:

Todos los métodos de gestión de sistemas pueden ser clasificados en tres categorías: divergente, convergente, y congruente.

Esto es importante puesto que nos que nos permitirán evaluar las diferentes herramientas de **infraestructura como código** que nos ofrece el mercado.

#### Método divergente

La divergencia es caracterizada por la configuración del sistema alejándose del estado ideal del sistema.

Esto es ilustrado en el flujo de trabajo típico de un sysadmin, el cual sigue un checklist y manualmente ejecuta paso por paso los comandos
necesarios para alcanzar el estado del sistema deseado. Si 3 distintos sysadmins siguen el mismo checklist, es probable que se obtengan 3 resultados distintos.

Este viene a ser el modelo tradicional, donde se usan scripts bash para automatizar algún despliegue. Esto no es considerado
**Infraestructura como código**, puesto que sigue un enfoque imperativo y no declarativo.

#### Método convergente

La convergencia es caracterizada por la configuración del sistema acercándose al estado ideal del sistema.

Esta es una mejora sobre el modelo divergente, en este caso se usan herramientas más maduras
que permiten declarar una descripción del sistema. Algunas herramientas actuales del mercado son: **Puppet**, **Ansible**.
El problema es que estás herramientas aún pueden diverger del estado ideal, es necesario
monitorear las diferencias entre la descripción del sistema y estado ideal deseado.

#### Método congruente

La congruencia es la práctica de mantener la configuración del sistema en completa igualdad al estado ideal del sistema.

Este modelo es caracterizado por herramientas actuales como **contenedores**(Docker, LXC) o **NixOS**(@dolstra2008nixos).
En estos casos el estado real del sistema es un espejo de la descripción del sistema.

### Herramientas de Devops

Ahora veremos las herramientas más populares actualmente que permiten gestionar la configuración del sistema a través de la Infraestructura como
Código.

#### Docker

Docker es una herramienta que permite crear entornos llamados contenedores,
compartir estos y correr aplicaciones aisladas en ellos.

El concepto de contenedores es similar a las máquinas virtuales.
Mientras que una máquina virtual ejecuta recursos virtualizados, un contenedor
comparte los recursos con su host.

Docker usa distintas técnicas ofrecidas por el kernel de Linux, para aislar
y abstrar el contenedor del host.

Otro aspecto importante de Docker es que permite compartir y distribuir imágenes.
Una imagen es derivada de una imagen base, por ejemplo ubuntu o fedora.
Y encima de este entorno el usuario puede realizar diferentes acciones que producen otra capa.

Un ejemplo de configuración en Docker es el siguiente:

```docker
FROM ubuntu:latest
ADD . /data
WORKDIR /data
RUN apt-get update
RUN apt-get install -y python
EXPOSE 8080
CMD python -m SimpleHTTPServer 8080
```

Este copia el directoria actual a `/data` y crea un servidor HTTP simple que corre en el puerto 8080.

#### Puppet

Puppet es una herramienta que consiste de un servidor y multiples clientes, sobre los cuales
el servidor define una configuración.

Las distintas configuraciones son agrupadas en un llamado **manifest** y estas son guardadas
en el servidor. Estos **manifests** son los que describen el estado deseado para cada uno de los clientes.
Los clientes se conectan al servidor, obtienen los **manifests** y aplican su contenido.

El siguiente es un ejemplo de configuración en Puppet:

```puppet
package { "apache2"
  ensure => installed
}
user { "admin"
  ensure => present
}
file {'foo' :
  path    => '/tmp/foo',
  ensure  => present,
  mode    => 0640,
  content => "Soy el archivo"
```

Si el **manifest** se aplica se asegurara de que el paquete "apache2" exista, que
el usuario "admin" esté presente y que un archivo "/tmp/foo" con el contenido "Soy un archivo"
esté presente.

#### NixOS

**NixOS**[@dolstra2008nixos], es una distribución de Linux que mejora el estado del arte en la gestión de configuración del sistema.
Nos brinda la capacidad de tener una configuración declarativa y puramente funcional, lo que permite hacer upgrades del sistema de forma confiable.

Un ejemplo de **configuración declarativa** se puede ver a continuación:

```nix
services.postgresql = {
  enable = true;
  package = pkgs.postgresql100;
  extraPlugins = [
    postgis
    pgrouting
  ];
  authentication = pkgs.lib.mkOverride 10 ''
    # Acepta Todas las conexiones IPv4 locales:
    host    all all 127.0.0.1/32 trust
  '';
  extraConfig = ''
    # Logging de todas las sentencias
    log_statement = 'all'
  '';
};
```

Con esta especificación, NixOS puede crear una instancia de PostgreSQL con las siguientes características:

- Instala la versión especificada de PostgreSQL(versión 10 en este caso).
- Está automáticamente "daemonizado"(se ejecuta indefinidamente). No hay necesidad de agregar configuraciones adicionales.
- Define una lista de extensiones requeridas. Estas ya vienen incluidas y no hay necesidad de ejecutar comandos adicionales.
- Define opciones de "logging" y autenticación. Sin necesidad de ubicar archivos adicionales.

Estas tareas involucraría varios pasos en cualquier otra distribución de Linux u otro sistema operativo.

### Comparación de herramientas

\begin{table}[H]
  \small
  \center
  \begin{tabular}{ m{0.2\textwidth} m{0.2\textwidth} m{0.2\textwidth} m{0.2\textwidth} } \hline
  &
  \textbf{Puppet} &
  \textbf{Docker} &
  \textbf{NixOS} \\ \hline
  \textbf{Modelo} &
  Convergente &
  Congruente &
  Congruente \\ \hline
  \textbf{Sin divergencia} &
  Difícil &
  Garantizado &
  Garantizado \\ \hline
  \textbf{Resultados reproducibles} &
  Difícil &
  Posible &
  Garantizado \\ \hline
  \textbf{Componentes} &
  SO + Puppet en servidor y clientes &
  SO + Docker + Gestor de Contenedores &
  NixOS \\ \hline
  \textbf{Facilidad de uso} &
  Difícil &
  Fácil &
  Difícil \\ \hline
  \end{tabular}
  \caption{Comparación de herramientas de gestión de sistemas}
\end{table}

Desde el punto de vista de congruencia, NixOS y Docker(**modelo congruente**) son superiores a Puppet(**modelo convergente**) puesto
que garantizan que el resultado de la configuración sea el estado deseado.

Desde el punto de vista de componentes, NixOS es superior a Docker, puesto que Docker añade una capa de virtualización,
ciertamente es una capa mucho más ligera que una máquina virtual, sin embargo nuestra propuesta involucra **bases de datos**, las
cuales requieren uso a discreción del hardware.

### PostgreSQL vs MySQL

Tanto Oracle como SQL Server solo ofrecen licencias comerciales y no son open source, lo cual las descalifica para nuestra propuesta.
Requerimos que la herramienta sea open source para poder extenderla si es necesario. Además que estos SGBDs comerciales
van a favor del riesgo de vendor lock-in, que justamente buscamos combatir.

Eso nos deja a PostgreSQL y MySQL, sobre los cuales haremos un cuadro comparativo.

\begin{table}[H]
  \small
  \begin{tabular}{ m{0.2\textwidth} m{0.4\textwidth} m{0.4\textwidth} }
  \hline
  &
  \textbf{PostgreSQL} &
  \textbf{MySQL} \\
  \hline
  \textbf{Gobernanza} &
  Es libre y open source y tiene la licencia PostgreSQL similar a la licencia MIT &
  Aunque es open source. El desarrollo es manejado por Oracle, el cual ofrece versiones pagadas para uso comercial \\
  \hline
  \textbf{Conformidad con SQL} &
  Ofrece total conformidad con el standard SQL &
  Parcialmente conforme con SQL, no implementa todo el standard(por ejemplo CHECK constraints) \\
  \hline
  \textbf{Comunidad} &
  Comunidad madura que sigue contribuyendo y aumentando funcionalidades de forma estable &
  Comunidad grande que se enfoca en mantener funcionalidades pasadas y raramente añade nuevas\\
  \hline
  \textbf{Replicación} &
  Incluye replicación maestro-esclavo y otros tipos de replicación son soportadas a través de extensiones. &
  Incluye replicación maestro-esclavo y maestro-maestro. \\
  \hline
  \textbf{Soporte NoSQL/JSON} &
  Múltiples soportados(JSON, JSONB, hstore, grafos a través de extensiones) &
  Solo JSON \\
  \hline
  \textbf{Sistema de Tipos extensible} &
  Su arquitectura soporta la extensibilidad. Es posible añadir nuevos tipos de datos, tipos de índice, etc. &
  No soporta extensibilidad. \\
  \hline
  \textbf{Extensible con lenguajes de programación} &
  Soporta extensibilidad en variedad de lenguajes, incluyendo python, java, javascript, .net, R, ruby y otros. &
  Solo soporta C, y su arquitectura no está diseñada para la extensibilidad. \\
  \hline
  \end{tabular}
  \caption{Comparación de PostgreSQL y MySQL}
\end{table}

Concluimos eligiendo PostgreSQL, puesto que presenta una arquitectura madura y sigue evoluacionando gracias a su extensibilidad.
Mientras MySQL se enfoca más en estabilizar sus funcionalidades presentes y su desarrollo se nota estancado.

\newpage \ \newpage

# Prueba de Concepto

## Pre-test

Al desplegar una nueva instancia de PostgreSQL, sin configuración alguna para recuperación de desastres(lo cual es bastante
común, incluso en sistemas que llevan años en producción), se tienen las siguientes medidas:

\begin{table}[H]
  \small
  \center
  \begin{tabular}{l l} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  $\infty$ &
  $\infty$ \\ \hline
  \end{tabular}
  \caption{RPO/RTO inicial}
\end{table}

Basícamente la pérdida de datos es infinita y el tiempo de recuperación también es infinito.
Nuestro objetivo es mejorar estas medidas a resultados más aceptables para la recuperación de desastres.

## Post-test

Se desea llegar a los siguientes resultados:

\begin{table}[H]
  \small
  \center
  \begin{tabular}{l l} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  ~ 0 &
  < 1 min \\ \hline
  \end{tabular}
  \caption{RPO/RTO deseado}
\end{table}

Lo que básicamente significa que existe pérdida de datos 0 y un tiempo de recuperación menor a 1 minuto. Lo que es ideal
para una base de datos de pequeño tamaño que se usará para la prueba de concepto.

## Entorno de la Prueba de Concepto

### NixOS y NixOps

Por los motivos presentados en la comparación de herramientas, usaremos NixOS para la prueba de concepto.

Gracias a NixOS podemos usar a _NixOps_[@dolstra2013charon], que extiende las capacidades de NixOS para aprovisionamiento y
despliegue de múltiples instancias en una red. Esto nos brinda la capacidad de hacer despliegues multi nube lo cual aumenta más la
resiliencia de la base de datos.

### Base de datos

Vamos a usar una base de datos pública conocida como World. En ella se listan todos los países, ciudades e idioma hablado en el mundo.
Esta base de datos tiene la siguiente estructura:

\begin{figure}%
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-er.png}
  \caption{Diagrama ER de World}%
\end{figure}

### AWS EC2

Usaremos el servicio de IaaS de AWS(conocido como EC2) para desplegar nuestras instancias de PostgreSQL.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{images/ec2-logo.jpeg}
  \caption{AWS EC2}%
\end{figure}

### AWS S3

Usaremos el servicio de almacenamiento de AWS(conocido como S3) para guardar los backups de PostgreSQL.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.3\textwidth]{images/s3.png}
  \caption{AWS S3}%
\end{figure}

### Herramientas UNIX

Para interactuar con la base de datos usaremos **psql** que es provista por PostgreSQL por defecto y funciona en terminales tradicionales.
Para conectarnos a la instancia de EC2 usaremos **ssh** y para interactuar con NixOS usaremos **bash**.

Todas estas son herramientas UNIX típicas en cualquier entorno.

## Proceso de la Prueba de Concepto

Ahora veremos como se logró la prueba de concepto, paso a paso.

### Configuración de servidor en EC2 y almacenamiento en S3

Primero definiremos los recursos en la nube de AWS, esto se hace usando una especificación en archivo nombrado **ec2-s3.nix**.
Esto luego nos servirá para desplegarlo con NixOps.

\begin{lstlisting}[basicstyle=\scriptsize, language=sh, caption=Configuración de EC2 y S3, numbers=left, captionpos=b]]
let
  region = "us-east-2";
  accessKeyId = "dev";

  ec2 = { resources, ... }:
    {
      deployment.targetEnv = "ec2";
      deployment.ec2 = {
        accessKeyId = accessKeyId;
        region = region;
        instanceType = "t2.micro";
        associatePublicIpAddress = true;
        keyPair = resources.ec2KeyPairs.pg-key-pair;
      };
    };
in
{
  resources = {
    ec2KeyPairs.pg-key-pair = {
      inherit region accessKeyId;
    };
    s3Buckets.poc-362 = {
      inherit region accessKeyId;
      versioning = "Suspended";
    };
  };
  pg = ec2;
  ## pg-recovery = ec2;
}
\end{lstlisting}

En 2 definimos la zona de disponibilidad en la cual sera desplegado nuestro servidor.

En 3 indicamos el nombre del archivo de nuestras credenciales de AWS(necesitamos estas para autenticarnos).

De 5 a 15 definimos las características de nuestro servidor en EC2. Notoriamente en 11, se define el tipo de instancia ec2(t2.micro),
que viene a ser una de uso gratuito, con limitados recursos, para fines de demostración.

De 19 a 21 definimos una llave(key pair) que le servirá a NixOps para acceder a nuestro servidor.

De 22 a 25 definimos el almacenamiento S3, estas unidades de almacenamiento son conocidas como "buckets" las cuales deben
tener un nombre único, en este caso `poc-362`.

En 27 definimos que todas estas características de ec2 serán destinadas a nuestro servidor dedicado para PostgreSQL(abreviado
usualmente como pg).

Finalmente, en la **línea 28**, hemos comentado la instancia de PostgreSQL que nos servirá de recuperación, la cual no se desplegará
al inicio.

### Configuración de instancia primaria de PostgreSQL

Aquí haremos la configuración de PostgreSQL, definida en un archivo **pg.nix**.

\begin{lstlisting}[basicstyle=\scriptsize, language=sh, caption=Configuración de instacia primaria de PostgreSQL, numbers=left, captionpos=b, showstringspaces=false, breaklines=true]]
{
  network.description = "Postgres servers";

  pg = { config, pkgs, resources, ... }: {
    environment.systemPackages = [
      pkgs.awscli
    ];
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      authentication = pkgs.lib.mkForce ''
        local all all trust
        local replication postgres trust
      '';
      extraConfig = ''
        wal_level = archive
        archive_mode = on
        max_wal_senders = 10
        archive_command = '${pkgs.awscli}/bin/aws s3 cp %p s3://${resources.s3Buckets.poc-362.name} 1>&2'
      '';
    };

    systemd.services.postgresql = {
      environment = {
        AWS_ACCESS_KEY_ID = builtins.readFile ./key_id.secret;
        AWS_SECRET_ACCESS_KEY = builtins.readFile ./access_key.secret;
      };
    };
  };

  pg-recovery = { config, pkgs, resources, ... }: {
    environment.systemPackages = [
      pkgs.awscli
    ];
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      recoveryConfig = ''
        restore_command = '${pkgs.awscli}/bin/aws s3 cp s3://${resources.s3Buckets.poc-362.name}/%f %p'
        # Especificar el tiempo de restauracion deseado
        # recovery_target_time='2018-12-22 00:04:00'
      '';
    };

    systemd.services.postgresql = {
      environment = {
        AWS_ACCESS_KEY_ID = builtins.readFile ./key_id.secret;
        AWS_SECRET_ACCESS_KEY = builtins.readFile ./access_key.secret;
      };
    };
  };
}
\end{lstlisting}

En 31 definimos el nombre de nuestra red de instancias de PostgreSQL.

De 33 a 59 definimos las caracteristicas de nuestro servidor que contendrá a PostgreSQL.

Primero, de 34 a 36 definimos un paquete adicional(aws-cli) que nos servirá para enviar los backups incrementales a S3.

De 37 a 50 definimos las características de PostgreSQL.

Esencialmente en 44 a 49, definimos que queremos backups incrementales, en PostgreSQL estas piezas de información son conocidas
como WAL logs. De 45 a 47 habilitamos el envío de estos WAL, y en 48 enviamos estos WALs a nuestro anteriormente definido S3 bucket
`poc-362`.

De 60 a 80, definimos nuestra instancia de PostgreSQL de recuperación, que inicialmente no será desplegada.

De 67 a 71, se define cómo se hará la recuperación. Primero en 68, se define que se consumirán los WAL logs enviados por la instancia
primaria, opcionalmente en 70, se puede especificar el punto de tiempo de recuperación deseado.

Finalmente tanto como de 52 a 58 y de 74 a 79 se definen las credenciales usadas por `aws-cli` en ambas instancias.

### Provisión de los recursos a EC2 y S3

Ahora podemos desplegar los recursos ya definidos a la nube de AWS. Para esto nos ayudará NixOps. Primero crearemos un registro
local de este despliegue, usando los dos archivos previamente definidos y el comando `nixops`:

```
nixops create ec-s3.nix pg.nix -d poc
```

A este registro local lo llamamos **poc**.

Ahora estamos listo para desplegar a la nube los recursos, nuevamente usando `nixops` desplegaremos **poc**:

```
nixops deploy -d poc
```

Y ahora podemos ver en la interfaz gráfica de AWS que la instancia EC2 fue creada:

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ec2-dashboard-1.png}
  \caption{Instancia primaria de PostgreSQL en EC2}%
\end{figure}

Se puede notar el nombre que le dimos a nuestra red "Postgres Servers" y el nombre de la instancia que desplegamos("pg").

Así también podemos ver el bucket S3 creado:

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/s3-dashboard-1.png}
  \caption{Bucket S3 para almacenamiento de backups incrementales}%
\end{figure}

Este finaliza con el nombre que le asignamos **poc-362**, puesto que NixOps le asigna un identificador que asegura su unicidad.

### Exploración de la base de datos

Manualmente, descargaremos y crearemos la base de datos **World** antes mencionada, en nuestra instancia de PostgreSQL en AWS.
Esta base de datos fue obtenida de https://wiki.postgresql.org/wiki/Sample_Databases. Para esto nos conectamos primero a nuestra
instancia con `nixops`:

```
nixops ssh -d poc pg
```

Una vez descargada el script de la base de datos **World** la crearemos en nuestra instancia de PostgreSQL:

```
create database world;
\i world.sql
```

Se puede notar que los WALs están siendo enviados a S3.

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/s3-dashboard-2.png}
  \caption{Enviado de backups incrementales a S3}%
\end{figure}

Ahora le daremos un vistazo a la data de la tabla **countrylanguage** usando **psql**:

```
psql -U postgres
\c world
SELECT * FROM countrylanguage;
```

Y estos son los resultados:

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-1.png}
  \caption{Data inicial de la bd World}%
\end{figure}

Se puede ver la lista de códigos de país con sus lenguajes más usados según porcentaje.

Teniendo un total de 984 registros según la sentencia SQL con el **count**.

### Simulación del desastre en la base de datos

Ahora simularemos el desastre en la base de datos, con el ya clásico DELETE sin WHERE:

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-2.png}
  \caption{DELETE sin WHERE}%
\end{figure}

### Recuperación de desastres

Ahora que ocurrió el error, lo que haremos será simplemente descomentar la **línea 28** del archivo **ec2-s3.nix** y desplegar
nuevamente con **nixops**, a su vez mediremos el tiempo de este despliegue con el comando **time**:

```
time nixops deploy -d poc
```

El tiempo total que reporta **time** para la finalización del despliegue es:

```
nixops deploy -d poc 19.46s
```

Podemos ver en el dashboard de AWS a la nueva instancia llamada **pg-recovery**:

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ec2-dashboard-2.png}
  \caption{AWS EC2 con instancia de recuperacion}%
\end{figure}

Y ahora nos conectaremos a esta instancia:

```
nixops ssh -d poc pg-recovery
```

Y veremos los contenidos de la tabla **countrylanguage**:

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-3.png}
  \caption{Base de datos recuperada}%
\end{figure}

Lo cual indica que la recuperación fue exitosa.

## Resultado de la Prueba de Concepto

Con los pasos anteriores ejecutados, se logró la siguiente medida:

\begin{table}[H]
  \small
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  0 &
  19.46s < 1 min \\ \hline
  \end{tabular}
  \caption{RPO/RTO final}
\end{table}

Lo que básicamente significa que existe pérdida de datos 0 y un tiempo de recuperación menor a 1 minuto. Lo que es ideal
según las características ofrecidas por un DbaaS.

\newpage

# Anexos

## Anexo 01: Entrevista para determinar la problemática operativa actual en la empresa Simply Connected Systems

\hfill \break

\begin{center}
  \begin{tabular}{ | l | l |} \hline
  \textbf{Nro Entrevista} & 1 \\ \hline
  \textbf{Entrevistado} & Steven Kramer \\ \hline
  \textbf{Cargo} & Chief Connection Officer \\ \hline
  \textbf{Fecha} & 2019/10/12 \\ \hline
  \end{tabular}
\end{center}

\hfill \break

### A qué se dedica Simply Connected Systems?

Simply Connected Systems[^1] está basada en Coral Springs en el estado de Florida, Estados Unidos.

Nos dedicamos a integrar sistemas infórmaticos corporativos de la forma más simple. Ofrecemos soluciones informáticas móviles
no disruptivas, customizables, asequibles, sin papel, sin retipeo, que permiten a las empresas mantener sus sistemas informáticos
heredados, flujo de trabajo e información.

Nos enorgullecemos en tener como caso de éxito la integración informática de una compañía eléctrica que ha operado por
4 generaciones(casi 100 años). Nuestras soluciones están pensadas para ser de bajo mantenimiento y lo más duraderas posible.

Actualmente nos estamos enfocando en un nuevo mercado: empresas de servicios de restauración. Estas empresas reparan daños
ocasionados por desastres naturales en distintas instalaciones. Hacen esto con maquinaria que trabaja "on site", la cual
se encarga de la dehumidificación del aire, absorción de agua, etc. Esta maquinaria en ocasiones se extravia,
por lo cual estamos proveyendo un sistema de monitoreo, usando dispositivos IoT, que le permita a la empresa
identificar la posición y el estado de la maquinaria.

El desarrollo de estos sistemas es algo nuevo para nosotros y hacemos uso extenso de la nube con sus distintos proveedores.

### El desarrollo de estos sistemas presenta nuevas dificultades?

Sí, existen diversas tareas que escapan a la expertiz del equipo de Simply Connected Systems. Por lo cual hacemos uso
de diversos contratistas de distintas nacionalidades. Usamos la plataforma Upwork[^2] para encontrarlos y contratarlos, esto nos
ha funcionado muy bien.

Así también, la nube ha reducido mucho nuestro costo operativo. En particular, para manejar nuestras bases de datos PostgreSQL[^3]
usamos AWS RDS[^4] y este nos ofrece muchas ventajas pero aquí viene nuestro principal problema.

Aunque nuestras soluciones basadas en la nube son suficientes para empresas pequeñas, empresas más grandes, con mayor madurez,
con politicas de gobernanza de datos y aversión al riesgo, desean alojar nuestras soluciones en infraestructura privada.

Sin embargo, AWS RDS no dispone de una versión "on premise". Para cumplir con los requisitos de estas empresas,
nuestros sistemas tendrían que replicar todas las ventajas que AWS RDS nos ofrece e instalarlos de una forma reproducible
para cada uno de nuestros clientes.

[^1]: https://www.simplyconnectedsystems.com
[^2]: https://upwork.com
[^3]: http://postgresql.org/
[^4]: https://aws.amazon.com/rds/

### Una versión "on-premise" de AWS RDS solucionaría el problema?

Sí. Sabemos que AWS RDS al manejar PostgreSQL ofrece funcionalidades como: backups incrementales, recuperación en un punto de tiempo,
alta disponibilidad, etc, en las cuales muchas empresas confían para garantizar la robustez de sus bases de datos.

Sin embargo, muchas empresas han manejado a PostgreSQL exitosamente en tiempos anteriores al boom del cloud computing.
Esto es debido al gran ecosistema open source que mantiene PostgreSQL. AWS RDS admite combinar distintos componentes open source
gratuitos que el ecosistema ofrece.

Creemos que en estos tiempos de DevOps, esta tarea, antes herculeana, es ahora factible.

Esto nos permitiría replicar un sistema robusto en cualquier infraestructura, tanto pública como privada.

\newpage

## Anexo 02: Diagrama de Ishikawa

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip

\usetikzlibrary{arrows,shapes.geometric,positioning,matrix}
\tikzset{
  ishikawa/.style={align=center, inner sep=0pt},
  matter/.style  ={rectangle, minimum size=6mm, very thick, draw=red!70!black!40,
    top color=white, bottom color=red!50!black!20, font=\itshape},
  level_1/.style ={ellipse, node distance=60pt, minimum size=6mm, very thick,
    draw=red!50!black!50, top color=white, bottom color=red!50!black!20, font=\itshape},
  level_2/.style={rectangle, minimum size=6mm, font=\itshape, font=\scriptsize}}
\tikzset{
  rows/.style 2 args={@/.style={row ##1/.style={#2}},@/.list={#1}},
  cols/.style 2 args={@/.style={column ##1/.style={#2}},@/.list={#1}},
}
\begin{tikzpicture}
\matrix[
  matrix of nodes,
  row sep=3cm,
  column sep=1cm,
  rows={1,3}{nodes=level_1},
  rows={2}{nodes=matter,anchor=center}
] (m) {
Atributos de calidad & Recursos humanos & \\
         &          & Recuperación de desastres \\
Requerimientos  & Riesgos de la nube & \\
};
\path[very thick,
  toarr/.style={->, shorten <=+0pt, shorten >=+.1cm},
  fromarr/.style={<-, shorten >=+0pt, shorten <=+.1cm}]
  [toarr]
  (m-1-1|-m-2-3) edge (m-2-3) % mid(left to right) arrow

  (m-1-1) edge[xslant=-.5]
    coordinate[pos=.3]   (@-1-1-1)
    coordinate[near end] (@-1-1-2) (m-1-1|-m-2-3)
  [fromarr]
  (@-1-1-1) edge node[above,level_2]{Alta disponibilidad} ++ (left:2.9cm)
  (@-1-1-2) edge node[above,level_2]{Alta confiabilidad} ++ (left:2.7cm)

  [toarr]
  (m-1-2) edge[xslant=-.5]
    coordinate[pos=.3]   (@-1-2-1)
    coordinate[near end] (@-1-2-2) (m-1-2|-m-2-3)
  [fromarr]
  (@-1-2-1) edge node[above,level_2]{Carencia de DBA} ++ (left:2.5cm)
  (@-1-2-2) edge node[above,level_2]{Dependencia en contratistas} ++ (left:3.8cm)

  [toarr]
  (m-3-1) edge[xslant=.5]
    coordinate[pos=.3]   (@-3-1-1)
    coordinate[near end] (@-3-1-2) (m-3-1|-m-2-3)
  [fromarr]
  (@-3-1-1) edge node[above,level_2]{Infraestructura privada} ++ (left:2.8cm)

  [toarr]
  (m-3-2) edge[xslant=.5]
    coordinate[pos=.3]   (@-3-2-1)
    coordinate[near end] (@-3-2-2) (m-3-2|-m-2-3)
  [fromarr]
  (@-3-2-1) edge node[above,level_2]{Vendor lock-in} ++ (left:2cm)
  (@-3-2-2) edge node[above,level_2]{Dependencia en RDS} ++ (left:2.5cm)
;
\end{tikzpicture}

\newpage

## Anexo 03: Matriz de consistencia

\begin{table}[H]
  \tiny
  \begin{tabular}{ | m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} |}
  \hline
  \textbf{PROBLEMAS} &
  \textbf{OBJETIVOS} &
  \textbf{HIPÓTESIS} &
  \textbf{VARIABLES} &
  \textbf{INDICADORES} \\
  \hline
  \textbf{Problema General} \newline
  ¿La aplicación de Devops en PostgreSQL puede garantizar un adecuado plan de recuperación de desastres?  &
  \textbf{Objetivo General} \newline
  Aplicar Devops en PostgreSQL para garantizar un adecuado plan de recuperación de desastres. &
  \textbf{Hipótesis General} \newline
  La aplicación de Devops en PostgreSQL garantizará un adecuado plan de recuperación de desastres. &
  \textbf{Variable Independiente} \newline
  Devops aplicado a PostgreSQL \newline\newline
  \textbf{Variable Dependiente} \newline
  Recuperación de desastres &
  \begin{itemize}[leftmargin=*]
    \item Punto de recuperación objetivo(RPO)
    \item Tiempo de recuperación objetivo(RTO)
    \item Tiempo de recuperación del trabajo(WRT)
    \item Máximo tiempo de inactividad tolerable(MTD)
  \end{itemize} \\
  \hline
  \textbf{Problema Específico 1} \newline
  ¿Ante un desastre, se puede garantizar una pérdida mínima de datos usando Devops? &
  \textbf{Objetivo Específico 1} \newline
  Garantizar una pérdida de datos mínima usando Devops &
  \textbf{Hipótesis Específica 1} \newline
  Devops permite garantizar la pérdida de datos mínima ante un desastre. &
  &
  \begin{itemize}[leftmargin=*]
    \item Punto de recuperación objetivo(RPO)
  \end{itemize} \\
  \hline
  \textbf{Problema Específico 2} \newline
  ¿Ante un desastre, se puede garantizar una recuperación pronta de la base de datos usando Devops? &
  \textbf{Objetivo Específico 2} \newline
  Garantizar una recuperación pronta de la base de datos usando Devops.  &
  \textbf{Hipótesis Específica 2} \newline
  Devops permite garantizar la recuperación pronta de la base de datos ante un desastre. &
  &
  \begin{itemize}[leftmargin=*]
    \item Tiempo de recuperación objetivo(RTO)
  \end{itemize} \\
  \hline
  \end{tabular}
  \caption{Matriz de consistencia}
\end{table}

## Anexo 04: Matriz de operacionalización

\begin{table}[H]
  \tiny
  \begin{tabular}{ | m{0.1\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} | m{0.1\textwidth} | m{0.2\textwidth} | m{0.1\textwidth} |}
  \hline
  \textbf{VARIABLE} &
  \textbf{DEFINICION CONCEPTUAL} &
  \textbf{DEFINICION OPERACIONAL} &
  \textbf{DIMENSIONES} &
  \textbf{INDICADORES} &
  \textbf{METODOLOGÍA} \\
  \hline
  Recuperación de desastres &
  Es un proceso documentado o conjunto de procedimientos o acciones para recuperar y
  proteger la infraestructura de TI de una organización en caso de un desastre. Refiriéndose
  con desastre a todo evento súbito, imprevisto catastrófico que interrumpe los procesos de
  negocio lo suficiente como para poner en peligro la viabilidad de la organización. \newline\newline
  \textbf{Fuente: Yarlequé(2019)}&
  El resultado de este procedimiento de recuperación se puede determinar a partir
  de indicadores como el RTO, que informa en cuánto tiempo se puede recuperar la base de datos,
  así como el RPO, que indica hasta dónde se puede recuperar el sistema. &
  Pérdida de datos &
  Punto de recuperación objetivo(RPO) &
  \textbf{Tipo de investigación} \newline
  Aplicada \newline\newline
  \textbf{Diseño de investigación} \newline
  Experimental \\
  \cline{4-5}
  &
  &
  &
  Tiempo de inactividad &
  Tiempo de recuperación objetivo(RTO) &
  \\
  \hline
  \end{tabular}
  \caption{Matriz de operacionalización}
\end{table}

\newpage

# Bibliografía
