---
papersize:
- a2
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{0pt}
- \usepackage{titling}
- \usepackage{array}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{float}
- \usepackage{listings}
- \usepackage{tikz}
- \usepackage{rotating}
- \usepackage{enumitem}
- \usepackage{multirow}
---

\title{Devops aplicado a PostgreSQL para la recuperación de desastres en la empresa Simply Connected Systems\vspace{2.0cm}}
\author{\textbf{Manuel Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Orientador: Prof. Menéndez Mueras Rosa}

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Agosto 2020}
\end{center}

\thispagestyle{empty}

\newpage

# 1. Matriz de consistencia

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} |}
  \hline
  \textbf{PROBLEMAS} &
  \textbf{OBJETIVOS} &
  \textbf{HIPÓTESIS} &
  \textbf{VARIABLES} &
  \textbf{INDICADORES} \\
  \hline
  \textbf{Problema General} \newline
  ¿La aplicación de Devops en PostgreSQL puede garantizar un adecuado plan de recuperación de desastres?  &
  \textbf{Objetivo General} \newline
  Aplicar Devops en PostgreSQL para garantizar un adecuado plan de recuperación de desastres. &
  \textbf{Hipótesis General} \newline
  La aplicación de Devops en PostgreSQL garantizará un adecuado plan de recuperación de desastres. &
  \textbf{Variable Independiente} \newline
  Devops aplicado a PostgreSQL \newline\newline
  \textbf{Variable Dependiente} \newline
  Recuperación de desastres &
  \begin{itemize}[leftmargin=*]
    \item Punto de recuperación objetivo(RPO)
    \item Tiempo de recuperación objetivo(RTO)
    \item Tiempo de recuperación del trabajo(WRT)
    \item Máximo tiempo de inactividad tolerable(MTD)
  \end{itemize} \\
  \hline
  \textbf{Problema Específico 1} \newline
  ¿Ante un desastre, se puede garantizar una pérdida mínima de datos usando Devops? &
  \textbf{Objetivo Específico 1} \newline
  Garantizar una pérdida de datos mínima usando Devops &
  \textbf{Hipótesis Específica 1} \newline
  Devops permite garantizar la pérdida de datos mínima ante un desastre. &
  &
  \begin{itemize}[leftmargin=*]
    \item Punto de recuperación objetivo(RPO)
  \end{itemize} \\
  \hline
  \textbf{Problema Específico 2} \newline
  ¿Ante un desastre, se puede garantizar una recuperación pronta de la base de datos usando Devops? &
  \textbf{Objetivo Específico 2} \newline
  Garantizar una recuperación pronta de la base de datos usando Devops.  &
  \textbf{Hipótesis Específica 2} \newline
  Devops permite garantizar la recuperación pronta de la base de datos ante un desastre. &
  &
  \begin{itemize}[leftmargin=*]
    \item Tiempo de recuperación objetivo(RTO)
  \end{itemize} \\
  \hline
  \end{tabular}
  \caption{Matriz de consistencia}
\end{table}

# 2. Matriz de operacionalización

\begin{table}[H]
  \begin{tabular}{ | m{0.1\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} | m{0.1\textwidth} | m{0.2\textwidth} | m{0.2\textwidth} |}
  \hline
  \textbf{VARIABLE} &
  \textbf{DEFINICION CONCEPTUAL} &
  \textbf{DEFINICION OPERACIONAL} &
  \textbf{DIMENSIONES} &
  \textbf{INDICADORES} &
  \textbf{METODOLOGÍA} \\
  \hline
  Recuperación de desastres &
  Es un proceso documentado o conjunto de procedimientos o acciones para recuperar y
  proteger la infraestructura de TI de una organización en caso de un desastre. Refiriéndose
  con desastre a todo evento súbito, imprevisto catastrófico que interrumpe los procesos de
  negocio lo suficiente como para poner en peligro la viabilidad de la organización. \newline\newline
  \textbf{Fuente: Yarlequé(2019)}&
  El resultado de este procedimiento de recuperación se puede determinar a partir
  de indicadores como el RTO, que informa en cuánto tiempo se puede recuperar la base de datos,
  así como el RPO, que indica hasta dónde se puede recuperar el sistema. &
  Pérdida de datos &
  Punto de recuperación objetivo(RPO) &
  \textbf{Tipo de investigación} \newline
  Aplicada \newline\newline
  \textbf{Diseño de investigación} \newline
  Experimental \\
  \cline{4-5}
  &
  &
  &
  Tiempo de inactividad &
  Tiempo de recuperación objetivo(RTO) &
  \\
  \hline
  \end{tabular}
  \caption{Matriz de operacionalización}
\end{table}

\newpage
