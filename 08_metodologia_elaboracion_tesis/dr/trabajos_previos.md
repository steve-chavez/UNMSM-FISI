---
header-includes:
- \setlength\parindent{0pt}
- \usepackage{titling}
- \usepackage{array}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{float}
- \usepackage{listings}
- \usepackage{tikz}
---

\title{Devops aplicado a PostgreSQL para la recuperación de desastres en la empresa Simply Connected Systems\vspace{2.0cm}}
\author{\textbf{Manuel Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Orientador: Prof. Menéndez Mueras Rosa}

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Agosto 2020}
\end{center}

\thispagestyle{empty}

\newpage

# Trabajos previos

Se muestran a continuación, trabajos previos relacionados a la **recuperación de desastres**, 3 nacionales y 3 internacionales, respectivamente.

## Implementación de una guía referencial para gestionar los riesgos informáticos en la universidad autónoma del perú

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.8\textwidth} |}
  \hline
  Año &
  2017 \\
  \hline
  Institución &
  Universidad Peruana de Ciencias Aplicadas - Facultad de Ingeniería \\
  \hline
  Grado académico &
  Ingeniero de sistemas \\
  \hline
  Autores &
  Michael André Aguirre Abanto, Giomar Anthony Lopez Ynostroza\\
  \hline
  URL &
  \href{http://repositorio.autonoma.edu.pe/handle/AUTONOMA/477}{repositorio.autonoma.edu.pe/handle/AUTONOMA/477} \\
  \hline
  \end{tabular}
\end{table}

Esta tesis tiene como enfoque la continuidad de negocios de los sistemas informáticos de la universidad autónoma del perú.
Para hacer esto se habla de la recuperación ante desastres. En el cual los backups son de vital importancia, se tienen en cuenta los backups del software base,
software aplicativo, bases de datos. Se mencionan posibles riesgos de desastre para el caso en particular, los cuales son: incendios, terremotos, inundaciones, sabotajes, robos y fallas en el hardware o software.

Así también se mencionan los indicadores para el resultado del procedimiento de recuperación. Citando textualmente:

"El resultado de este procedimiento de recuperación se puede determinar a partir de
indicadores como el RTO (Recovery Time Objective), que informa de en cuánto
tiempo se puede recuperar el sistema informático de la organización, así como el
RPO (Recovery Point Objective), que indica hasta dónde se puede recuperar el
sistema."

\newpage

## Modelo para la gestión de la continuidad del servicio de tecnologías de la información para empresas de tipo burocracia profesional basada en la norma técnica internacional ISO 22301

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.8\textwidth} |}
  \hline
  Año &
  2017 \\
  \hline
  Institución &
  Universidad Nacional de San Agustín - Facultad de Ingeniería de Producción y Servicios \\
  \hline
  Grado académico &
  Maestría en Ingeniería de Sistemas \\
  \hline
  Autor &
  Oscar Alberto Ramírez Valdez\\
  \hline
  URL &
  \href{http://repositorio.unsa.edu.pe/handle/UNSA/5831}{repositorio.unsa.edu.pe/handle/UNSA/5831} \\
  \hline
  \end{tabular}
\end{table}

En esta tesis se menciona la importancia de la resiliencia(capacidad de un sistema de soportar y recuperarse ante desastres)
en los sistemas de información. Se plantea un modelo de sistema de gestión de continuidad de servicio(SGC).

Se recalca la importancia de la recuperación de desastres y continuidad del negocio:

"Ninguna organización está libre de padecer algún problema que interrumpa la
normalidad de sus operaciones, los desastres suceden y las organizaciones no pueden
correr el riesgo de perder su información o lo que es más importante, la información
crítica de sus clientes. Los desastres pueden tener un impacto financiero importante
en las organizaciones y las interrupciones técnicas hacen que se pierdan clientes y el
tiempo de inactividad puede provocar el cierre de las organizaciones."

Así también se mencionan a los indicadores usuales RPO y RTO y añade dos indicadores más que también son pertinentes.

RTO: Recovery Time Objective, está asociado con la recuperación de recursos, tales
como sistemas de computación, equipos de manufactura e infraestructura física.

RPO:  Recovery Point Objective, este tiempo está relacionado, con la tolerancia que la
empresa puede tener, sobre la pérdida de datos, medidos en términos del tiempo entre el
último respaldo (backup) de datos y el evento del desastre.

MTD: Maximun Tolerable Downtime, consiste en el espacio de tiempo durante el cual
un proceso puede estar inoperante hasta que la empresa empiece a tener pérdidas y
colapse.

WRT: se calcula como el tiempo entre la recuperación del sistema y la normalización del procsamiento

\newpage

## Diseño de un Plan de Recuperación de Desastres de TI (DRP TI) para el Centro de Cómputo de la sede principal de una entidad educativa superior del sector privado basado en la norma NIST SP 800-34

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.8\textwidth} |}
  \hline
  Año &
  2019 \\
  \hline
  Institución &
  Universidad Peruana de Ciencias Aplicadas - Facultad de Ingeniería \\
  \hline
  Grado académico &
  Ingeniero de redes y comunicaciones \\
  \hline
  Autor &
  Yarlequé Gutiérrez, Alfredo \\
  \hline
  URL &
  \href{https://repositorioacademico.upc.edu.pe/handle/10757/625709}{repositorioacademico.upc.edu.pe/handle/10757/625709} \\
  \hline
  \end{tabular}
\end{table}

En este trabajo se crea un documento de apoyo dentro de una entidad educativa
superior del sector privado para la elaboración de un Plan de Recuperación de Desastres de
TI. Con el fin de salvaguardar el bien más preciado de la organización que es la información.

También se menciona a la Gestión de la Continuidad del Negocio como un proceso holístico que identifica amenazas
potenciales a la organización e impactos a las operaciones del negocio.
Esto proporciona la estructura para construir resiliencia con capacidad para dar respuesta efectiva protegiendo los intereses
de las partes interesadas, el valor de la marca, la reputación y las actividades  creadoras de valor.

Así también se catalogan los tipos de amenazas en:

- Naturales: fenómenos naturales.
- Humanas: intervenciones humanas.
- Humanas no intencionadas: sin premeditación.
- Humanas intencionadas: provocadas por por personal interno(por descontento o varias razones) o externo.

\newpage

## Effectiveness of Backup and Disaster Recovery in Cloud: A Comparative study on Tape and Cloud based Backup and Disaster Recovery

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.8\textwidth} |}
  \hline
  Año &
  2015 \\
  \hline
  Traducción &
  La efectividad del Backup y la Recuperación de desastres en la Nube\\
  \hline
  Institución &
  Blekinge Institute of Technology, Faculty of Computing, Department of Communication Systems - Suecia \\
  \hline
  Grado académico &
  Master of Science in Electrical Engineering with emphasis on Telecommunication Systems \\
  \hline
  Autor &
  Yarrapothu, Sindhura\\
  \hline
  URL &
  \href{http://bth.diva-portal.org/smash/record.jsf?pid=diva2\%3A861846}{bth.diva-portal.org/smash/record.jsf?pid=diva2\%3A861846} \\
  \hline
  \end{tabular}
\end{table}

En este trabajo se discute la efectivdad del backup en un plan de reucperación de desastres. Se analiza la efectividad
de un backup en cinta y de un backup en un storage de la nube.

Se mide las ya conocidas metricas de RPO y RTO respecto a backups en cinta y nube. Se concluye que el backup en nube brinda
mejores resultados que el de cinta.

\newpage

## Challenges and Opportunities of Having an IT Disaster Recovery Plan

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.8\textwidth} |}
  \hline
  Año &
  2017 \\
  \hline
  Traducción &
  Los retos y oportunidades de tener un Plan de Recuperación de desastres de TI\\
  \hline
  Institución &
  Umeå University, Faculty of Social Sciences, Department of Informatics. - Suecia \\
  \hline
  Grado académico &
  Master in IT Management \\
  \hline
  Autor &
  Ghannam, Mohamed Ziyad\\
  \hline
  URL &
  \href{http://umu.diva-portal.org/smash/record.jsf?pid=diva2\%3A1117263}{umu.diva-portal.org/smash/record.jsf?pid=diva2\%3A1117263} \\
  \hline
  \end{tabular}
\end{table}

En este trabajo, se hace una investigación cualitativa mediante una serie de entrevistas en diversas organizaciones, para determinar los retos y oportunidades de tener un plan de recuperación de desastres.

Se concluye que los principales retos son tener apoyo de la alta gerencia y disponibilidad de personal, y como principales beneficios se tiene la protección de los datos y la reducción de la interrupción de las funciones del negocio.

Así también los entrevistados mencionan los principales desastres ocurridos en sus organizaciones:

- Corte de energía: 70%
- Fuego: 69%
- Gestión de la configuración: 64%
- Cyber ataques: 63%
- Empleados maliciosos: 63%
- Pérdida de datos: 63%

\newpage

## Database System Architecture for Fault tolerance and Disaster Recovery

\begin{table}[H]
  \begin{tabular}{ | m{0.2\textwidth} | m{0.8\textwidth} |}
  \hline
  Año &
  2009 \\
  \hline
  Traducción &
  Architectura de Sistemas de Base de datos para Tolerancia a Fallos y Recuperación de Desastres\\
  \hline
  Institución &
  Regis University, School of Computer \& Information Science - Estados Unidos \\
  \hline
  Grado académico &
  Master Information Technology Management \\
  \hline
  Autor &
  Anthony Nguyen\\
  \hline
  URL &
  \href{https://epublications.regis.edu/theses/56}{epublications.regis.edu/theses/56/} \\
  \hline
  \end{tabular}
\end{table}

En este trabajo se plantea una arquitectura adecuada para la tolerancia a fallos y la recuperación de desastres. Usa la base de datos Oracle.

Se mencionan las siguientes estrategias de recuperación de desastres:

- Cluster con failover(Failover Clustering)
- Espejo de la base de datos(Database mirroring)
- Replicación par a par(Peer-to-peer transactional replication)
- Mantenimiento de una base de datos de reserva en caliente(Maintenance of a warm standby database)
- Backup y restauración(Backup and restore)
- Redundancia de datos usando arreglos de discos(RAID)
