---
title:
- Devops aplicado a PostgreSQL para la recuperación de desastres en la empresa Simply Connected Systems
author:
- Steve Chávez Astucuri
institute:
- UNMSM - FISI
theme:
- metropolis

header-includes:
- \usepackage{caption}
- \usepackage{tikz}
- \usetikzlibrary{positioning}
- \usepackage[export]{adjustbox}
- \usepackage{array}
---

# Agenda

- Realidad problemática
- Bases teóricas
- Prueba de Concepto

#

\begin{center}
\Huge Realidad Problemática
\end{center}

# Introducción

- En la ya madura era de la información, es conocido que para una empresa u organización, los datos son el activo más valioso.
Para garantizar la perennidad de estos datos, es estándar que la organización posea un plan de **recuperación de desastres**.

- Sin embargo, realizar este plan requiere del trabajo de un experto, el cual hace un trabajo ad hoc para cada base de datos; esto incurre en costo operacional y pérdida de agilidad en la organización.
O lo que es peor aún, no se realiza ningún plan, y la empresa queda a merced de riesgos tan grandes como el del **ransomware**.

# Ransomware

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ransomware-initial.jpg}
\end{figure}

# Ransomware en Mysql

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ransomware-mysql-2.png}
\end{figure}

# Ransomware en MongoDB

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ransomware-mongo.png}
\end{figure}

# Realidad problemática

- A nivel internacional, organizaciones maduras tienen infraestructura dedicada para la recuperación de desastres. Startups o empresas
pequeñas prefieren confiar en la nube y sus servicios de DbaaS, los cuales se ocupan de la recuperación de desastres de forma automática.

- A nivel nacional, se puede observar que existen estándares planes de recuperación de desastres, pero estos están a un nivel documental.
No tienen un enfoque DevOps que permita poner en práctica el plan fácilmente.

- A nivel organizacional, SCS, no cuenta con un experto en bases de datos y tienen clientes con infraestructura
propia que desean alojar y manejar sus propias bases de datos. Ellos requieren de DevOps para garantizar y agilizar los
planes de recuperación de desastres de sus clientes.

# Planteamiento del Problema

## Problema General
\bigskip
¿La aplicación de Devops en PostgreSQL puede garantizar un adecuado plan de recuperación de desastres?

## Objetivo General
\bigskip
Aplicar Devops en PostgreSQL para garantizar un adecuado plan de recuperación de desastres.

#

\begin{center}
\Huge Bases teóricas
\end{center}

# Trabajos Previos - Nacionales

- Implementación de una guía referencial para gestionar los riesgos informáticos en la universidad autónoma del perú(2019)

- Modelo para la gestión de la continuidad del servicio de tecnologías de la información para empresas de tipo burocracia profesional basada en la norma técnica internacional ISO 22301(2017)

- Diseño de un Plan de Recuperación de Desastres de TI (DRP TI) para el Centro de Cómputo de la sede principal de una entidad educativa superior del sector privado basado en la norma NIST SP 800-34(2017)

# Trabajos Previos - Internacionales

- Effectiveness of Backup and Disaster Recovery in Cloud: A Comparative study on Tape and Cloud based Backup and Disaster Recovery(2015)

- Challenges and Opportunities of Having an IT Disaster Recovery Plan(2017)

- Database System Architecture for Fault tolerance and Disaster Recovery(2009)

# Variable Independiente: DevOps

Según @artac2017devops, **DevOps** comprende una serie de tácticas de ingeniería de software que apuntan a
reducir el tiempo accionable de cambio de diseño de software. Una de estas tácticas es aprovechar la **infraestructura como código**,
esto es, escribir un "blueprint" que contiene especificaciones de despiegue, listo para la orquestación en la nube.

Según @sushmaautomation, **DevOps** es un método de desarrollo de software que permite la colaboración de equipos de desarrollo de software y operaciones
para mejorar la calidad del software entregado. Ayuda a los equipos con la integración continua, el planeamiento continuo,
el despliegue continuo y el feedback continuo.

# Infraestructura como Código(IaC)

Según @artac2018infrastructure, como parte de las tácticas de DevOps, la infraestructura como código(IaC), provee
la habilidad de crear, configurar y manejar infraestructuras complejas mediante código ejecutable.

**El aprovisionamiento** implica adquirir,
configurar y ejecutar las máquinas virtuales o los contenedores necesarios en los que se pueden ejecutar los servicios.

**La gestión de la configuración** es el proceso de desplegar y manejar todos los servicios requeridos(por ejemplo Tomcat, MySQL, Hadoop, Cassandra).
Consume recetas reusables que describen como manejar y configurar el servicio a través de su ciclo de vida.

**El despliegue** es la fase en la que se ejecutan las aplicaciones del usuario en la infraestructura resultante.

# Infraestructura como Código(IaC)

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/provisioning-versus-configuration.png}
\end{figure}

# Variable Dependiente: Recuperación de Desastres

Según @yarleque2019drp, es un proceso documentado o conjunto de procedimientos o acciones para recuperar y
proteger la infraestructura de TI de una organización en caso de un desastre. Refiriéndose
con desastre a todo evento súbito, imprevisto catastrófico que interrumpe los procesos de
negocio lo suficiente como para poner en peligro la viabilidad de la organización. Un
desastre podría ser el resultado de un daño importante a una parte de las operaciones, la
pérdida total de una instalación, o la incapacidad de los empleados para acceder a esa
instalación.

Según @ramirez2017bc, es un documento que contiene un conjunto de acciones y procedimientos definidos previamente, con
responsabilidades claramente establecidas, para la recuperación del componente tecnológico, sistemas y servicios de telecomunicaciones.

# Indicadores

El resultado del procedimiento de recuperación de desastres se puede determinar a partir de
indicadores como el **RTO**(Recovery Time Objective), que informa de en cuánto
tiempo se puede recuperar el sistema informático de la organización, así como el
**RPO** (Recovery Point Objective), que indica hasta dónde se puede recuperar el
sistema.

# Indicadores

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/rporto.png}
\end{figure}

# Punto objetivo de recuperación(RPO)

Es la cantidad máxima de datos que se pueden perder cuando se restablece el servicio después de una interrupción.

\begin{equation*}
\boldsymbol{RPO = FechaHoraDesastre - FechaUltimoBackup}
\end{equation*}

Cero es un valor válido y es equivalente a un requisito de "pérdida de datos cero".

# Tiempo objetivo de recuperación(RTO)

Es el tiempo máximo permitido para la recuperación de un servicio después de una interrupción.

\begin{equation*}
\boldsymbol{RTO = FechaHoraRecuperacion - FechaHoraDesastre}
\end{equation*}

#

\begin{center}
\Huge Prueba de Concepto
\end{center}

# Estado inicial

Al desplegar una nueva instancia de PostgreSQL, sin configuración alguna para recuperación de desastres(lo cual es común,
incluso en sistemas que llevan años en producción), se tienen las siguientes medidas:

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  $\infty$ &
  $\infty$ \\ \hline
  \end{tabular}
  \caption{RPO/RTO inicial}
\end{table}

# Objetivo

Se desea llegar a los siguientes resultados:

\begin{table}[H]
  \small
  \center
  \begin{tabular}{l l} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  ~ 0 &
  < 1 min \\ \hline
  \end{tabular}
  \caption{RPO/RTO deseado}
\end{table}

Lo que significa que existe pérdida de datos 0 y un tiempo de recuperación menor a 1 minuto. Lo que es ideal
para una base de datos de pequeño tamaño que se usará para la prueba de concepto.

# Herramientas

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/iac-tools.png}
  \caption{Herramientas de gestión de sistemas}%
\end{figure}

# Diagrama ER de base de datos de prueba

\begin{figure}%
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-er.png}
  \caption{Diagrama ER de World}%
\end{figure}

# AWS EC2

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ec2-logo.jpeg}
  \caption{AWS EC2}%
\end{figure}

# AWS S3

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{images/s3.png}
  \caption{AWS S3}%
\end{figure}

# Aprovisionamiento - 1

```sh
let
  region = "us-east-2";
  accessKeyId = "dev";

  ec2 = { resources, ... }:
    {
      deployment.targetEnv = "ec2";
      deployment.ec2 = {
        accessKeyId = accessKeyId;
        region = region;
        instanceType = "t2.micro";
        associatePublicIpAddress = true;
        keyPair = resources.ec2KeyPairs.pg-key-pair;
      };
    };
```

# Aprovisionamiento - 2

```sh
in
{
  resources = {
    ec2KeyPairs.pg-key-pair = {
      inherit region accessKeyId;
    };
    s3Buckets.poc-362 = {
      inherit region accessKeyId;
      versioning = "Suspended";
    };
  };
  pg = ec2;
  ## pg-recovery = ec2;
}
```

# Configuración - Instacia primaria de PostgreSQL - 1

```nix
{
  network.description = "Servidores Postgres";

  pg = { config, pkgs, resources, ... }: {
    environment.systemPackages = [
      pkgs.awscli
    ];
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      authentication = pkgs.lib.mkForce ''
        local all all trust
        local replication postgres trust
      '';
      extraConfig = ''
```

# Configuración - Instacia primaria de PostgreSQL - 2

```nix
        wal_level = archive
        archive_mode = on
        max_wal_senders = 10
        archive_command = '${pkgs.awscli}/bin/aws s3 cp %p s3://${resources.s3Buckets.poc-362.name} 1>&2'
      '';
    };

    systemd.services.postgresql = {
      environment = {
        AWS_ACCESS_KEY_ID = builtins.readFile ./key_id.secret;
        AWS_SECRET_ACCESS_KEY = builtins.readFile ./access_key.secret;
      };
    };
  };
```

# Configuración - Instacia primaria de PostgreSQL - 3

```nix
  pg-recovery = { config, pkgs, resources, ... }: {
    environment.systemPackages = [
      pkgs.awscli
    ];
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      recoveryConfig = ''
        restore_command = '${pkgs.awscli}/bin/aws s3 cp s3://${resources.s3Buckets.poc-362.name}/%f %p'
        # Especificar el tiempo de restauracion deseado
        # recovery_target_time='2018-12-22 00:04:00'
      '';
    };

```

# Configuración - Instacia primaria de PostgreSQL - 4

```
    systemd.services.postgresql = {
      environment = {
        AWS_ACCESS_KEY_ID = builtins.readFile ./key_id.secret;
        AWS_SECRET_ACCESS_KEY = builtins.readFile ./access_key.secret;
      };
    };
  };
}
```

# Despliegue - 1

\begin{center}
\Huge{nixops deploy -d poc}
\end{center}

# Despliegue - 2

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ec2-dashboard-1.png}
  \caption{Instancia primaria de PostgreSQL en EC2}%
\end{figure}

# Despliegue - 3

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/s3-dashboard-1.png}
  \caption{Bucket S3 para almacenamiento de backups incrementales}%
\end{figure}

# Exploración de la base de datos

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-1.png}
  \caption{Data inicial de la bd World}%
\end{figure}

# Simulación de desastre

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-2.png}
  \caption{DELETE sin WHERE}%
\end{figure}

# Recuperación de desastres - 1

\begin{center}
\Huge{time nixops deploy -d poc}
\Huge{19.46s}
\end{center}

# Recuperación de desastres - 2

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ec2-dashboard-2.png}
  \caption{AWS EC2 con instancia de recuperacion}%
\end{figure}

# Recuperación de desastres - 3

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-3.png}
  \caption{Base de datos recuperada}%
\end{figure}

# Resultado de la Prueba de Concepto

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  0 &
  19.46s < 1 min \\ \hline
  \end{tabular}
  \caption{RPO/RTO final}
\end{table}

# Mejoras Futuras

- Pruebas en distintos tamaños de bases de datos
  - Menor a 1GB
  - Mayor a 1GB
- Pruebas en distintas instancias virtuales en la nube.
- Pruebas en un hardware puro(bare-metal), sin virtualización.

#

\begin{center}
\Huge{Gracias}
\end{center}

# Bibliografía
