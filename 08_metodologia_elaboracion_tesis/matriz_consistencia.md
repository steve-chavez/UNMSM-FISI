---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{0pt}
- \usepackage{titling}
- \usepackage{array}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{float}
- \usepackage{listings}
- \usepackage{tikz}
---


\title{Devops aplicado a PostgreSQL para obtener los SLAs de un Database as a Service(DbaaS) en la empresa Simply Connected Systems\vspace{2.0cm}}
\author{\textbf{Manuel Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Orientador: Prof. Menéndez Mueras Rosa}

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Julio 2020}
\end{center}

\thispagestyle{empty}

\newpage

# 1. Matriz consistencia

\begin{table}[H]
  \begin{tabular}{ | m{0.25\textwidth} | m{0.25\textwidth} | m{0.25\textwidth} | m{0.25\textwidth} |}
  \hline
  \textbf{PROBLEMA} &
  \textbf{OBJETIVOS} &
  \textbf{HIPÓTESIS} &
  \textbf{VARIABLES E INDICADORES} \\
  \hline
  \textbf{General} &
  \textbf{General} &
  \textbf{General} &
  \textbf{Variable Independiente} \\
  \hline
  La aplicación de Devops en PostgreSQL puede evitar la dependencia en un proveedor en la nube y el riesgo de vendor lock-in?  &
  Aplicar la infraestructura como código usando NixOS para ofrecer las mismas garantías que ofrece un proveedor de la nube &
  Si se ofrece una especificación usando la infraestructura como código se puede prescindir de los proveedores de la nube para gestionar PostgreSQL. &
  Devops aplicado a PostgreSQL \\
  \hline
  \textbf{Específico} &
  \textbf{Específico} &
  \textbf{Específico} &
  \textbf{Variable Dependiente} \\
  \hline
  Podemos ofrecer las mismas garantías en recuperación de desastres de un DbaaS con NixOS? &
  Aplicar mejores prácticas de recuperación de desastres usando NixOS para asegurar la misma calidad de un DbaaS.  &
  H1.  Usando la infraestructura como código, se puede ofrecer características de recuperación de desastres similares a las de un DbaaS.  &
  Mitigación de riesgos en la nube \newline\newline
  \textbf{Indicadores:} \newline
  \begin{itemize}
    \item Punto de recuperación objetivo(RPO)
    \item Tiempo de recuperación objetivo(RTO)
  \end{itemize} \\
  \hline
  \end{tabular}
  \caption{Matriz de consistencia}
\end{table}

\newpage

# 2. Indicadores

### 2.1 Recuperación de desastres(Disaster Recovery)

La principal oferta de un DbaaS es la recuperación de desastres, es decir, la restauración de la base de datos al
estado más reciente y consistente. Para esto tenemos dos indicadores.

### 2.2 Tiempo objetivo de recuperación(RTO)

Es el tiempo máximo permitido para la recuperación de un servicio después de una interrupción.

\begin{equation*}
\boldsymbol{RTO = FechaHoraDesastre - FechaHoraRecuperacion}
\end{equation*}

Un RTO ideal para un servicio de cloud computing viene a ser menor a 1 minuto.

### 2.3 Punto objetivo de recuperación(RPO)

Es la cantidad máxima de datos que se pueden perder cuando se restablece el servicio después de una interrupción.
El punto de recuperación objetivo se expresa en un período de tiempo antes de la falla.
Por ejemplo, un punto de recuperación objetivo de un día puede ser apoyado por los backups diarios, y donde se pueden perder datos
de hasta 24 horas.

\begin{equation*}
\boldsymbol{RPO = DatosPerdidos(FechaHoraDesastre - FechaUltimoBackup)}
\end{equation*}

Cero es un valor válido y es equivalente a un requisito de "pérdida de datos cero".

### 2.4 Estado inicial

Al desplegar una nueva instancia de PostgreSQL, sin configuración alguna para recuperación de desastres(lo cual es bastante
común, incluso en sistemas que llevan años en producción), se tienen las siguientes medidas:

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  $\infty$ &
  $\infty$ \\ \hline
  \end{tabular}
  \caption{RPO/RTO inicial}
\end{table}

Basícamente la pérdida de datos es infinita y el tiempo de recuperación también es infinito.
Nuestro objetivo es mejorar estas medidas a resultados más aceptables para la recuperación de desastres.

### 2.4 Objetivo

Se desea llegar a los siguientes resultados:

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  ~ 0 &
  < 1 min \\ \hline
  \end{tabular}
  \caption{RPO/RTO deseado}
\end{table}

Lo que básicamente significa que existe pérdida de datos 0 y un tiempo de recuperación menor a 1 minuto. Lo que es ideal
según las características ofrecidas por un DbaaS.
