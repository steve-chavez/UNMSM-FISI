---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \setlength\parindent{0pt}
- \usepackage{titling}
- \usepackage{array}
- \usepackage{graphicx}
- \usepackage{subfig}
- \usepackage{float}
- \usepackage{listings}
- \usepackage{tikz}
- \usepackage{lscape}
---


\title{Devops aplicado a PostgreSQL para evitar el riesgo de vendor lock-in del cloud computing en la empresa Simply Connected Systems\vspace{2.0cm}}
\author{\textbf{Manuel Chávez Astucuri}}

\renewcommand\maketitlehooka{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{images/escudo.png}
  \end{center}%
}

\maketitle

\begin{center}

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\textbf{Orientador: Prof. Menéndez Mueras Rosa}

\vfill

\textbf{UNMSM - Escuela de Ingeniería de Sistemas}

\textbf{Junio 2020}
\end{center}

\thispagestyle{empty}

\newpage

# 01: Entrevista para determinar la problemática operativa actual en la empresa Simply Connected Systems

\hfill \break

\begin{center}
  \begin{tabular}{ | l | l |} \hline
  \textbf{Nro Entrevista} & 1 \\ \hline
  \textbf{Entrevistado} & Steven Kramer \\ \hline
  \textbf{Cargo} & Chief Connection Officer \\ \hline
  \textbf{Fecha} & 2019/10/12 \\ \hline
  \end{tabular}
\end{center}

\hfill \break

## 1. A qué se dedica Simply Connected Systems?

Simply Connected Systems[^1] está basada en Coral Springs en el estado de Florida, Estados Unidos.

Nos dedicamos a integrar sistemas infórmaticos corporativos de la forma más simple. Ofrecemos soluciones informáticas móviles
no disruptivas, customizables, asequibles, sin papel, sin retipeo, que permiten a las empresas mantener sus sistemas informáticos
heredados, flujo de trabajo e información.

Nos enorgullecemos en tener como caso de éxito la integración informática de una compañía de construcción que ha operado por
4 generaciones[^2](casi 100 años). Nuestras soluciones están pensadas para ser de bajo mantenimiento y lo más duraderas posible.

Actualmente nos estamos enfocando en un nuevo mercado: empresas de servicios de restauración. Estas empresas reparan daños
ocasionados por desastres naturales en distintas instalaciones. Hacen esto con maquinaria que trabaja "on site", la cual
se encarga de la dehumidificación del aire, absorción de agua, etc. Esta maquinaria en ocasiones se extravia,
por lo cual estamos proveyendo un sistema de monitoreo, usando dispositivos IoT, que le permita a la empresa
identificar la posición y el estado de la maquinaria.

El desarrollo de estos sistemas es algo nuevo para nosotros y hacemos uso extenso de la nube con sus distintos proveedores.

## 2. El desarrollo de estos sistemas presenta nuevas dificultades?

Sí, existen diversas tareas que escapan a la expertiz del equipo de Simply Connected Systems. Por lo cual hacemos uso
de diversos contratistas de distintas nacionalidades. Usamos la plataforma Upwork[^3] para encontrarlos y contratarlos, esto nos
ha funcionado muy bien.

Así también, la nube ha reducido mucho nuestro costo operativo. En particular, para manejar nuestras bases de datos PostgreSQL
usamos AWS RDS[^4] y este nos ofrece muchas ventajas pero aquí viene nuestro principal problema.

Aunque nuestras soluciones basadas en la nube son suficientes para empresas pequeñas, empresas más grandes, con mayor madurez,
con politicas de gobernanza de datos y aversión al riesgo, desean alojar nuestras soluciones en infraestructura privada.

Sin embargo, AWS RDS no dispone de una versión "on premise". Para cumplir con los requisitos de estas empresas,
nuestros sistemas tendrían que replicar todas las ventajas que AWS RDS nos ofrece e instalarlos de una forma reproducible
para cada uno de nuestros clientes.

[^1]: https://www.simplyconnectedsystems.com
[^2]: https://www.youtube.com/watch?v=7b1yA2lE2qo
[^3]: https://upwork.com
[^4]: https://aws.amazon.com/rds/

## 3. Una versión "on-premise" de AWS RDS solucionaría el problema?

Sí. Sabemos que AWS RDS al manejar PostgreSQL ofrece funcionalidades como: backups incrementales, recuperación en un punto de tiempo,
alta disponibilidad, etc, en las cuales muchas empresas confían para garantizar la robustez de sus bases de datos.

Sin embargo, muchas empresas han manejado a PostgreSQL exitosamente en tiempos anteriores al boom del cloud computing.
Esto es debido al gran ecosistema open source que mantiene PostgreSQL. AWS RDS admite combinar distintos componentes open source
gratuitos que el ecosistema ofrece.

Creemos que en estos tiempos de DevOps, esta tarea, antes herculeana, es ahora factible.

Esto nos permitiría replicar un sistema robusto en cualquier infraestructura, tanto pública como privada.

\newpage

# 02: Diagrama de Ishikawa

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip

\usetikzlibrary{arrows,shapes.geometric,positioning,matrix}
\tikzset{
  ishikawa/.style={align=center, inner sep=0pt},
  matter/.style  ={rectangle, minimum size=6mm, very thick, draw=red!70!black!40,
    top color=white, bottom color=red!50!black!20, font=\itshape},
  level_1/.style ={ellipse, node distance=60pt, minimum size=6mm, very thick,
    draw=red!50!black!50, top color=white, bottom color=red!50!black!20, font=\itshape},
  level_2/.style={rectangle, minimum size=6mm, font=\itshape, font=\scriptsize}}
\tikzset{
  rows/.style 2 args={@/.style={row ##1/.style={#2}},@/.list={#1}},
  cols/.style 2 args={@/.style={column ##1/.style={#2}},@/.list={#1}},
}
\begin{tikzpicture}
\matrix[
  matrix of nodes,
  row sep=3cm,
  column sep=1cm,
  rows={1,3}{nodes=level_1},
  rows={2}{nodes=matter,anchor=center}
] (m) {
Atributos de calidad & Recursos humanos & \\
         &          & Portabilidad del sistema \\
Requerimientos  & Riesgos de la nube & \\
};
\path[very thick,
  toarr/.style={->, shorten <=+0pt, shorten >=+.1cm},
  fromarr/.style={<-, shorten >=+0pt, shorten <=+.1cm}]
  [toarr]
  (m-1-1|-m-2-3) edge (m-2-3) % mid(left to right) arrow

  (m-1-1) edge[xslant=-.5]
    coordinate[pos=.3]   (@-1-1-1)
    coordinate[near end] (@-1-1-2) (m-1-1|-m-2-3)
  [fromarr]
  (@-1-1-1) edge node[above,level_2]{Alta disponibilidad} ++ (left:2.9cm)
  (@-1-1-2) edge node[above,level_2]{Alta confiabilidad} ++ (left:2.7cm)

  [toarr]
  (m-1-2) edge[xslant=-.5]
    coordinate[pos=.3]   (@-1-2-1)
    coordinate[near end] (@-1-2-2) (m-1-2|-m-2-3)
  [fromarr]
  (@-1-2-1) edge node[above,level_2]{Carencia de DBA} ++ (left:2.5cm)
  (@-1-2-2) edge node[above,level_2]{Dependencia en contratistas} ++ (left:3.8cm)

  [toarr]
  (m-3-1) edge[xslant=.5]
    coordinate[pos=.3]   (@-3-1-1)
    coordinate[near end] (@-3-1-2) (m-3-1|-m-2-3)
  [fromarr]
  (@-3-1-1) edge node[above,level_2]{Infraestructura privada} ++ (left:2.8cm)

  [toarr]
  (m-3-2) edge[xslant=.5]
    coordinate[pos=.3]   (@-3-2-1)
    coordinate[near end] (@-3-2-2) (m-3-2|-m-2-3)
  [fromarr]
  (@-3-2-1) edge node[above,level_2]{Vendor lock-in} ++ (left:2cm)
  (@-3-2-2) edge node[above,level_2]{Dependencia en RDS} ++ (left:2.5cm)
;
\end{tikzpicture}

\newpage

## 03: Diagrama de funciones cruzadas

\usetikzlibrary{shapes}
\tikzset{
    recgray/.style={draw,minimum width=3cm,minimum height=2cm,align=center,text width=3cm,fill=gray!50,font=\sffamily},
    rndgray/.style={rounded corners=1cm,draw,minimum width=3cm,minimum height=2cm,align=center,text width=3cm,fill=gray!50,font=\sffamily},
    recgren/.style={draw,minimum width=3cm,minimum height=2cm,align=center,text width=3cm,fill=green!50,font=\sffamily},
    diagren/.style={diamond,draw,minimum width=3cm,minimum height=2cm,align=center,text width=3cm,fill=green!50,font=\sffamily,aspect=1.5},
}
\begin{landscape}
\begin{figure}
\begin{tikzpicture}[x=4.5cm,y=2cm, scale=0.8]
%---
\foreach \i in {-4,-2,0,2} {
    \draw (-.75,\i) rectangle (6.25,\i+2);
    \draw (-.75,\i) rectangle (-.5,\i+2);
}
  \node[rotate=90,font=\sffamily] at (-.625,1) {SCS(Empresa)};
\node[rotate=90,font=\sffamily] at (-.625,3) {Cliente};
\node[rotate=90,font=\sffamily] at (-.625,-1) {Contratista};
\node[rotate=90,font=\sffamily] at (-.625,-3) {AWS};
%---
\node[rndgray] (cus1) at (0,3) {Solución informática solicitada};
\node[recgray] (sale1) at (0,1) {Recaba requerimientos};
\node[recgray] (cre1) at (0,-1) {Implementa requerimientos};
\node[recgray] (ware1) at (0,-3) {Aprovisiona recursos};
\node[diagren] (cre2) at (2,-1) {Requiere infraestructura privada?};
\node[recgray] (cre4) at (4,-1) {Aplica Devops e infraestructura como código};
\node[recgray] (cre5) at (5,-1) {Despliega servicios en la infraestructura privada};
\node[recgray] (ware5) at (5.25,-3) {Aloja servicios en su infraestructura};
\node[rndgray,minimum width=4.5cm,text width=4.5cm] (x) at (5.5,3)
  {Solución entregada};
\begin{scope}[every path/.style={-latex}]
\draw (cus1) edge (sale1)
    (sale1) edge (cre1)
    (cre1) edge (cre2)
    (cre2) edge node[midway,fill=white,inner sep=2pt] {Sí} (cre4)
    (cre4) edge (cre5)
    (cre2) edge node[midway,fill=white,inner sep=2pt] {No} (ware1)
    (ware1) edge (ware5);
\draw (cre5.north east) -- ++(0,3);
\draw (ware5.north east) -- ++(0,5);
\end{scope}
\end{tikzpicture}
\end{figure}
\end{landscape}

## 04: Matriz de consistencia

\begin{table}[H]
  \begin{tabular}{ | m{0.25\textwidth} | m{0.25\textwidth} | m{0.25\textwidth} | m{0.25\textwidth} |}
  \hline
  \textbf{PROBLEMA} &
  \textbf{OBJETIVOS} &
  \textbf{HIPÓTESIS} &
  \textbf{VARIABLES E INDICADORES} \\
  \hline
  \textbf{General} &
  \textbf{General} &
  \textbf{General} &
  \textbf{Variable Independiente} \\
  \hline
  La aplicación de Devops en PostgreSQL puede evitar la dependencia en un proveedor en la nube y el riesgo de vendor lock-in?  &
  Aplicar la infraestructura como código usando NixOS para ofrecer las mismas garantías que ofrece un proveedor de la nube &
  Si se ofrece una especificación usando la infraestructura como código se puede prescindir de los proveedores de la nube para gestionar PostgreSQL. &
  Devops aplicado a PostgreSQL \\
  \hline
  \textbf{Específico} &
  \textbf{Específico} &
  \textbf{Específico} &
  \textbf{Variable Dependiente} \\
  \hline
  Podemos ofrecer las mismas garantías en recuperación de desastres de un DbaaS con NixOS? &
  Aplicar mejores prácticas de recuperación de desastres usando NixOS para asegurar la misma calidad de un DbaaS.  &
  H1.  Usando la infraestructura como código, se puede ofrecer características de recuperación de desastres similares a las de un DbaaS.  &
  Riesgo del vendor lock-in del cloud computing \newline\newline
  \textbf{Indicadores:} \newline
  \begin{itemize}
    \item Punto de recuperación objetivo(RPO)
    \item Tiempo de recuperación objetivo(RTO)
  \end{itemize} \\
  \hline
  \end{tabular}
  \caption{Matriz de consistencia}
\end{table}

\newpage
