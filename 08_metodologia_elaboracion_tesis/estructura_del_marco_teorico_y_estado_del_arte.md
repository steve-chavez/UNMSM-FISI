# 2. Estructura del marco teórico

## 2.1. Conceptos Previos

2.1.1. Cloud Computing

2.1.2. PaaS

2.1.3. SaaS

2.1.4. IaaS

2.1.5. Nuevos tipos de cloud computing: DbaaS, BaaS, DRaaS

2.1.6. Infraestructura como código

2.1.7. DevOps

## 2.2. Gestión de Riesgos

## 2.3. Atributos de calidad

2.3.1. Disponibilidad

2.3.2. Performance

2.3.2. Robustez

## 2.4. Recuperación de desastres

2.4.1. Tiempo de Recuperación Objetivo(RTO)

2.4.2. Punto de Recuperación Objetivo(RPO)

## 2.5. Riesgos del cloud computing.

2.5.1. Ontología de riesgos

2.5.2. Riesgos principales identificados

# 3. Estructura del estado del arte

## 3.1. Funcionalidades de AWS RDS

3.1.1. Aprovisionamiento de recursos automáticos

3.1.2. Seguridad

3.1.3. Backups automáticos

3.1.4. Alta disponibilidad

3.1.5. Escalabilidad con mínimo tiempo inactivo por mantenimiento

3.1.6. Tareas administrativas no automatizadas

## 3.2. Estrategias de replicación

3.2.1. Esquemas de replicación

3.2.2. Replicación síncrona

3.2.3. Replicación asíncrona

3.2.4. Replicación multi-maestro

3.2.5. Replicación maestro-esclavo

3.2.4. Recuperación de fallas(failover)

## 3.3. Modelos de particionamiento(sharding)

3.3.1. Sharding por geografía

3.3.2. Sharding por identificador de entidad

3.3.3. Sharding por tiempo

## 3.4. Modelos de gestión de sistemas

3.4.1. Modelo Divergente

3.4.2. Modelo Convergente

3.4.3. Modelo Congruente

## 3.5. Herramientas que aplican el Modelo Congruente

3.5.1. Docker Containers

3.5.2. NixOS

3.5.3. Comparación de las herramientas
