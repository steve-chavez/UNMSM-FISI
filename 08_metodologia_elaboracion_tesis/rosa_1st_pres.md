---
title:
- Administración autónoma de bases de datos en entornos de nube híbrida
author:
- Manuel Chávez Astucuri
institute:
- UNMSM - FISI
theme:
- metropolis

header-includes:
- \usepackage{caption}
- \usepackage{tikz}
- \usetikzlibrary{positioning}
- \usepackage[export]{adjustbox}
- \usepackage{array}
---

# Agenda

- 1. Antecedentes
- 2. Problema - Riesgos del cloud computing
- 3. Propuesta
- 4. Detalles de la propuesta

# Antecedentes

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/cloud-trend.png}
  \caption*{Spiceworks: tendencias de la nube pública al 2019}
\end{figure}

#

\begin{center}
\Huge{Riesgos del cloud computing}
\end{center}

# Riesgos del cloud computing

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/risks/digital_ocean.png}
  \caption*{Digital Ocean mató nuestro compañía}
\end{figure}

# Riesgos del cloud computing

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/risks/github_iran.png}
  \caption*{Github bloquea a usuarios iraníes}
\end{figure}

# Riesgos del cloud computing

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/risks/capital_one.png}
  \caption*{Empleada de AWS roba información de clientes de Capital One}
\end{figure}

# Riesgos del cloud computing

Según _Dutta, Amab and Peng, Guo Chao Alex and Choudhary, Alok_(2013)
en **Risks in enterprise cloud computing: the perspective of IT experts**.
Los 10 principales riesgos en cloud computing son los que se tienen a continuación.

# Riesgos del cloud computing - Parte 1

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  1 &
  LR1.1 &
  La privacidad de la empresa o data del cliente es puesta en riesgo en la nube &
  153.50 \\
  \hline
  2 &
  LR1.3 &
  Leyes inconsistentes de protección de datos en los distintos países donde la data se genera y se guarda. &
  151.75 \\
  \hline
  3 &
  OGR4.2 &
  Dificultad para cambiar de proveedor de la nube incluso en caso de insatisfacción con el servicio(vendor lock-in) &
  148.50 \\
  \hline
  4 &
  OGR5.2 &
  Las compañías usuarias carecen de recuperación de desastres y planes de contingencia para lidiar con fallas
  técnicas inesperadas en la nube &
  147.75 \\
  \hline
  \end{tabular}
\end{table}

# Riesgos del cloud computing - Parte 2

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  5 &
  LR3.2 &
  Dificultad de migración de la empresa al término del contrato con el proveedor de la nube &
  140.25 \\
  \hline
  6 &
  OPR4.2 &
  Inadecuado entrenamiento y/o conocimiento de los servicios de la nube. &
  139.75 \\
  \hline
  7 &
  OPR5.1 &
  Las aplicaciones en la nube temporalmente caen temporalmente en fuera de servicio. &
  137.25 \\
  \hline
  8 &
  OPR2.1 &
  Costos ocultos que se incrementan debido a modos de operacion no transparentes en la nube. &
  136.00 \\
  \hline
  \end{tabular}
\end{table}

# Riesgos del cloud computing - Parte 3

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  9 &
  TR4.3 &
  Ataques de denegación de servicio a un proveedor de la nube. &
  135.50 \\
  \hline
  10 &
  TR4.1 &
  Acceso no authorizado a data empresarial en la nube. &
  135.00 \\
  \hline
  \end{tabular}
\end{table}

# Propuesta - nube híbrida

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/multi-cloud.png}
\end{figure}

# Propuesta - nube híbrida

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/kubernetes.png}
\end{figure}

# Propuesta - Infraestructura como codigo

Según _Hermanns, Steffens_(2013) en **The current state of Infrastructure as Code and how it changes the software development
process**. Las principales herramientas para el IaC son:

# Propuesta - Herramientas

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/iac-tools.png}
  \caption{Herramientas de gestión de sistemas}%
\end{figure}

#

\begin{center}
\Huge{Gracias}
\end{center}
