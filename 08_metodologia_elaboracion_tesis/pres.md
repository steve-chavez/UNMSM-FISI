---
title:
- Administración autónoma de bases de datos para mitigación de riesgos de aprisionamiento tecnológico en la nube
author:
- Manuel Chávez Astucuri
institute:
- UNMSM - FISI
theme:
- metropolis

header-includes:
- \usepackage{caption}
- \usepackage{tikz}
- \usetikzlibrary{positioning}
- \usepackage[export]{adjustbox}
- \usepackage{array}
---

# Agenda

- 1. Antecedentes
- 2. Riesgos del cloud computing
- 3. Propuesta
- 4. Prueba de Concepto

# Antecedentes

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/cloud-trend.png}
\end{figure}

# Riesgos del cloud computing

Según _Dutta, Amab and Peng, Guo Chao Alex and Choudhary, Alok_(2013)
en **Risks in enterprise cloud computing: the perspective of IT experts**.
Los 10 principales riesgos en cloud computing son los que se tienen a continuación.

# Riesgos del cloud computing - Parte 1

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  1 &
  LR1.1 &
  La privacidad de la empresa o data del cliente es puesta en riesgo en la nube &
  153.50 \\
  \hline
  2 &
  LR1.3 &
  Leyes inconsistentes de protección de datos en los distintos países donde la data se genera y se guarda. &
  151.75 \\
  \hline
  3 &
  OGR4.2 &
  Dificultad para cambiar de proveedor de la nube incluso en caso de insatisfacción con el servicio(vendor lock-in) &
  148.50 \\
  \hline
  4 &
  OGR5.2 &
  Las compañías usuarias carecen de recuperación de desastres y planes de contingencia para lidiar con fallas
  técnicas inesperadas en la nube &
  147.75 \\
  \hline
  \end{tabular}
\end{table}

# Riesgos del cloud computing - Parte 2

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  5 &
  LR3.2 &
  Dificultad de migración de la empresa al término del contrato con el proveedor de la nube &
  140.25 \\
  \hline
  6 &
  OPR4.2 &
  Inadecuado entrenamiento y/o conocimiento de los servicios de la nube. &
  139.75 \\
  \hline
  7 &
  OPR5.1 &
  Las aplicaciones en la nube temporalmente caen temporalmente en fuera de servicio. &
  137.25 \\
  \hline
  8 &
  OPR2.1 &
  Costos ocultos que se incrementan debido a modos de operacion no transparentes en la nube. &
  136.00 \\
  \hline
  \end{tabular}
\end{table}

# Riesgos del cloud computing - Parte 3

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  9 &
  TR4.3 &
  Ataques de denegación de servicio a un proveedor de la nube. &
  135.50 \\
  \hline
  10 &
  TR4.1 &
  Acceso no authorizado a data empresarial en la nube. &
  135.00 \\
  \hline
  \end{tabular}
\end{table}

# Propuesta - Multi nube

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/multi-cloud.png}
\end{figure}

# Propuesta - Multi nube

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/kubernetes.png}
\end{figure}

#

\begin{center}
\Huge{Prueba de concepto}
\end{center}

# Objetivo

Al desplegar una nueva instancia de PostgreSQL, sin configuración alguna para recuperación de desastres(lo cual es bastante
común, incluso en sistemas que llevan años en producción), se tienen las siguientes medidas:

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  $\infty$ &
  $\infty$ \\ \hline
  \end{tabular}
  \caption{RPO/RTO inicial}
\end{table}

# Herramientas

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/iac-tools.png}
  \caption{Herramientas de gestión de sistemas}%
\end{figure}

# Diagrama ER de base de datos de prueba

\begin{figure}%
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-er.png}
  \caption{Diagrama ER de World}%
\end{figure}

# AWS EC2

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ec2-logo.jpeg}
  \caption{AWS EC2}%
\end{figure}

# AWS S3

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{images/s3.png}
  \caption{AWS S3}%
\end{figure}

# Configuración de servidor en EC2 y almacenamiento en S3 - 1

```sh
let
  region = "us-east-2";
  accessKeyId = "dev";

  ec2 = { resources, ... }:
    {
      deployment.targetEnv = "ec2";
      deployment.ec2 = {
        accessKeyId = accessKeyId;
        region = region;
        instanceType = "t2.micro";
        associatePublicIpAddress = true;
        keyPair = resources.ec2KeyPairs.pg-key-pair;
      };
    };
```

# Configuración de servidor en EC2 y almacenamiento en S3 - 2

```sh
in
{
  resources = {
    ec2KeyPairs.pg-key-pair = {
      inherit region accessKeyId;
    };
    s3Buckets.poc-362 = {
      inherit region accessKeyId;
      versioning = "Suspended";
    };
  };
  pg = ec2;
  ## pg-recovery = ec2;
}
```

# Configuración de instacia primaria de PostgreSQL - 1

```nix
{
  network.description = "Postgres servers";

  pg = { config, pkgs, resources, ... }: {
    environment.systemPackages = [
      pkgs.awscli
    ];
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      authentication = pkgs.lib.mkForce ''
        local all all trust
        local replication postgres trust
      '';
      extraConfig = ''
```

# Configuración de instacia primaria de PostgreSQL - 2

```nix
        wal_level = archive
        archive_mode = on
        max_wal_senders = 10
        archive_command = '${pkgs.awscli}/bin/aws s3 cp %p s3://${resources.s3Buckets.poc-362.name} 1>&2'
      '';
    };

    systemd.services.postgresql = {
      environment = {
        AWS_ACCESS_KEY_ID = builtins.readFile ./key_id.secret;
        AWS_SECRET_ACCESS_KEY = builtins.readFile ./access_key.secret;
      };
    };
  };
```

# Configuración de instacia primaria de PostgreSQL - 3

```nix
  pg-recovery = { config, pkgs, resources, ... }: {
    environment.systemPackages = [
      pkgs.awscli
    ];
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      recoveryConfig = ''
        restore_command = '${pkgs.awscli}/bin/aws s3 cp s3://${resources.s3Buckets.poc-362.name}/%f %p'
        # Especificar el tiempo de restauracion deseado
        # recovery_target_time='2018-12-22 00:04:00'
      '';
    };

```

# Configuración de instacia primaria de PostgreSQL - 4

```
    systemd.services.postgresql = {
      environment = {
        AWS_ACCESS_KEY_ID = builtins.readFile ./key_id.secret;
        AWS_SECRET_ACCESS_KEY = builtins.readFile ./access_key.secret;
      };
    };
  };
}
```

# Provisión de los recursos a EC2 y S3 - 1

\begin{center}
\Huge{nixops deploy -d poc}
\end{center}

# Provisión de los recursos a EC2 y S3 - 2

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ec2-dashboard-1.png}
  \caption{Instancia primaria de PostgreSQL en EC2}%
\end{figure}

# Provisión de los recursos a EC2 y S3 - 3

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/s3-dashboard-1.png}
  \caption{Bucket S3 para almacenamiento de backups incrementales}%
\end{figure}

# Exploración de la base de datos

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-1.png}
  \caption{Data inicial de la bd World}%
\end{figure}

# Simulación de error

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-2.png}
  \caption{DELETE sin WHERE}%
\end{figure}

# Recuperación de desastres - 1

\begin{center}
\Huge{nixops deploy -d poc 19.46s}
\end{center}

# Recuperación de desastres - 2

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/ec2-dashboard-2.png}
  \caption{AWS EC2 con instancia de recuperacion}%
\end{figure}

# Recuperación de desastres - 3

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{images/world-3.png}
  \caption{Base de datos recuperada}%
\end{figure}

# Resultado de la Prueba de Concepto

\begin{table}[H]
  \center
  \begin{tabular}{| l | l |} \hline
  \textbf{RPO} &
  \textbf{RTO} \\ \hline
  0 &
  19.46s < 1 min \\ \hline
  \end{tabular}
  \caption{RPO/RTO final}
\end{table}

#

\begin{center}
\Huge{Gracias}
\end{center}
