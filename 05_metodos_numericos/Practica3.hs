module Practica3 where

import Misc (e)

prob1 :: Float -> Float
prob1 x = sqrt x - cos x

prob2 :: Float -> Float
prob2 x = 3*(x + 1 )*(x - 1/2)*(x - 1)

prob3i :: Float -> Float
prob3i x = x^3 - 7*x^2 + 14*x - 6

prob3ii :: Float -> Float
prob3ii x = x^4 - 2*x^3 - 4*x^2 + 4*x + 4

prob4a :: Float -> Float
prob4a x = x - 1/2**x

prob4b :: Float -> Float
prob4b x = e**x - x^2 + 3*x - 2

prob4c :: Float -> Float
prob4c x = 2*x*(cos 2*x) - (x + 1)^2

prob4d :: Float -> Float
prob4d x = x*(cos x) - 2*x^2 + 3*x - 1

prob4e :: Float -> Float
prob4e x = x^3 - x - 1

prob5 :: Float -> Float
prob5 x = x^2 - 3

prob6 :: Float -> Float
prob6 x = x^3 - 25

prob7 :: Float -> Float
prob7 x = (x + 2)*(x + 1)*x*((x - 1)^3)*(x - 2)

prob8 :: Float -> Float
prob8 x = (x + 2)*((x + 1)^2)*x*((x - 1)^3)*(x - 2)

prob9 :: Float -> Float
prob9 x = x^3 + x - 4

prob10a :: Float -> Float
prob10a x = tan x

prob10b :: Float -> Float
prob10b x = x^3 - x - 1
