module Differentiation where

centralDifference2nd :: (Float -> Float) -> Float -> Float -> Float
centralDifference2nd f x h = (f (x + h) - f (x - h)) / (2*h)

centralDifference4th :: (Float -> Float) -> Float -> Float -> Float
centralDifference4th f x h = (- f (x + 2*h) + 8*(f (x + h)) - 8*(f (x - h)) + f (x - 2*h)) / (12*h)

--f x = x^2 + 2*x
--centralDifference2nd f 3 1
--8.0
--
-- f x = cos x
-- centralDifference2nd f 0.8 0.01
-- -0.7173449
-- centralDifference4th f 0.8 0.01
-- -0.7173588
