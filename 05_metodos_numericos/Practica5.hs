module Practica5 where

import Misc (e)

prob1a :: Float -> Float
prob1a x = x^2 - 6

prob1a' :: Float -> Float
prob1a' x = 2*x

prob1b :: Float -> Float
prob1b x = -x^3 - cos x

prob1b' :: Float -> Float
prob1b' x = -3*(x^2) + sin x

prob2a :: Float -> Float
prob2a x = x^3 -2*x^2 - 5

prob2a' :: Float -> Float
prob2a' x = 3*x^2 - 4*x

prob2b :: Float -> Float
prob2b x = x - cos x

prob2b' :: Float -> Float
prob2b' x = 1 + sin x

prob2c :: Float -> Float
prob2c x = x^3 + 3*x^2 - 1

prob2c' :: Float -> Float
prob2c' x = 3*x^2 + 6*x

prob2d :: Float -> Float
prob2d x = x - 0.8 - 0.2*(sin x)

prob2d' :: Float -> Float
prob2d' x = 1 - cos x

prob3a :: Float -> Float
prob3a x = e**x + 1/2**x + 2*(cos x) - 6

prob3b :: Float -> Float
prob3b x = log (x - 1) + cos (x - 1)

prob3c :: Float -> Float
prob3c x = 2*x*(cos 2*x) - (x - 2)^2

prob3c' :: Float -> Float
prob3c' x = -4*x*(sin 2*x) + 2*(cos 2*x) - 2*(x - 2)

prob3d :: Float -> Float
prob3d x = e**x - 3*x^2

prob3e :: Float -> Float
prob3e x = sin x - 1/e**x
