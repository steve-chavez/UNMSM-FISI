module Integration where

import Debug.Trace

-- https://en.wikipedia.org/wiki/Trapezoidal_rule
trapezoidalRule :: (Float -> Float) -> (Float, Float) -> Float -> Float
trapezoidalRule f (a, b) n =
  (h/2)*(f a + sum [2*(f (a + i*h)) | i <- [1..n-1]] + f b)
  where
    h = (b - a) / n

--f x = 1 + x^2
--trapezoidalRule f (1, 5) 4
--46.0

simpsonRuleOneThird :: (Float -> Float) -> (Float, Float) -> Float -> Float
simpsonRuleOneThird f (a, b) n =
  (h/3)*(f a + sum [4*(f (a + i*h)) | i <- [1..n-1], odd $ round i] + sum [2*(f (a + i*h)) | i <- [1..n-1], even $ round i] + f b)
  where
    h = (b - a) / n

--f x = 1/(1 + x^5)
--simpsonRuleOneThird f (0, 3) 6
--1.0749154

simpsonRuleThreeEighth :: (Float -> Float) -> (Float, Float) -> Float -> Float
simpsonRuleThreeEighth f (a, b) n =
  (3*h/8)*(f a + sum [3*(f (a + i*h)) | i <- [1..n-1], round i `mod` 3 /= 0] + sum [2*(f (a + i*h)) | i <- [1..n-1], round i `mod` 3 == 0] + f b)
  where
    h = (b - a) / n

--f x = 1/(1 + x^2)
--simpsonRuleThreeEighth f (0, 6) 6
--1.3570808
