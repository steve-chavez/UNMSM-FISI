module InitialValueProblem where

import Constants
import Debug.Trace

-- https://en.wikipedia.org/wiki/Euler_method
eulerMethod :: (Float -> Float -> Float) -> (Float, Float) -> Float -> Float -> Float
eulerMethod f (x0, y0) h m
  | x0 >= m   = y0
  | otherwise = eulerMethod f (x0 + h, y0 + h*(f x0 y0)) h m

--f x y = (2/x)*y + x^2*(e**x)
--eulerMethod f (1, 0) 0.1 1.2
--0.68475556

-- https://en.wikipedia.org/wiki/Heun%27s_method
-- a.k.a improved euler
heunMethod :: (Float -> Float -> Float) -> (Float, Float) -> Float -> Float -> Float
heunMethod f (x0, y0) h m
  | x0 >= m   = y0
  | otherwise = heunMethod f (x0 + h, y0 + h*(f x0 y0 + f (x0 + h) (y0 + h*(f x0 y0)))/2) h m

--f x y = x*y
--heunMethod f (1, 1) 0.1 1.3
--1.4109441

-- https://www.geeksforgeeks.org/predictor-corrector-or-modified-euler-method-for-solving-differential-equation/
modifiedEulerMethod :: (Float -> Float -> Float) -> (Float, Float) -> Float -> Float -> Float
modifiedEulerMethod f (x0, y0) h m
  | x0 >= m   = y0
  | otherwise = modifiedEulerMethod f (x0 + h, y0 + (h/2)*(f x0 y0 + f (x0 + h) predicted)) h m
  where
    predicted = y0 + h*(f x0 y0)

--f x y = x + sqrt y
--eulerMethod f (0, 1) 0.2 0.2
--1.2
--modifiedEulerMethod f (0, 1) 0.2 0.2
--1.2295445

-- https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods
rungeKutta :: (Float -> Float -> Float) -> (Float, Float) -> Float -> Float -> Float
rungeKutta f (x0, y0) h m
  | x0 >= m   = y0
  | otherwise = rungeKutta f (x0 + h, y0 + (h/6)*(k1 + 2*k2 + 2*k3 + k4)) h m
  where
    k1 = f x0 y0
    k2 = f (x0 + h/2) (y0 + k1/2)
    k3 = f (x0 + h/2) (y0 + k2/2)
    k4 = f (x0 + h) (y0 + k3)

--f x y = (x - y)/2
--rungeKutta f (0, 1) 1 1
--0.8203125
--rungeKutta f (0, 1) 1 2
--1.1045126
--rungeKutta f (0, 1) 1 3
--1.670186

adamsBashforthMoulton :: (Float -> Float -> Float) -> (Float, Float) -> (Float, Float) -> (Float, Float) -> (Float, Float) -> Float -> Float -> Float
adamsBashforthMoulton f (x0, y0) (x1, y1) (x2, y2) (x3, y3) h m
  | x3 >= m   = y3
  | otherwise = adamsBashforthMoulton f (x1, k1) (x2, k2) (x3, k3) (x3 + h, k4c) h m
  where
    k0  = f x0 y0
    k1  = f x1 y1
    k2  = f x2 y2
    k3  = f x3 y3
    k4  = y0 + (h/24)*(-9*k0 + 37*k1 - 59*k2 + 55*k3) -- bashforth
    k4c = y0 + (h/24)*(9*k4 + 19*k3 - 5*k2 + k1)      -- moulton

--f x y = x*y/2
--adamsBashforthMoulton f (0, 1) (0.1, 1.01) (0.2, 1.022) (0.3, 1.023) 0.1 0.4
