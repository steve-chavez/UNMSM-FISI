module Practica4 where

import Misc (e)

prob1 :: Float -> Float
prob1 x = x^4 + 2*x^2 -x - 3

prob1_g1 :: Float -> Float
prob1_g1 x = (3 + x - 2*x^2)**(1/4)

prob1_g2 :: Float -> Float
prob1_g2 x = ((3 + x)/(x^2 + 2))**(1/2)

prob1_g3 :: Float -> Float
prob1_g3 x = sqrt $ (3 + x - x^4)/2

prob1_g4 :: Float -> Float
prob1_g4 x = (3*x^4 + 2*x^2 + 3)/(4*x^3 + 4*x - 1)
