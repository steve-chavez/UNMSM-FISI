module Interpolation where

import Data.List (inits)

-- https://en.wikipedia.org/wiki/Lagrange_polynomial
lagrangePolynomial :: [(Float, Float)] -> (Float -> Float)
lagrangePolynomial points =
  \x -> sum [ (basis x xi) * yi | (xi, yi) <- points ]
  where
    basis x xi = product [ (x - xj)/(xi - xj) | (xj, _) <- points, xj /= xi ]

--l = lagrangePolynomial [(3, 7), (4, 6), (5, 3)]
--l 3.8
--6.3599997

--l = lagrangePolynomial [(2, 4), (3, 7), (4, 6), (5, 3)]
--l 3.8
--6.424

-- https://en.wikipedia.org/wiki/Newton_polynomial
newtonPolynomial :: [(Float, Float)] -> (Float -> Float)
newtonPolynomial points =
   \x -> sum [ basis x ps | ps <- tail $ inits points ]
  where
    basis x ps = dividedDifferences ps * product [ x - xi | (xi, _) <- init ps ]

-- https://en.wikipedia.org/wiki/Divided_differences#Example
dividedDifferences :: [(Float, Float)] -> Float
dividedDifferences points = case points of
  [(_, y0)]            -> y0
  [(x0, y0), (x1, y1)] -> (y1 - y0)/(x1 - x0)
  ps                   -> (dividedDifferences (tail ps) - dividedDifferences (init ps))/(fst (last ps) - fst (head ps))

newtonCoefficients :: [(Float, Float)] -> [Float]
newtonCoefficients points = [ dividedDifferences ps | ps <- tail $ inits points ]

--f = newtonPolynomial [(0, 1), (1, 4), (2, 9), (3, 16)]
--f is x^2 + 2*x + 1
--f 4
--25.0
--f 5
--36.0
--
--newtonCoefficients  [(0, 1), (1, 4), (2, 9), (3, 16)]
--[1.0,3.0,1.0,0.0]
--newtonCoefficients  [(-5, -2), (-1, 6), (0, 1), (2, 3)]
--[-2.0,2.0,-1.4,0.4857143]
