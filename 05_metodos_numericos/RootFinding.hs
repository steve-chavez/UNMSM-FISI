module RootFinding where

import           Debug.Trace

import qualified Misc         as M
import qualified Practica3    as P3
import qualified Practica4    as P4
import qualified Practica5    as P5

-- From https://en.wikipedia.org/wiki/Bisection_method#Algorithm
-- numIter == 1 to coincide with the iterations in
-- https://en.wikipedia.org/wiki/Bisection_method#Example:_Finding_the_root_of_a_polynomial
-- tol(erance) could be understood as the accuracy we want to exceed
bisection :: (Float -> Float) -> (Float, Float) -> Float -> Integer -> Float
bisection f (a, b) tol numIter
  | f c == 0 || numIter <= 1 || inc < tol = c
  | (f a) * (f c) < 0                     = bisection f (a, c) tol $ numIter - 1
  | otherwise                             = bisection f (c, b) tol $ numIter - 1
  where
    c   = (a + b)/2
    inc = abs $ (b - a)/2

regulaFalsi :: (Float -> Float) -> (Float, Float) -> Float -> Integer -> Float
regulaFalsi f (a, b) tol numIter
  | f c == 0 || numIter <= 1 || inc < tol = c
  | fa * fb < 0                           = regulaFalsi f (a, c) tol $ numIter - 1
  | otherwise                             = regulaFalsi f (c, b) tol $ numIter - 1
  where
    fa = f a
    fb = f b
    c   = (a*fb - b*fa)/(fb - fa)
    inc = abs $ (b - a)/2

fixedPointMethod :: (Float -> Float) -> Float -> Float -> Integer -> Float
fixedPointMethod g x0 tol numIter
  | numIter <= 1 || inc < tol = c
  | otherwise                 = fixedPointMethod g c tol $ numIter - 1
  where
    c   = g x0
    inc = abs $ c - x0

newtonRaphson :: (Float -> Float) -> (Float -> Float) -> Float -> Float -> Integer -> Float
newtonRaphson f df x0 tol numIter
  | numIter <= 1 || inc < tol = c
  | otherwise                 = newtonRaphson f df c tol $ numIter - 1
  where
    c   = x0 - (f x0)/(df x0)
    inc = abs $ c - x0

-- From https://en.wikipedia.org/wiki/Secant_method
secantMethod :: (Float -> Float) -> Float -> Float -> Float -> Integer -> Float
secantMethod f x0 x1 tol numIter
  | numIter <= 1 || inc < tol = x2
  | otherwise                 = secantMethod f x1 x2 tol $ numIter - 1
  where
    x2   = x1 - (f x1)*(x1 - x0)/(f x1 - f x0)
    inc = abs $ x2 - x1

-- λ> regulaFalsi example22 (-5, -4) 0.00005 100
-- -0.45896232
-- λ> regulaFalsi example22 (0.9, 1) 0.00005 100
-- 0.9100075
-- λ> regulaFalsi example22 (3.7, 3.8) 0.00005 100
-- 3.7330792

-- λ> bisection example22 (-5, -4) 0.00005 100
-- -4.0000305
-- λ> bisection example22 (0.9, 1) 0.00005 100
-- 0.9100097
-- λ> bisection example22 (3.7, 3.8) 0.00005 100
-- 3.7330565

--λ> newtonRaphson prob5_1 dprob5_1 1 0.00001 2
--2.607143
