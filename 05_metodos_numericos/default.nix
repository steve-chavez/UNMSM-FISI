# From https://maybevoid.com/posts/2019-01-27-getting-started-haskell-nix.html
{ nixpkgs ? import <nixpkgs> {} }:
with nixpkgs;

stdenv.mkDerivation {
  name = "metodos_numericos";
  buildInputs = [
    ghc
  ];
}
