module Misc where

import Constants

-- From https://ncalculators.com/math-worksheets/regula-falsi-method-example.htm
ncalculators :: Float -> Float
ncalculators x = x^3 - 2*x - 5

-- From https://en.wikipedia.org/wiki/Bisection_method#Example:_Finding_the_root_of_a_polynomial
bisectionWikiExample :: Float -> Float
bisectionWikiExample x = x^3 - x - 2

-- From https://en.wikipedia.org/wiki/Secant_method#Computational_example
secantWikiExample :: Float -> Float
secantWikiExample x = x^2 - 612

-- From clases/2\ BISECCION-REGULA\ FALSI\ 2.pdf
example22 :: Float -> Float
example22 x = 3*x^2 - e**x

example22_g1 :: Float -> Float
example22_g1 x = (e**(x/2))/(sqrt 3)

-- From clases/4\ NEWTON\ SECANTE.pdf
example211 :: Float -> Float
example211 x = x - tan x

-- From https://ece.uwaterloo.ca/~dwharder/NumericalAnalysis/10RootFinding/secant/examples.html
uWaterlooExample :: Float -> Float
uWaterlooExample x = cos x + 2*(sin x) + x^2

-- From https://mat.iitm.ac.in/home/sryedida/public_html/caimna/transcendental/iteration%20methods/fixed-point/iteration.html
madrasExample :: Float -> Float
madrasExample x = x^4 - x - 10

madrasExample_g2 :: Float -> Float
madrasExample_g2 x = (x + 10)**(1/4)
