---
title:
- CORBA - Common Object Request Broker Architecture
author:
- Manuel Chávez Astucuri
institute:
- UNMSM - FISI
theme:
- metropolis

header-includes:
   - \usepackage{caption}
   - \usepackage{tikz}
   - \usetikzlibrary{positioning}
   - \usepackage[export]{adjustbox}
---

# Agenda

- 1. Qué es CORBA?
- 2. Arquitectura.
- 3. Componentes.
- 4. Implementación.

# Qué es CORBA?


\begin{columns}

\column{0.6\textwidth}

\begin{itemize}
\item Es un standard definido por el OMG(Object Management Group) que sirve para crear \textbf{objetos distribuidos}.
\item Promueve el diseño de aplicaciones como un conjunto de \textbf{objetos} cooperantes.
\item Estos \textbf{objetos} son independientes de la plataforma(sistema operativo o lenguaje de programación).
\item Estos \textbf{objetos} pueden estar ubicados en cualquier lugar en una red.
\end{itemize}

\column{0.3\textwidth}

\begin{figure}
  \centering
  \includegraphics{images/omg.jpg}
\end{figure}

\end{columns}

# CORBA vs RMI - Diferencias

\begin{columns}

\column{0.6\textwidth}

\begin{itemize}
  \item CORBA es independiente del lenguaje, RMI no.
  \item Las interfaces de CORBA están definidas en un IDL(interface description language),
  \item las interface de RMI están definidas en interfaces de Java.
  \item CORBA puede usar protocolos más eficientes como ZIOP(Zipped Inter-ORB protocol).
\end{itemize}

\column{0.3\textwidth}

\begin{figure}
  \centering
  \includegraphics{images/corba_langs.png}
\end{figure}

\end{columns}

# CORBA vs RMI - Similitudes

![](images/corba-vs-rmi-1.jpg)

# CORBA vs RMI - Similitudes

\centering
\includegraphics[width=0.8\textwidth]{images/corba-vs-rmi-2.jpg}

# Arquitectura

\centering
\includegraphics[width=0.8\textwidth]{images/architecture-1.png}

# Arquitectura

![](images/architecture-2.jpg)

# Componentes - Object Request Broker

![](images/orb.png)

# Componentes - Object Request Broker

- Hub de comunicaciones para todos los objetos
- El broker maneja peticiones entre clientes y servidores
- El broker puede elegir el servidor que mejor satisfaga las necesidades
  del cliente
- Permite la seperacion de implementación e interfaz
- Provee transparencia de ubicación

# Componentes - Object Adapter

- Sirve de puente entre los objetos CORBA y las interfaces IDL.
- Crea referencias remotas a objetos CORBA.
- Provee un nombre único a cada objeto CORBA.
- Permite al ORB operar con distintos tipos de objetos.

# Componentes - Interface Definition Language

![](images/idl.png)

# Componentes - Interface Definition Language

- IDL provee facilidades para definir modulos, interfaces, tipos, atributos y metodos.
- La gramática es un subconjunto del ANSI C++.

  ```idl
  module HelloApp {
    interface Hello {
      string sayHello();
      oneway void shutdown();
    };
  };
  ```

# Componentes - IIOP

- El General Inter Orb Protocol(GIOP) define
  + Una representación externa, llamada CDR(Command data representation)
  + Specifica los formatos de los mensajes de petición-respuesta.

- El Internet Inter Orb Protocol(IIOP) define la forma estándar para referencias a objetos remotos.
  + IIOP es una forma especializada de GIOP sobre TCP/IP.

# Implementación - Pasos para crear una aplicación basada en CORBA.

1. Definir una interfaz(archivo IDL).
2. Mapear el IDL a Java(usando el compilador idlj)
3. Implementar la interfaz
4. Correr una instancia ORB.
5. Implementar/correr el servidor
6. Implementar/correr el cliente

#

\begin{center}
\Huge{Demo}
\end{center}

# 

\begin{center}
\Huge{Gracias}
\end{center}
