# Pgday 2019

For development do:

```bash
make live
```

Slides based on https://www.slideshare.net/begriffs/a-tour-of-postgrest.

To create the companies image do:

```bash
-- montage comes from imagemagick
montage pgrst_companies/*.png companies.png
```
