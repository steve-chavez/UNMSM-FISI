---
title:
- Cloud computing - Computación en la nube
author:
- Manuel Chávez Astucuri
institute:
- UNMSM - FISI
theme:
- metropolis

header-includes:
- \usepackage{caption}
- \usepackage{tikz}
- \usetikzlibrary{positioning}
- \usepackage[export]{adjustbox}
- \usepackage{array}
---

# Agenda

- 1. Definición
- 2. Modelos de servicio
- 3. Modelos de despliegue
- 4. Proveedores líderes según Gartner
- 5. Comparación de proveedores
- 6. Riesgos del cloud computing

# Qué es cloud computing?

De acuerdo a **NIST**(traducción):

"Cloud computing es un modelo que habilita un acceso on-demand, ubicuo y conveniente
a un pool compartido de recursos computacionales que puede ser rápidamente provisto
con un esfuerzo mínimo de configuración e interacción con el proveedor del servicio."

\vspace{30px}

\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{images/nist.png}
\end{figure}

# Características esenciales

\begin{itemize}
  \item Auto servicio on-demand
  \item Acceso a la red amplio
  \item Pooling de recursos
  \item Rápida elasticidad
  \item Medida de servicio
\end{itemize}

# Modelos de despliegue

- **Nube privada**: uso exclusivo para una sola organización. on y off premise.
- **Nube comunitaria**: uso para multiples organizaciones con necesidades comunes. on y off premise.
- **Nube pública**: uso para público general. off premise.
- **Nube híbrida**: combinación de los tipos anteriores. enfocado en portabilidad en distintas nubes.

# Modelos de despliegue

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/cloud-deploy-models.jpg}
\end{figure}

# Modelos de servicio

- Infrastructure as a Service(IaaS)
- Platform as a Service(PaaS)
- Software as a Service(SaaS)

# Modelos de servicio

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/cloud-service-models.png}
\end{figure}

# Infrastructure as a Service(IaaS)

"La capacidad provista al consumidor es la de poder aprovisionar procesamiento, almacenamiento, redes y 
otros recursos computacionales fundamentales donde el consumidor
es capaz de desplegar y correr software arbitrario, que puede incluir sistemas oper
ativos y aplicaciones. El consumidor no controla la infraestructura(hardware) pero
tiene control sobre el sistema operativo, almacenamiento, aplicaciones desplegadas y
posiblemente la configuración de red."

# Platform as a Service(PaaS)

"La capacidad provista al consumidor es el despliegue a la infraestructura del proveedor.
Pero solo de aplicaciones creadas usando lenguajes de programación, librerías, servicios
y herramientas soportadas por el proveedor. El consumidor no maneja o controla
la infraestructura incluyendo la configuración de red, servidor, sistema operativo y
almacenamiento, pero tiene control sobre las aplicaciones desplegadas y posiblemente
sobre la configuración de la aplicación desplegada."

# Software as a Service(SaaS)

"La capacidad provista al consumidor es el uso de aplicaciones alojadas en la infraestructura del proveedor. 
Las aplicaciones son accesibles desde varios dispositivos clientes a
través de una interfaz ligera, como un navegador web o la interfaz de un programa. El
consumidor no maneja o controla la infraestructura del proveedor, incluyendo su confi
guración de red, servidores, sistemas operativos, almacenamiento o incluso capacidades
individuales de las aplicaciones, con la posible excepción de algunas configuraciones
específicas al usuario."

# Cuadrante mágico Gartner 2018

\begin{figure}
  \centering
  \includegraphics[width=0.75\textwidth]{images/magic-q.png}
\end{figure}

# AWS vs GCP vs Azure

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/cloud-providers-vs.png}
\end{figure}

# Riesgos del cloud computing

Según _Dutta, Amab and Peng, Guo Chao Alex and Choudhary, Alok_(2013)
en **Risks in enterprise cloud computing: the perspective of IT experts**.
Los 10 principales riesgos en cloud computing son los que se tienen a continuación.

# Riesgos del cloud computing - Parte 1

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  1 &
  LR1.1 &
  La privacidad de la empresa o data del cliente es puesta en riesgo en la nube &
  153.50 \\
  \hline
  2 &
  LR1.3 &
  Leyes inconsistentes de protección de datos en los distintos países donde la data se genera y se guarda. &
  151.75 \\
  \hline
  3 &
  OGR4.2 &
  Dificultad para cambiar de proveedor de la nube incluso en caso de insatisfacción con el servicio(vendor lock-in) &
  148.50 \\
  \hline
  4 &
  OGR5.2 &
  Las compañías usuarias carecen de recuperación de desastres y planes de contingencia para lidiar con fallas
  técnicas inesperadas en la nube &
  147.75 \\
  \hline
  \end{tabular}
\end{table}

# Riesgos del cloud computing - Parte 2

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  5 &
  LR3.2 &
  Dificultad de migración de la empresa al término del contrato con el proveedor de la nube &
  140.25 \\
  \hline
  6 &
  OPR4.2 &
  Inadecuado entrenamiento y/o conocimiento de los servicios de la nube. &
  139.75 \\
  \hline
  7 &
  OPR5.1 &
  Las aplicaciones en la nube temporalmente caen temporalmente en fuera de servicio. &
  137.25 \\
  \hline
  8 &
  OPR2.1 &
  Costos ocultos que se incrementan debido a modos de operacion no transparentes en la nube. &
  136.00 \\
  \hline
  \end{tabular}
\end{table}

# Riesgos del cloud computing - Parte 3

\begin{table}[H]
  \begin{tabular}{ | l | m{0.1\textwidth} | m{0.6\textwidth} | m{0.1\textwidth} |}
  \hline
  &
  \textbf{ID} &
  \textbf{Descripcion} &
  \textbf{Score} \\
  \hline
  9 &
  TR4.3 &
  Ataques de denegación de servicio a un proveedor de la nube. &
  135.50 \\
  \hline
  10 &
  TR4.1 &
  Acceso no authorizado a data empresarial en la nube. &
  135.00 \\
  \hline
  \end{tabular}
\end{table}

# Nueva tendencia - Multi nube

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/multi-cloud.png}
\end{figure}

# Nueva tendencia - Multi nube

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/kubernetes.png}
\end{figure}

# 

\begin{center}
\Huge{Gracias}
\end{center}
